import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * This class is used for the sunflower object in the game
 * 
 * @author Eric Liu
 * @version May 9, 2018
 */

public class Sunflower extends Plants {
    
    private GifImage gif = new GifImage("sun_flower.gif");
    private String game_mode; // For which game mode the user chose
    
    /**
     * Constructor for this class
     */
    public Sunflower (String mode) {
        setImage(gif.getCurrentImage());
        this.game_mode = mode;
    }
    
    private long last_time = System.currentTimeMillis();
    
    /**
     * Helper method to produce sunlight with a time interval in milliseconds
     */
    public void Produce_Sun (int num_time) {
        long currTime = System.currentTimeMillis();
        if (currTime >= last_time + num_time) {
            last_time = currTime;
            Sun sun = new Sun(this.game_mode);
            World world = getWorld();
            if (world.getObjectsAt(getX() + 30, getY() + 30, Sun.class).isEmpty()) world.addObject(sun, getX() + 30, getY() + 30);
        }
    }
    
    /**
     * Act - do whatever the Sunflower wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act () {
        setImage(gif.getCurrentImage());
        Produce_Sun(5000);//spawn a sun every 5 seconds
        Plant_Hit("sun_flower_dying.gif", 1000); // Run the helper method in the super class to see if sunflower is hit by zombie
    }    
}
