import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Used for stopping selected song in Music_Settings
 * Class doesn't contain any code whatsoever...
 * 
 * @author Charles Cai
 * @version June 11th, 2018
 */

public class StopButton extends Actor {
    /**
     * Act - do whatever the StopButton wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act () {
        
    }    
}
