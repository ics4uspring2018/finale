import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * This class is used to display the final winning screen for the game
 * Make sure to display the player information on this final win-screen
 * 
 * @author Eric Liu
 * @version May 29th, 2018
 */

public class Win_Screen extends World {

    private GreenfootImage background = new GreenfootImage("thankYouScreen.png");
    private Player_Info player_info; // Player_Info object
    private Sound_Info sound_info; // Sound_Info object
    private Button exit; // Exit button
    private Button scoreboard; // Scoreboard button
    private GreenfootSound music = new GreenfootSound("winmusic.wav");
    
    /**
     * Constructor for objects of class Win_Screen.
     * 
     */
    public Win_Screen (Sound_Info soundinfo, Player_Info playerinfo) {    
        // Create a new world with 800x600 cells with a cell size of 1x1 pixels.
        // super(1111, 602, 1);
        super(800, 600, 1, false);
        this.sound_info = soundinfo;
        this.player_info = playerinfo;
        background.scale(getWidth(), getHeight());
        int xcoor = 0, ycoor = 0;
        getBackground().drawImage(background, xcoor, ycoor);
        // background.scale(getWidth(), getHeight());
        // getBackground().drawImage(background, 0, 0);
        // Instantiate a new instance of these Button objects
        exit = new Button("exit", new GreenfootImage("exit_button.png"));
        scoreboard = new Button("scoreboard", new GreenfootImage("view_scoreboard.png"));
        addObject(exit, getWidth() / 2, 560);
        addObject(scoreboard, getWidth() / 2, 160);
        // Replay replay = new Replay(this.sound_info, this.player_info);
        // addObject(replay, getWidth() / 2, 500);
        Display();
        // Play the winning song
        music.playLoop(); // Play it in a loop
    }
    
    /**
     * Method displays the player's information onto the World screen
     * Make sure to display information such as the player's username, level, and final score
     */
    public void Display () {
        // Display the player's username first
        addObject(new Text("Player: " + player_info.getName(), 30, "black", "white"), getWidth() / 2, 250);
        addObject(new Text("Level: " + String.valueOf(player_info.getLevel()), 30, "black", "white"), getWidth() / 2, 300);
        addObject(new Text("Score: " + String.valueOf(player_info.getScore()), 30, "black", "white"), getWidth() / 2, 350);
        addObject(new Text("Chosen Difficulty: " + String.valueOf(player_info.getDifficulty()), 30, "black", "white"), getWidth() / 2, 400);
    }
    
    /**
     * Act - do whatever the Player_Info wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act () {
        if (exit.isPressed()) {
            music.stop();
            Greenfoot.setWorld(new StartScreen(this.sound_info, this.player_info));
            Greenfoot.stop();
        }
        if (scoreboard.isPressed()) {
            music.stop();
            Greenfoot.setWorld(new Scoreboard(this.sound_info, this.player_info, 2));
        }
   }
}
