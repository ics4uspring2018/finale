import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import javax.swing.*; // For JFrame and JOptionPane
import java.io.*;
import java.util.*;
import java.util.List;
import java.util.ArrayList;

/**
 * This class serves as the primary class for the regular game mode
 * This class includes almost all of the methods that are used to run the game
 * The game ends when the user has completed all of the levels and/or obtained all of the plants
 * There should be a trophy that is awarded to the user at the end
 * 
 * @author Charles Cai, Eric Liu, Cindy Cao
 * @version June 12, 2018
 */

public class Backyard extends World {

    private GreenfootImage background = new GreenfootImage("background.png"); // Background picture
    private Button pause; // Pause button
    private Button settings; // Settings button to direct the user to the settings screen
    private Button view_scoreboard; // For viewing the scoreboard
    private Button restart; // For restarting the game
    private String username; // The player's username
    // Use this to push every GreenfootSound variable onto the stack//queue depending on LIFO or FIFO
    private Stack<GreenfootSound> sounds = new Stack<GreenfootSound>();
    // Use this to push every GreenfootImage variable onto a queue/stack depending on LIFO or FIFO
    private Queue<GreenfootImage> images = new Queue<GreenfootImage>();
    private String sound1 = "menu.wav", sound2 = "chomp.wav", sound3 = "atebrains.wav", sound4 = "zombies_coming.wav", sound5 = "lawnmower.wav", sound6 = "menu.wav", sound7 = "game_end.wav", music_file = "background.wav"; // Initialize these String variables later
    // Theoretically, we only need one background picture since there is only 1 level for now
    private String background1 = "background.png", background2 = "background.png", background3 = "background.png", background4 = "background.png"; // Initialize these String variables later
    private GreenfootSound music; // = new GreenfootSound(music_file); // Initialize this later
    private SimpleTimer timer; // Use this for the main SimpleTimer object
    private boolean has_lost, has_won; // Boolean for whether the user has lost and won the game
    private boolean new_wave_zombies; // Boolean for whether there is a new wave of zombies -> same as levelling up
    private int level; // Current level of player
    private Player_Info player_info; // Main Player_Info object -> use this to write the scores to the text file
    private final String scores_file = "scores.txt"; // File name for reading and/or writing scores
    private Reading_and_Writing_Text_Files reading_writing_files_1 = new Reading_and_Writing_Text_Files(scores_file); // Used to write to the scores.txt file
    private int score; // Current score of player
    private ArrayList<Player_Info> players; // ArrayList to store the list of a certain number of players from the scores.txt file
    private GreenfootImage trophy = new GreenfootImage("trophy.png"); // Make sure to display this at the end when the user has completed all of the levels
    private int difficulty; // Current difficulty level chosen for game

    // Declare and initialize all of the sound effects for the game
    private GreenfootSound menu_sound = new GreenfootSound(sound1), brains_ate = new GreenfootSound(sound3), eating_effect = new GreenfootSound(sound2), zombie_sound = new GreenfootSound(sound4), pea_shooting, zombie_died, plant_died, mower_sound = new GreenfootSound(sound5), lose_sound = new GreenfootSound(sound7), win_sound;

    // Define the boolean variables needed for falling sunlight

    private boolean fallingSunAlertAlreadyDisplayed;
    private boolean fallingSunAlertRemoved;

    private Starburst starburst = new Starburst(); // Starburst object
    private AlertMessage alertMessage; // Define the AlertMessage object

    // Counter objects
    public static Counter sun_counter = new Counter();
    public static Counter score_counter = new Counter();

    // For storing coordinates
    private int cols[];
    private int rows[];

    // RNG object
    private Random rng = new Random(); 

    Queue<Sun> sunq = new Queue<Sun>();

    private Sound_Info sound_info = null;
    
    // Declare temp boolean variables
    private boolean flag = false;
    private boolean flag1 = false;
    

    private final int GAME_SPEED = 50;
    // Declare the two 2-dimensional arrays for the sod spacing
    private int [][] sodSpaceCentreY;
    private int [][] sodSpaceCentreX;
    // Declare the width and height variables for the sod space/garden
    private final int sodSpaceHeight = 100;
    private final int sodSpaceWidth = 80;
    // Declare the offset variables for the sod space/garden
    private final int sodSpaceOffsetY = 120;
    private final int sodSpaceOffsetX = 80;
    private boolean [][] sodSpaceOccupied; // 2-dimensional array for sod space that is occupied by a plant

    private Regular_Lawnmower [] mower; // Object array for class Lawnmower (can use ArrayLists alternatively)

    private int gameStatus; // Status for events that occur in-game

    // Define and declare all of the Seedpacket classes here!!!
    private PeashooterSeedpacket peaShooterSeedPacket; // Object for class PeashooterSeedpacket
    private SunflowerSeedpacket sunflowerSeedPacket; // Object for class SunflowerSeedpacket
    private IcePeashooterSeedpacket icepeaShooterSeedPacket; // Object for class IcePeashooterSeedpacket
    private TriplePeashooterSeedPacket triplePeashooterSeedPacket; // Object for class TriplePeashooterSeedpacket
    private DoublePeashooterSeedPacket doublePeashooterSeedPacket; // Object for class DoublePeashooterSeedPacket
    private MachineGunSeedPacket machinegunPeaShooterSeedPacket; // Object for class MachineGunSeedPacket
    private PotatoSeedPacket potatoSeedPacket; // Object for class PotatoSeedpacket
    private CherryBombSeedPacket cherrybombSeedPacket; // Object for class CherryBombSeedpacket

    private int sunpoints = 0; // Store number of points that are collected from the sun

    //private Text1 sunPointsLabel; // Text1 object for label of amount of sun points

    // Define constants needed for "ready", "set", "plant", and other messages
    // NOTE: We can also use Java's enums when doing code refactoring! :)
    private final int ALERT_READY = 0;
    private final int ALERT_SET = 1;
    private final int ALERT_PLANT = 2;
    private final int ALERT_CLICK_SUN = 3;
    private final int ALERT_YOUR_HOUSE = 4;
    private final int ALERT_ZOMBIES_WIN = 5;

    // Define all of the constants needed to indicate the game's status (what stage the game is at)
    // NOTE: We can also use Java's enums when doing code refactoring! :)
    private final int TITLE_SCREEN = 0;
    private final int BG_SCROLL_SETUP = 1;
    private final int BG_SCROLL_ANIMATE = 2;
    private final int STAGE_SETUP = 3;
    private final int READY_MESSAGE = 4;
    private final int BEGIN_PLAY = 5;

    private int stage_CycleCnt = 0; // Variable used for cycling through the stage

    private GreenfootSound titleScreenMusic = new GreenfootSound("pvz_titleScreen.mp3");
    private GreenfootSound frontYardMusic = new GreenfootSound("background.wav");
    private GreenfootSound scrollMusic = new GreenfootSound("pvz_menuTheme.mp3");
    private GreenfootSound win_music = new GreenfootSound("winmusic.wav");
    private GreenfootSound lose_music = new GreenfootSound("losemusic.wav");
    private GreenfootSound scream = new GreenfootSound("scream.wav"); // Scream sound for when the user loses (when the zombie has entered the house)
    private GreenfootSound zombieLaughMusic = new GreenfootSound("losemusic.wav");
    private int musicVolume = 100;

    private int alpha = 0; // Store transparency level

    // Play the fields associated with the in-game sounds
    private boolean awoogaAlreadyPlayed = false;
    private GreenfootSound awooga = new GreenfootSound("awooga.wav");
    private GreenfootSound zombieGroan[]; // Groan noises associated with zombies

    private boolean music_playing = false; // Is the music playing in the game?
    private boolean starburst_on_screen = false; // Is there a Starburst object on-screen?
    private GreenfootSound in_game_music; // GreenfootSound for in-game music

    // Screen variable for displaying most of the animations
    private Screen screen;

    // Needed for the background scrolling
    private int bgOffset = 0;
    private int bgOffsetDelta = 1;

    // Now declare all of the variables associated with GreenfootImage
    // Excuse the names of these variables
    // TODO: Still need to use better variable names
    private GreenfootImage blackScreen = new GreenfootImage("BlackScreen.png"); // Supposed to be beginning screen => change the picture later so that it's not entirely black
    private GreenfootImage whiteScreen = new GreenfootImage("whiteScreen.png"); // Declare both of the objects associated with class Screen
    private GreenfootImage frontYardImage = new GreenfootImage("yard_5rows.png"); // Picture for yard with 5 rows
    private GreenfootImage fullBGImage = new GreenfootImage("yard_full5rows.png"); // Pictue for yard with full 5 ros

    // TODO: Make sure to change the filename for the last GreenfootImage
    private GreenfootImage thankYouScreen = new GreenfootImage("thankYouScreen.png"); // Change this to a screen for when the user has won (completed all the levels)

    private GreenfootImage popcapindent = new GreenfootImage("PopCapIdent.png"); // Logo for popcapindent => Was formerly devIdentScreen1
    private GreenfootImage titleScreen = new GreenfootImage("PVZtitleScreen.png"); // Was formerly screen2
    private GreenfootImage credits = new GreenfootImage("Ident.png"); // Was formerly devIdentScreen1
    
    private GreenfootImage new_wave = new GreenfootImage("huge_wave_of_zombies_text.png"); // Alert user of new zombie wave
    

    // Declare the ArrayList of GreenfootImages for the final plant seed packets that the user collects
    private ArrayList<GreenfootImage> seed_packet_list = new ArrayList<GreenfootImage>();
    // Alternatively, we can also use a HashMap<GreenfootImage, boolean> instead

    // The key pair consists of: GreenfootImage and a Boolean => used to check which seedpacket the user has available to them
    private HashMap<Seedpacket, Boolean> dictionary = new HashMap<Seedpacket, Boolean>();

    // List for storing all of the seed packets that are to be used in regular mode
    private ArrayList<Seedpacket> seedpackets = new ArrayList<Seedpacket>();

    // Define booleans for the "click status" of all of the seed packets
    // TODO: Make sure to add more plants!
    public boolean sunflowerSeedPacketClicked, peashooterSeedPacketClicked, icepeashooterSeedPacketClicked, doublepeashooterSeedPacketClicked, triplepeashooterSeedPacketClicked, machinegunpeashooterSeedPacketClicked, potatoSeedPacketClicked, cherrybombSeedPacketClicked;
    // NOTE: These are for the other plants that can be implemented in the future

    // Declare variable for whether user has completed the level
    private boolean stage_completed;

    // Variable is used for the current stage of the regular mode the user is currently at => used as an index for iterating through the Lists of seedpackets, etc
    private int level_idx = 2; // Set this to 2 initially => since user always start with both the sunflower and the peashooter

    private boolean easy, medium, hard; // One ofr each diffiuclty level

    // Just planning to have 4 types of zombies in total
    private int num_types_zombies = 4; // For the number of types of zombies => set this to 4 initially

    private String zombies_types[]; // For storing the available types of zombies that are to be used in the game

    // Declare the variable used for storing the number of levels needed for the user to win

    private final int total_num_levels = 8; // Have 8 levels in total => feel free to change this value later

    private int action;
    private SBoard sunCount = new SBoard(100, 0); // Count the sun

    private boolean plant_removed = false; // Have any plants been removed yet by the shovel

    private Regular_Shovel shovel = new Regular_Shovel(735, 40); // Shovel object1

    private boolean added_suncount = false; // For whether the suncount object has been added in world at each stage

    private boolean has_levelled = false; // Has the user levelled up

    private int zombie_waveCycleCnt = 0; // For generating new zombie waves at set intervals
    
    private int num_waves = 0; // How many zombie waves
    
    private boolean done_waves = false; // Have all of the waves already been generated

    /**
     * 1st Constructor for objects of class Backyard.
     * NOTE: The commented lines are all unnecessary for this game mode
     * Make sure to ask the user for his username in this constructor
     */
    public Backyard () {    
        // Create a new world with 1111x600 cells with a cell size of 1x1 pixels.
        super(1111, 600, 1, false);
        int xcoor = 0, ycoor = 0;

        // Set the speed of the game
        Greenfoot.setSpeed(GAME_SPEED);

        screen = new Screen(blackScreen);
        // Add the screen
        addObject(screen, 556, 300);

        // Prepare all of the Greenfoot objects
        GenerateSounds();
        GenerateBackgrounds();

        // Make sure to set the correct paint order before beginning
        setPaintOrder(SeedPacketEndStage.class, Starburst.class, Screen.class, Sun.class, Sun1.class, AlertMessage.class, SBoard.class, Counter.class, Regular_Zombies.class, Frozen_Zombie.class, Seedpacket.class, SeedPacketTray.class, Regular_Pea.class, Plant.class);

        // Now set the dimensions for the 2-D arrays used for the backyard
        sodSpaceCentreX = new int[5][9];
        sodSpaceCentreY = new int[5][9];
        sodSpaceOccupied = new boolean[5][9];

        // Now initialize all of the lawnmowers needed
        mower = new Regular_Lawnmower[5];
        for (int i=0; i<5; i++) {
            mower[i] = new Regular_Lawnmower();
        }

        // Instantiate the seed packets
        peaShooterSeedPacket = new PeashooterSeedpacket();
        sunflowerSeedPacket = new SunflowerSeedpacket();
        icepeaShooterSeedPacket = new IcePeashooterSeedpacket();
        triplePeashooterSeedPacket = new TriplePeashooterSeedPacket();
        doublePeashooterSeedPacket = new DoublePeashooterSeedPacket();
        machinegunPeaShooterSeedPacket = new MachineGunSeedPacket();
        potatoSeedPacket = new PotatoSeedPacket();
        cherrybombSeedPacket = new CherryBombSeedPacket();

        Add_Seed_Packets(); // Now iitialize list of seedpackets
        
        // Declare size of GreenfootSound array
        // Now initialize contents of array 
        zombieGroan = new GreenfootSound[13];
        zombieGroan[0] = new GreenfootSound("groan.wav");
        zombieGroan[1] = new GreenfootSound("groan2.wav");
        zombieGroan[2] = new GreenfootSound("groan3.wav");
        zombieGroan[3] = new GreenfootSound("groan4.wav");
        zombieGroan[4] = new GreenfootSound("groan5.wav");
        zombieGroan[5] = new GreenfootSound("lowgroan.wav");
        zombieGroan[6] = new GreenfootSound("lowgroan2.wav");
        zombieGroan[7] = new GreenfootSound("sukhbir.wav");
        zombieGroan[8] = new GreenfootSound("sukhbir2.wav");
        zombieGroan[9] = new GreenfootSound("sukhbir3.wav");
        zombieGroan[10] = new GreenfootSound("sukhbir4.wav");
        zombieGroan[11] = new GreenfootSound("sukhbir5.wav");
        zombieGroan[12] = new GreenfootSound("sukhbir6.wav");

        // Now initialize the values for the sodspacing arrays
        for (int i=0; i<5; i++) {
            for (int j=0; j<9; j++) {
                // Note the Offset values
                sodSpaceCentreX[i][j] = sodSpaceOffsetX + sodSpaceWidth * j;
                sodSpaceCentreY[i][j] = sodSpaceOffsetY + sodSpaceHeight * i;
            }
        }
        if (!this.has_levelled) askUsername(); // Ask the user for their username before starting
        zombies_types = new String[num_types_zombies + 1]; // Declare the size of the array
        action = 0;
        sunpoints = 150; //initial sun points
        addObject(sunCount, 157, 69); // Add the object to the world
        // addObject(score_counter, 710, 68);

        //play the chosen music
        if (Music_Settings.song1_pressed) frontYardMusic = new GreenfootSound("background.wav");
        else if (Music_Settings.song2_pressed) frontYardMusic = new GreenfootSound("03 Choose Your Seeds.mp3");
        else if (Music_Settings.song3_pressed) frontYardMusic = new GreenfootSound("05 Loonboon.mp3");
        else if (Music_Settings.song4_pressed) frontYardMusic = new GreenfootSound("06 Moongrains.mp3");
        else if (Music_Settings.song5_pressed) frontYardMusic = new GreenfootSound("10 Ultimate Battle.mp3");
        else if (Music_Settings.song6_pressed) frontYardMusic = new GreenfootSound("09 Watery Graves (fast).mp3");
        else music = new GreenfootSound("background.wav");
        
    }

    /**
     * 2nd Constructor for objects of class Backyard.
     * Make sure to ask the user for his username in this constructor
     * @param levelledup = has player levelled up (moving to next stage?)
     * @param score_num = previous recorded score
     */
    public Backyard (Sound_Info soundinfo, Player_Info playerinfo, int level_num, boolean levelledup, int score_num) {    
        // Create a new world with 1111x600 cells with a cell size of 1x1 pixels.
        super(1111, 600, 1, false);
        this.sound_info = soundinfo;
        this.player_info = playerinfo;
        this.level_idx = level_num;
        this.has_levelled = levelledup;
        this.score = score_num;
        if (this.player_info != null) {
            // Now make sure to tranfer over the field variables from the objects
            if (this.player_info.getDifficulty() > 0) {
                this.difficulty = player_info.getDifficulty();
            }
            if (this.player_info.getLevel() > 0) { // Only select a valid level
                this.level = player_info.getLevel();
            }
            if (this.difficulty == 1) this.easy = true;
            else if (this.difficulty == 2) this.medium = true;
            else if (this.difficulty == 3) this.hard = true;
        }
        int xcoor = 0, ycoor = 0;

        Greenfoot.setSpeed(GAME_SPEED);// Set the speed of the game (hardcode this in)

        screen = new Screen(blackScreen);
        // Add the screen
        addObject(screen, 556, 300);

        // Prepare all of the Greenfoot objects
        GenerateSounds();
        GenerateBackgrounds();

        // Make sure to set the correct paint order before beginning
        setPaintOrder(SeedPacketEndStage.class, Starburst.class, Screen.class, Sun.class, Sun1.class, AlertMessage.class, SBoard.class, Counter.class, Regular_Zombies.class, Frozen_Zombie.class, Seedpacket.class, SeedPacketTray.class, Regular_Pea.class, Plant.class);

        // Now set the dimensions for the 2-D arrays used for the backyard
        sodSpaceCentreX = new int[5][9];
        sodSpaceCentreY = new int[5][9];
        sodSpaceOccupied = new boolean[5][9];

        // Now initialize all of the lawnmowers needed
        mower = new Regular_Lawnmower[5];
        for (int i=0; i<5; i++) {
            mower[i] = new Regular_Lawnmower();
        }

        // Instantiate the seed packets (have to store this in a list preferably)
        peaShooterSeedPacket = new PeashooterSeedpacket();
        sunflowerSeedPacket = new SunflowerSeedpacket();
        icepeaShooterSeedPacket = new IcePeashooterSeedpacket();
        triplePeashooterSeedPacket = new TriplePeashooterSeedPacket();
        doublePeashooterSeedPacket = new DoublePeashooterSeedPacket();
        machinegunPeaShooterSeedPacket = new MachineGunSeedPacket();
        potatoSeedPacket = new PotatoSeedPacket();
        cherrybombSeedPacket = new CherryBombSeedPacket();

        Add_Seed_Packets(); // Now iitialize list of seedpackets

        // Declare size of GreenfootSound array
        // Now initialize contents of array 
        zombieGroan = new GreenfootSound[13];
        zombieGroan[0] = new GreenfootSound("groan.wav");
        zombieGroan[1] = new GreenfootSound("groan2.wav");
        zombieGroan[2] = new GreenfootSound("groan3.wav");
        zombieGroan[3] = new GreenfootSound("groan4.wav");
        zombieGroan[4] = new GreenfootSound("groan5.wav");
        zombieGroan[5] = new GreenfootSound("lowgroan.wav");
        zombieGroan[6] = new GreenfootSound("lowgroan2.wav");
        zombieGroan[7] = new GreenfootSound("sukhbir.wav");
        zombieGroan[8] = new GreenfootSound("sukhbir2.wav");
        zombieGroan[9] = new GreenfootSound("sukhbir3.wav");
        zombieGroan[10] = new GreenfootSound("sukhbir4.wav");
        zombieGroan[11] = new GreenfootSound("sukhbir5.wav");
        zombieGroan[12] = new GreenfootSound("sukhbir6.wav");

        // Now initialize the values for the sodspacing arrays
        for (int i=0; i<5; i++) {
            for (int j=0; j<9; j++) {
                // Note the Offset values
                sodSpaceCentreX[i][j] = sodSpaceOffsetX + sodSpaceWidth * j;
                sodSpaceCentreY[i][j] = sodSpaceOffsetY + sodSpaceHeight * i;
            }
        }

        if (!this.has_levelled) askUsername(); // Ask the user for their username before starting
        zombies_types = new String[num_types_zombies + 1]; // Declare the size of the array
        action = 0;
        sunpoints = 150;
        addObject(sunCount, 157, 69); // Add the object to the world => make sure it aligns with the seed packet tray
        // addObject(score_counter, 710, 68);

        //play chosen music
        if (Music_Settings.song1_pressed) frontYardMusic = new GreenfootSound("background.wav");
        else if (Music_Settings.song2_pressed) frontYardMusic = new GreenfootSound("03 Choose Your Seeds.mp3");
        else if (Music_Settings.song3_pressed) frontYardMusic = new GreenfootSound("05 Loonboon.mp3");
        else if (Music_Settings.song4_pressed) frontYardMusic = new GreenfootSound("06 Moongrains.mp3");
        else if (Music_Settings.song5_pressed) frontYardMusic = new GreenfootSound("10 Ultimate Battle.mp3");
        else if (Music_Settings.song6_pressed) frontYardMusic = new GreenfootSound("09 Watery Graves (fast).mp3");
        else music = new GreenfootSound("background.wav");

    }

    /**
     * Method is used to initialize the ArrayList seedpackets
     * NOTE: Refer to this method in the future when deciding to add more plants!
     * NOTE: The order of the cards matters!!!
     * We can also use HashSet instead
     */
    public void Add_Seed_Packets () {
        // Always have sunflower and peashooter as defaults
        this.seedpackets.add(peaShooterSeedPacket);
        this.seedpackets.add(sunflowerSeedPacket);
        this.seedpackets.add(potatoSeedPacket);
        this.seedpackets.add(cherrybombSeedPacket);
        this.seedpackets.add(doublePeashooterSeedPacket);
        this.seedpackets.add(triplePeashooterSeedPacket);
        this.seedpackets.add(machinegunPeaShooterSeedPacket);
        this.seedpackets.add(icepeaShooterSeedPacket);
    }

    /**
     * Method is used to add all of the current seed packets to the seed packet tray in the backyard
     */
    public void Add_Seed_Packets_To_Tray () {
        addObject(new SeedPacketTray(), 305, 44); // Make sure to first add the seed packet tray to the world before adding all of the seed packets
        Add_Seed_Packets(); // First add all of the seed packets first
        // Make sure to declare both coordinates before iterating through list and adding the objects
        int x_coor = 124, y_coor = 41;

        // Declare all of the boolean variables that are used
        boolean is_peashooter = false;
        boolean is_icepeashooter = false;
        boolean is_sunflower = false;
        boolean is_potato = false;
        boolean is_doublepeashooter = false;
        boolean is_triplepeashooter = false;
        boolean is_machinegunpeashooter = false;
        boolean is_cherrybomb = false;

        // Iterate through the list to add all of the objects
        if (level_idx < seedpackets.size()) {
            for (int i=0; i<level_idx; i++) {
                Seedpacket curr_seedpacket = seedpackets.get(i);
                // Check to see which instance/class the current object is of
                if (curr_seedpacket instanceof PeashooterSeedpacket) {
                    is_peashooter = true;
                    addObject((PeashooterSeedpacket) curr_seedpacket, x_coor, y_coor);
                    if (dictionary.get(curr_seedpacket) == null) {
                        dictionary.put((PeashooterSeedpacket) curr_seedpacket, true);
                    }
                }
                else if (curr_seedpacket instanceof IcePeashooterSeedpacket) {
                    is_icepeashooter = true;
                    addObject((IcePeashooterSeedpacket) curr_seedpacket, x_coor, y_coor);
                    if (dictionary.get(curr_seedpacket) == null) {
                        dictionary.put((IcePeashooterSeedpacket) curr_seedpacket, true);
                    }
                }
                else if (curr_seedpacket instanceof SunflowerSeedpacket) {
                    is_sunflower = true;
                    addObject((SunflowerSeedpacket) curr_seedpacket, x_coor, y_coor);
                    if (dictionary.get(curr_seedpacket) == null) {
                        dictionary.put((SunflowerSeedpacket) curr_seedpacket, true);
                    }
                }
                else if (curr_seedpacket instanceof PotatoSeedPacket) {
                    is_icepeashooter = true;
                    addObject((PotatoSeedPacket) curr_seedpacket, x_coor, y_coor);
                    if (dictionary.get(curr_seedpacket) == null) {
                        dictionary.put((PotatoSeedPacket) curr_seedpacket, true);
                    }
                }
                else if (curr_seedpacket instanceof DoublePeashooterSeedPacket) {
                    is_doublepeashooter = true;
                    addObject((DoublePeashooterSeedPacket) curr_seedpacket, x_coor, y_coor);
                    if (dictionary.get(curr_seedpacket) == null) {
                        dictionary.put((DoublePeashooterSeedPacket) curr_seedpacket, true);
                    }
                }
                else if (curr_seedpacket instanceof TriplePeashooterSeedPacket) {
                    is_triplepeashooter = true;
                    addObject((TriplePeashooterSeedPacket) curr_seedpacket, x_coor, y_coor);
                    if (dictionary.get(curr_seedpacket) == null) {
                        dictionary.put((TriplePeashooterSeedPacket) curr_seedpacket, true);
                    }
                }
                else if (curr_seedpacket instanceof MachineGunSeedPacket) {
                    is_machinegunpeashooter = true;
                    addObject((MachineGunSeedPacket) curr_seedpacket, x_coor, y_coor);
                    if (dictionary.get(curr_seedpacket) == null) {
                        dictionary.put((MachineGunSeedPacket) curr_seedpacket, true);
                    }
                }
                else if (curr_seedpacket instanceof CherryBombSeedPacket) {
                    is_cherrybomb = true;
                    addObject((CherryBombSeedPacket) curr_seedpacket, x_coor, y_coor);
                    if (dictionary.get(curr_seedpacket) == null) {
                        dictionary.put((CherryBombSeedPacket) curr_seedpacket, true);
                    }
                }
                // Now update the contents of the hashmap, to check which seedpacket the user has availabe towards them
                x_coor += 60; // Make sure to increment the current x-coordinate by 60 => approximate spacing
            }
        }
    }

    private long lastTime = System.currentTimeMillis(); // Store the last recorded time in milliseconds

    /**
     * Method is actually used -> different from the other methods that are used for the blitz mode
     */
    public void setUpStage () {
        // Set up all of the seed packets, seed packet tray, and sun labels
        Add_Seed_Packets_To_Tray(); // Now add all of the seed packets that are currently available to the user at this stage in the game
        // Initialize the lawnmowers
        // Make sure the lawnmowers are perfectly aligned (vertically) with the backyard
        if (getObjects(Regular_Lawnmower.class).isEmpty()) {
            Add_Lawnmowers();
        }
        addObject(score_counter, 645, 63);
        
        
        // Initialize the alert message to be displayed
        alertMessage = new AlertMessage(ALERT_READY);
        addObject(alertMessage, 400, 450); // Now add it to the world
        Greenfoot.playSound("readysetplant_thump.wav"); // Now play the sound effect
    }

    /**
     * Method is used to add lawnmowers into the world
     */
    public void Add_Lawnmowers () {
        addObject(mower[0], 10, 125);
        addObject(mower[1], 10, 225);
        addObject(mower[2], 10, 325);
        addObject(mower[3], 10, 425);
        addObject(mower[4], 10, 525);
    }

    /**
     * Method is used to generate a new zombie wave at set intervals, depending on the difficulty level
     * Randomly generates and/or adds zombies to the world (with random x and y coordinates)
     */
    public void Generate_Zombie_Wave () {
        addObject(new ZombiesAreComing(), 400, 300);
        int num_zombies = 0; // For the number of zombies to spawn
        if (this.easy) {
            num_zombies = 4; // Start off with 4 only
        }
        else if (this.medium) {
            num_zombies = 5; // Start off with 5 only
        }
        else if (this.hard) {
            num_zombies = 6; // Start off with 6 only
        }
        num_zombies += level_idx; // Now increment the number of zombies by the current stage user is on

        int xcoor = 0, ycoor = 0; // Declare x and y coordinates needed for zombie placement in world
        int last_rand_x = xcoor; // Declare the last recorded random x coordinate

        for (int i=0; i<num_zombies; i++) {
            int rand_int = rng.nextInt(num_types_zombies) + 1;
            int rand_y = rng.nextInt(5) + 1; // Used for the y-coordinate
            int rand_x = rng.nextInt(300) + 1111; // For random x-coordinate spawning
            // If they're too close together, adjust accordingly
            if (Math.abs(rand_x - last_rand_x) <= 80) { // Have a spacing of 80 for now
                // Always increment the bigger value
                if (rand_x > last_rand_x) rand_x += Math.abs(rand_x - last_rand_x) + 5;
                else last_rand_x += Math.abs(rand_x - last_rand_x) + 5; 
            }
            if (Math.abs(rand_x - last_rand_x) > 80) { // Have a spacing of 80 for now
                if (rand_int == 1) { // Bucket zombie
                    Bucketzombie bucket_zombie = new Bucketzombie(this.sound_info, this.player_info, this.difficulty, this.level_idx, this.done_waves);
                    if (rand_y == 1) {
                        addObject(bucket_zombie, rand_x, 100);
                    }
                    else if (rand_y == 2) {
                        addObject(bucket_zombie, rand_x, 200);
                    }
                    else if (rand_y == 3) {
                        addObject(bucket_zombie, rand_x, 300);
                    }
                    else if (rand_y == 4) {
                        addObject(bucket_zombie, rand_x, 400);
                    }
                    else if (rand_y == 5) {
                        addObject(bucket_zombie, rand_x, 500);
                    }
                }
                else if (rand_int == 2) { // Normal Normalzombie
                    Normalzombie normal_zombie = new Normalzombie(this.sound_info, this.player_info, this.difficulty, this.level_idx, this.done_waves);
                    if (rand_y == 1) {
                        addObject(normal_zombie, rand_x, 100);
                    }
                    else if (rand_y == 2) {
                        addObject(normal_zombie, rand_x, 200);
                    }
                    else if (rand_y == 3) {
                        addObject(normal_zombie, rand_x, 300);
                    }
                    else if (rand_y == 4) {
                        addObject(normal_zombie, rand_x, 400);
                    }
                    else if (rand_y == 5) {
                        addObject(normal_zombie, rand_x, 500);
                    }
                }
                else if (rand_int == 3) { // Cone zombie
                    Conezombie cone_zombie = new Conezombie(this.sound_info, this.player_info, this.difficulty, this.level_idx, this.done_waves);
                    if (rand_y == 1) {
                        addObject(cone_zombie, rand_x, 100);
                    }
                    else if (rand_y == 2) {
                        addObject(cone_zombie, rand_x, 200);
                    }
                    else if (rand_y == 3) {
                        addObject(cone_zombie, rand_x, 300);
                    }
                    else if (rand_y == 4) {
                        addObject(cone_zombie, rand_x, 400);
                    }
                    else if (rand_y == 5) {
                        addObject(cone_zombie, rand_x, 500);
                    }
                }
                else if (rand_int == 4) { // Cone zombie
                    Babyzombie baby_zombie = new Babyzombie(this.sound_info, this.player_info, this.difficulty, this.level_idx, this.done_waves);
                    if (rand_y == 1) {
                        addObject(baby_zombie, rand_x, 100);
                    }
                    else if (rand_y == 2) {
                        addObject(baby_zombie, rand_x, 200);
                    }
                    else if (rand_y == 3) {
                        addObject(baby_zombie, rand_x, 300);
                    }
                    else if (rand_y == 4) {
                        addObject(baby_zombie, rand_x, 400);
                    }
                    else if (rand_y == 5) {
                        addObject(baby_zombie, rand_x, 500);
                    }
                }
            }
            last_rand_x = rand_x; // Record the last random x coordinate to check for spacing
        }
    }

    /**
     * Method is used to start the initial stage
     */
    public void startStage () {
        musicVolume -= 1;
        musicVolume = Math.max(0, musicVolume); // Ensure that the volume doesn't go below 0
        if (musicVolume > 0) music.setVolume(musicVolume);
        else {
            music.setVolume(0);
            music.stop();
        }

        if (stage_CycleCnt == 40) {
            // TODO: Probably should make this a GreenfootSound object
            Greenfoot.playSound("readysetplant_thump.wav");
            alertMessage.setImgMessage(ALERT_SET);
        }   
        else if (stage_CycleCnt == 80) {
            // TODO: Probably should make this a GreenfootSound object too
            Greenfoot.playSound("readysetplant_boom.wav");
            alertMessage.setImgMessage(ALERT_PLANT);
        }  
        else if (stage_CycleCnt == 150) {
            removeObject(alertMessage);
            gameStatus = BEGIN_PLAY;
            // Now initialize the music for the stage
            music = frontYardMusic;
            music.setVolume(100); 
            // music.playLoop();
        }
    }

    /**
     * Method is used to randomly spawn the sunlight at certain time intervals
     * Use SimpleTimer class for calculating the time intervals
     */
    public void Spawn_Sunlight () {
        Sun sun = new Sun("regular");
        addObject(sun, cols[rng.nextInt(8)], 0);
        sun.Drop_Sun(); // Call the method contained within the class: Sun
    }

    /**
     * Method is used to update the boolean plant_removed
     * If a plant has been removed, make sure to set the coordinates in the 2d boolean array to false
     */
    public void update_RemovedStatus () {
        if (shovel != null) {
            this.plant_removed = shovel.getRemoved();
            int row_ans = 0, col_ans = 0;
            int row_removed = -1, col_removed = -1; // For storing the removed coordinates of the plant
            if (shovel.getRemovedRow() >= 0) row_removed = shovel.getRemovedRow();
            if (shovel.getRemovedCol() >= 0) col_removed = shovel.getRemovedCol();
            // Now make sure to update the boolean array
            if (row_removed != -1 && col_removed != -1) {
                // First get the ACTUAL row and column first before updating the boolean array
                double best_dist = Double.MAX_VALUE; // initialize this to some large value (large enough never to be exceeded)
                double curr_dist = 0.0; // Initialize to zero
                // We can use a nested for loop to search for the nearest sodSpace center point (relative to the location of the mouse click)
                for ( int i=0; i<5; i++) {
                    for (int j=0; j<9; j++) {
                        // Compute the Euclidean distance between the mouse click point and the current sodSpace center point (for row i and column j)
                        curr_dist = Math.sqrt(Math.pow(Math.abs(row_removed - sodSpaceCentreX[i][j]), 2) + Math.pow(Math.abs(col_removed - sodSpaceCentreY[i][j]), 2));
                        // Updates the location of the closest sodSpace center 
                        // (Note there is no else clause... instead we just keep iterating and updating until we have traversed the entire 2-D array!)
                        if (curr_dist < best_dist) {
                            // Now update values of local variables
                            best_dist = curr_dist;
                            // Update row and column location of the sodSpace center that is the closest of those searched so far 
                            row_ans = i;
                            col_ans = j;
                        }
                    }
                }
                sodSpaceOccupied[row_ans][col_ans] = false;
            }
        }
    }

    /**
     * Method adds a random zombie to the world onto the specified row
     * NOTE: Used for the blitz mode, not regular mode
     */
    public void Randomize_Zombie (int row) {
        if (rng.nextInt(2) == 0) Add_Zombie(new Normal_Zombie(this.sound_info, this.player_info, "blitz"), row);
        else Add_Zombie(new Football_Zombie(this.sound_info, this.player_info, "blitz"), row);
    }

    /**
     * Method is used to check whether the player has lost the game
     * Occurs when the zombies enter the house (once the lawnmowers have been removed)
     */
    public void Check_Loss () {
        // First check to see if the user has already lost
        List<Regular_Zombies> zombies_rem = getObjects(Regular_Zombies.class); // Get a list of all the remaining zombies
        if (!zombies_rem.isEmpty()) {
            Regular_Zombies zombie_init = zombies_rem.get(0);
            for (int i=1; i<=zombies_rem.size(); i++) {
                Regular_Zombies zombies = zombies_rem.get(i); // Now get all of the other zombies
                // Check to see if any zombie object has entered the house
                if (zombies.getX() < 5 || zombie_init.getX() < 5) {
                    this.has_lost = true;
                    break;
                }
            }
        }        
        if (has_lost) {
            // Play the lose music
            lose_music.play();
            scream.play(); // Play the sound effect (make sure that the two sounds don't overlap)
            if (player_info == null) {
                Update_Player_Info();
            }
            write_Scores(player_info); // Make sure to write the score to the text file first
            if (this.score > 0) Greenfoot.setWorld(new GameOver(this.sound_info, this.player_info)); // Only set the game over screen if the user has a positive score
        }
    }

    /**
     * Use this method to write the Player information to the scores.txt file
     */
    public void write_Scores (Player_Info info) {
        reading_writing_files_1.Write(info); // Write the info to the file
    }

    /**
     * Method asks the player for his/her username at the start of the game using jFrame
     */
    public void askUsername () {
        username = JOptionPane.showInputDialog("Please input your username");
    }

    /**
     * Method is used to generate new waves of zombies
     */ 
    public void Generate_Wave () {
        // Use a for loop to loop through all of the 5 rows
        for (int i=0; i<5; i++) Randomize_Zombie(i);
    }

    /**
     * Use this to generate all of the upcoming sounds into the stack/queue
     * Only store the objects if it is currently empty
     */
    public void GenerateSounds () {
        if (sounds.isEmpty()) {
            sounds.push(new GreenfootSound(sound1));
            sounds.push(new GreenfootSound(sound2));
            sounds.push(new GreenfootSound(sound3));
        }
    }

    /**
     * Use this to generate all of the upcoming backgrounds into the stack/queue
     */
    public void GenerateBackgrounds () {
        if (images.isEmpty()) {
            images.enqueue(new GreenfootImage(background1));
            images.enqueue(new GreenfootImage(background2));
            images.enqueue(new GreenfootImage(background3));
        }
    }

    /**
     * getCounters
     */
    public Counter Get_SunCounter() {
        return this.sun_counter;
    }

    /**
     * getScoreCounters
     */
    public Counter Get_ScoreCounter() {
        return this.score_counter;
    }

    /**
     * Set Coordinates
     */
    public void Set_Coordinates () {
        rows = new int[5];
        rows[0] = 78;
        rows[1] = 184;
        rows[2] = 299;
        rows[3] = 405;
        rows[4] = 515;
        cols = new int[9];
        cols[0] = 326;
        cols[1] = 413;
        cols[2] = 500;
        cols[3] = 588;
        cols[4] = 679;
        cols[5] = 765;
        cols[6] = 857;
        cols[7] = 943;
        cols[8] = 1042;
    }

    /**
     * Method is used to stop the background music
     */
    public void stopBackgroundMusic () {
        this.music.stop();
    }

    /**
     * Method is used to get the row position of the backyard
     */
    public int Get_Row_Position (int y_coor) {
        int [] row_grid = {0, 137, 246, 357, 462, 569};
        for (int i=0; i<5; i++) {
            if (y_coor > row_grid[i] && y_coor < row_grid[i + 1]) return rows[i];
        }
        return -1;
    }

    /**
     * Method is used to get the column position of the backyard
     */
    public int Get_Col_Position (int x) {
        int [] col_grid = {280, 364, 449, 543, 632, 721, 812, 897, 985, 1089};
        for (int i=0; i<9; i++) {
            if (x > col_grid[i] && x < col_grid[i + 1]) return cols[i];
        }
        return -1;
    }

    /**
     * Method is used to set the lawnmowers on the specified positions in the backyard
     */
    public void Set_Lawnmowers () {
        int [] r = {77, 183, 309, 422, 522};
        int [] c = {242, 240, 226, 219, 214};
        for (int i=0; i<5; i++) addObject(new Lawnmower(), c[i], r[i]);
    }

    /**
     * Method is used to set the sidebar in the backyard
     */
    public void Set_Sidebar () {
        addObject(score_counter, 49, 78);
        addObject(sun_counter, 49, 567);
        sun_counter.setValue(50);
        addObject(new WalnutIcon("regular", 49,140), 49, 140);
        addObject(new PeaShootIcon("regular", 49, 236), 49, 236);
        addObject(new SunFlowerIcon("regular", 49, 332), 49, 332);
        addObject(new BeetRootIcon("regular", 49, 428), 49, 428);
    }

    /**
     * Method is used to add the zombie object to the specified row in the backyard
     */
    public void Add_Zombie (Zombies zombie, int row) {
        addObject(zombie, cols[8] + 70, rows[row]);
    }

    /**
     * Method is used to update the information that is stored in the Player_Info class
     * Use the field variables from other Actor classes -> (e.g.: score);
     */
    public void Update_Player_Info () {
        // System.out.println(this.player_info.getScore());
        player_info = new Player_Info(this.username, this.score, this.level, this.difficulty); // Use THIS for regular mode
    }

    /**
     * Updates the status of whether or not the sunflower's seed packet has been clicked
     * Used along with the other helper methods to set and/or check for the status of each seed packet
     */
    public void setSunflowerSeedPacketClickStatus (boolean status) {
        sunflowerSeedPacketClicked = status;
    }

    /**
     * Updates the status of whether or not the peashooter's seed packet has been clicked
     * Used along with the other helper methods to set and/or check for the status of each seed packet
     */
    public void setPeaShooterSeedPacketClickStatus (boolean status) {
        peashooterSeedPacketClicked = status;
    }

    /**
     * Updates the status of whether or not the ice peashooter's seed packet has been clicked
     * Used along with the other helper methods to set and/or check for the status of each seed packet
     */
    public void setIcePeaShooterSeedPacketClickStatus (boolean status) {
        icepeashooterSeedPacketClicked = status;
    }

    /**
     * Updates the status of whether or not the double peashooter's seed packet has been clicked
     * Used along with the other helper methods to set and/or check for the status of each seed packet
     */
    public void setDoublePeaShooterSeedPacketClickStatus (boolean status) {
        doublepeashooterSeedPacketClicked = status;
    }

    /**
     * Updates the status of whether or not the tripe peashooter's seed packet has been clicked
     * Used along with the other helper methods to set and/or check for the status of each seed packet
     */
    public void setTriplePeaShooterSeedPacketClickStatus (boolean status) {
        triplepeashooterSeedPacketClicked = status;
    }

    /**
     * Updates the status of whether or not the machine gun peashooter's seed packet has been clicked
     * Used along with the other helper methods to set and/or check for the status of each seed packet
     */
    public void setMachineGunPeaShooterSeedPacketClickStatus (boolean status) {
        machinegunpeashooterSeedPacketClicked = status;
    }

    /**
     * Updates the status of whether or not the potato's seed packet has been clicked
     * Used along with the other helper methods to set and/or check for the status of each seed packet
     */
    public void setPotatoSeedPacketClickStatus (boolean status) {
        potatoSeedPacketClicked = status;
    }

    /**
     * Updates the status of whether or not the cherry bomb's seed packet has been clicked
     * Used along with the other helper methods to set and/or check for the status of each seed packet
     */
    public void setCherryBombSeedPacketClickStatus (boolean status) {
        cherrybombSeedPacketClicked = status;
    }

    /**
     * Updates the sun points text field after a sun has been clicked
     */
    public void addSunPoints (int pointsAdded) {
        sunpoints += pointsAdded;
        if (fallingSunAlertAlreadyDisplayed && !fallingSunAlertRemoved) {
            removeObject(alertMessage);
            fallingSunAlertRemoved = true; // Now make sure to remove the sun alert message
        }
    }

    /**
     * Method for checking whether a peashooter card has been clicked
     */
    public void checkForPeaShooterPacketClicked () {

        if (peashooterSeedPacketClicked && sunpoints >= 100) {
            if (Greenfoot.mouseClicked(this)) {
                // Local variables for storing the location of the last mouse click
                int mouseClickedX = Greenfoot.getMouseInfo().getX();
                int mouseClickedY = Greenfoot.getMouseInfo().getY();
                // Use a O(N^2) algorithm to find the closest sodSpace centre location (relative to mouse click).
                double bestDist = Double.MAX_VALUE; // Best distance (closest)
                double currDist = 0.0; // Store current distance
                int best_row = 0, best_col = 0; // Store best coordinates for both rows and columns
                for (int i=0; i<5; i++) {
                    for (int j=0; j<9; j++) {
                        // Compute the Euclidean distance between the mouse click point and the current sodspace center point
                        currDist = Math.sqrt(Math.pow(mouseClickedX - sodSpaceCentreX[i][j], 2) + Math.pow(mouseClickedY - sodSpaceCentreY[i][j], 2));
                        // Update the location of the closest sodspace center
                        if (currDist < bestDist) {
                            bestDist = currDist;
                            best_row = i;
                            best_col = j;
                        }
                    }
                }
                // If the sod space isn't currently occupied at the moment by a peashooter
                // If it is, then add a new PeaShooter object to the world and update the boolean arrays
                if (!sodSpaceOccupied[best_row][best_col]) {
                    addObject(new Regular_Peashooter(this.level_idx), sodSpaceCentreX[best_row][best_col], sodSpaceCentreY[best_row][best_col]);
                    sodSpaceOccupied[best_row][best_col] = true;
                    Greenfoot.playSound("plant.wav"); // Play this sound effect
                    // Decrease the sunpoints accordingly
                    addSunPoints(-100); // 

                    // Now have to reset the seed packets
                    peashooterSeedPacketClicked = false;
                    List<PeashooterSeedpacket> peaShooterSeedPacketList = getObjects(PeashooterSeedpacket.class);
                    for (PeashooterSeedpacket packet : peaShooterSeedPacketList) {
                        packet.resetImage();
                    }

                }
            }
        }
    }

    /**
     * Method is used to check for any sunflower packets that have been clicked
     */
    public void checkForSunflowerPacketClicked () {
        if (sunflowerSeedPacketClicked && sunpoints >= 50) {
            if (Greenfoot.mouseClicked(this)) {
                int mouseClickX = Greenfoot.getMouseInfo().getX();
                int mouseClickY = Greenfoot.getMouseInfo().getY();
                // We need to search for the closest sodSpace center location (relative to our mouse click).
                // To do this, we first need to set up some local variables...
                double best_dist = Double.MAX_VALUE; // initialize this to some large value (large enough never to be exceeded)
                double curr_dist = 0.0; // initialize to zero
                int best_row = 0, best_col = 0; // Store best coordinates for both rows and columns

                // We can use a nested for loop to search for the nearest sodSpace center point
                // (relative to the location of the mouse click)
                for ( int i=0; i<5 ; i++) {
                    for (int j=0; j<9 ; j++) {
                        // Compute the Euclidean distance between the mouse click point and the current sodSpace center point (for row i and column j)
                        curr_dist = Math.sqrt(Math.pow(Math.abs(mouseClickX - sodSpaceCentreX[i][j]), 2) + Math.pow(Math.abs(mouseClickY - sodSpaceCentreY[i][j]), 2));
                        // Updates the location of the closest sodSpace center 
                        // (Note there is no else clause... instead we just keep iterating and updating until we have traversed the entire 2-D array!)
                        if (curr_dist < best_dist) {
                            // update values of local variables
                            best_dist = curr_dist;
                            // update row and column location of the sodSpace center
                            // that is the closest of those searched so far 
                            best_row = i;
                            best_col = j;
                        }
                    }
                }
                // Now that we have determined which sodSpace center is closest to where you clicked,
                // place the object at that position, but first check to see if any plant has already 
                // been placed at this point; if not, then mark the position accordingly
                if (!sodSpaceOccupied[best_row][best_col]) {
                    addObject(new Regular_Sunflower(this.level_idx), sodSpaceCentreX[best_row][best_col], sodSpaceCentreY[best_row][best_col]);
                    Greenfoot.playSound("plant2.wav");
                    sodSpaceOccupied[best_row][best_col] = true;
                    // Decrease sunpoints accordingly
                    addSunPoints(-50);
                    // Now reset seed packet
                    sunflowerSeedPacketClicked = false;
                    List<SunflowerSeedpacket> sunflowerSeedPacketList = getObjects(SunflowerSeedpacket.class);
                    for (SunflowerSeedpacket thisSeedPacket : sunflowerSeedPacketList) thisSeedPacket.resetImage();
                }
            }
        }
    }

    /**
     * Method for checking whether a icepeashooter card has been clicked
     * Logic should be similar to both the PeaShooter and Sunflower methods from above
     */
    public void checkForIcePeaShooterPacketClicked () {
        if (icepeashooterSeedPacketClicked && sunpoints >= 175) { // Make sure the sunpoints is >= the cost of the plant
            if (Greenfoot.mouseClicked(this)) {
                int mouseClickX = Greenfoot.getMouseInfo().getX();
                int mouseClickY = Greenfoot.getMouseInfo().getY();
                // We need to search for the closest sodSpace center location (relative to our mouse click).
                // To do this, we first need to set up some local variables...
                double best_dist = Double.MAX_VALUE; // initialize this to some large value (large enough never to be exceeded)
                double curr_dist = 0.0; // initialize to zero
                int best_row = 0, best_col = 0; // Store best coordinates for both rows and columns

                // We can use a nested for loop to search for the nearest sodSpace center point
                // (relative to the location of the mouse click)
                for ( int i=0; i<5 ; i++) {
                    for (int j=0; j<9 ; j++) {
                        // Compute the Euclidean distance between the mouse click point and the current sodSpace center point (for row i and column j)
                        curr_dist = Math.sqrt(Math.pow(Math.abs(mouseClickX - sodSpaceCentreX[i][j]), 2) + Math.pow(Math.abs(mouseClickY - sodSpaceCentreY[i][j]), 2));
                        // Updates the location of the closest sodSpace center 
                        // (Note there is no else clause... instead we just keep iterating and updating until we have traversed the entire 2-D array!)
                        if (curr_dist < best_dist) {
                            // update values of local variables
                            best_dist = curr_dist;
                            // update row and column location of the sodSpace center
                            // that is the closest of those searched so far 
                            best_row = i;
                            best_col = j;
                        }
                    }
                }
                // Now that we have determined which sodSpace center is closest to where you clicked,
                // place the object at that position, but first check to see if any plant has already 
                // been placed at this point; if not, then mark the position accordingly
                if (!sodSpaceOccupied[best_row][best_col]) {
                    // Simply instantiate a new instance of the ice pea shooter class
                    addObject(new IcePeaShooter(this.level_idx), sodSpaceCentreX[best_row][best_col], sodSpaceCentreY[best_row][best_col]);
                    Greenfoot.playSound("plant2.wav");
                    sodSpaceOccupied[best_row][best_col] = true;
                    // Decrease sunpoints accordingly
                    addSunPoints(-175);
                    // Now reset seed packets
                    icepeashooterSeedPacketClicked = false;
                    List<IcePeashooterSeedpacket> icepeashooterSeedPacketList = getObjects(IcePeashooterSeedpacket.class);
                    for (IcePeashooterSeedpacket thisSeedPacket : icepeashooterSeedPacketList) thisSeedPacket.resetImage();
                }
            }
        }
    }

    /**
     * Method for checking whether a machinegunpeashooter card has been clicked
     */
    public void checkForMachineGunPeaShooterPacketClicked () {
        if (machinegunpeashooterSeedPacketClicked && sunpoints >= 250) {
            if (Greenfoot.mouseClicked(this)) {
                int mouseClickX = Greenfoot.getMouseInfo().getX();
                int mouseClickY = Greenfoot.getMouseInfo().getY();
                // We need to search for the closest sodSpace center location (relative to our mouse click).
                // To do this, we first need to set up some local variables...
                double best_dist = Double.MAX_VALUE; // initialize this to some large value (large enough never to be exceeded)
                double curr_dist = 0.0; // initialize to zero
                int best_row = 0, best_col = 0; // Store best coordinates for both rows and columns

                // We can use a nested for loop to search for the nearest sodSpace center point
                // (relative to the location of the mouse click)
                for ( int i=0; i<5 ; i++) {
                    for (int j=0; j<9 ; j++) {
                        // Compute the Euclidean distance between the mouse click point and the current sodSpace center point (for row i and column j)
                        curr_dist = Math.sqrt(Math.pow(Math.abs(mouseClickX - sodSpaceCentreX[i][j]), 2) + Math.pow(Math.abs(mouseClickY - sodSpaceCentreY[i][j]), 2));
                        // Updates the location of the closest sodSpace center 
                        // (Note there is no else clause... instead we just keep 
                        // iterating and updating until we have traversed the entire 2-D array!)
                        if (curr_dist < best_dist) {
                            // update values of local variables
                            best_dist = curr_dist;
                            // update row and column location of the sodSpace center
                            // that is the closest of those searched so far 
                            best_row = i;
                            best_col = j;
                        }
                    }
                }
                // Now that we have determined which sodSpace center is closest to where you clicked,
                // place the object at that position, but first check to see if any plant has already 
                // been placed at this point; if not, then mark the position accordingly
                if (!sodSpaceOccupied[best_row][best_col]) {
                    addObject(new MachineGunPeaShooter(this.level_idx), sodSpaceCentreX[best_row][best_col], sodSpaceCentreY[best_row][best_col]);
                    Greenfoot.playSound("plant2.wav");
                    sodSpaceOccupied[best_row][best_col] = true;
                    // Decrease sunpoints accordingly
                    addSunPoints(-250);
                    // Now reset seed packet
                    machinegunpeashooterSeedPacketClicked = false;
                    List<MachineGunSeedPacket> machinegunSeedPacketList = getObjects(MachineGunSeedPacket.class);
                    for (MachineGunSeedPacket thisSeedPacket : machinegunSeedPacketList) thisSeedPacket.resetImage();
                }
            }
        }
    }

    /**
     * Method for checking whether a doublepeashooter card has been clicked
     */
    public void checkForDoublePeaShooterPacketClicked () {
        if (doublepeashooterSeedPacketClicked && sunpoints >= 200) {
            if (Greenfoot.mouseClicked(this)) {
                int mouseClickX = Greenfoot.getMouseInfo().getX();
                int mouseClickY = Greenfoot.getMouseInfo().getY();
                // We need to search for the closest sodSpace center location (relative to our mouse click).
                // To do this, we first need to set up some local variables...
                double best_dist = Double.MAX_VALUE; // initialize this to some large value (large enough never to be exceeded)
                double curr_dist = 0.0; // initialize to zero
                int best_row = 0, best_col = 0; // Store best coordinates for both rows and columns

                // We can use a nested for loop to search for the nearest sodSpace center point
                // (relative to the location of the mouse click)
                for ( int i=0; i<5 ; i++) {
                    for (int j=0; j<9 ; j++) {
                        // Compute the Euclidean distance between the mouse click point and the current sodSpace center point (for row i and column j)
                        curr_dist = Math.sqrt(Math.pow(Math.abs(mouseClickX - sodSpaceCentreX[i][j]), 2) + Math.pow(Math.abs(mouseClickY - sodSpaceCentreY[i][j]), 2));
                        // Updates the location of the closest sodSpace center 
                        // (Note there is no else clause... instead we just keep iterating and updating until we have traversed the entire 2-D array!)
                        if (curr_dist < best_dist) {
                            // update values of local variables
                            best_dist = curr_dist;
                            // update row and column location of the sodSpace center
                            // that is the closest of those searched so far 
                            best_row = i;
                            best_col = j;
                        }
                    }
                }
                // Now that we have determined which sodSpace center is closest to where you clicked,
                // place the object at that position, but first check to see if any plant has already 
                // been placed at this point; if not, then mark the position accordingly
                if (!sodSpaceOccupied[best_row][best_col]) {
                    addObject(new DoublePeaShooter(this.level_idx), sodSpaceCentreX[best_row][best_col], sodSpaceCentreY[best_row][best_col]);
                    Greenfoot.playSound("plant2.wav");
                    sodSpaceOccupied[best_row][best_col] = true;
                    // Decrease sunpoints accordingly
                    addSunPoints(-200);
                    // Now reset seed packet
                    doublepeashooterSeedPacketClicked = false;
                    List<DoublePeashooterSeedPacket> doublepeashooterSeedPacketList = getObjects(DoublePeashooterSeedPacket.class);
                    for (DoublePeashooterSeedPacket thisSeedPacket : doublepeashooterSeedPacketList) thisSeedPacket.resetImage();
                }
            }
        }
    }

    /**
     * Method for checking whether a triplepeashooter card has been clicked
     */
    public void checkForTriplePeaShooterPacketClicked () {
        if (triplepeashooterSeedPacketClicked && sunpoints >= 325) {
            if (Greenfoot.mouseClicked(this)) {
                int mouseClickX = Greenfoot.getMouseInfo().getX();
                int mouseClickY = Greenfoot.getMouseInfo().getY();
                // We need to search for the closest sodSpace center location (relative to our mouse click).
                // To do this, we first need to set up some local variables...
                double best_dist = Double.MAX_VALUE; // initialize this to some large value (large enough never to be exceeded)
                double curr_dist = 0.0; // initialize to zero
                int best_row = 0, best_col = 0; // Store best coordinates for both rows and columns

                // We can use a nested for loop to search for the nearest sodSpace center point
                // (relative to the location of the mouse click)
                for ( int i=0; i<5 ; i++) {
                    for (int j=0; j<9 ; j++) {
                        // Compute the Euclidean distance between the mouse click point and the current sodSpace center point (for row i and column j)
                        curr_dist = Math.sqrt(Math.pow(Math.abs(mouseClickX - sodSpaceCentreX[i][j]), 2) + Math.pow(Math.abs(mouseClickY - sodSpaceCentreY[i][j]), 2));
                        // Updates the location of the closest sodSpace center 
                        // (Note there is no else clause... instead we just keep iterating and updating until we have traversed the entire 2-D array!)
                        if (curr_dist < best_dist) {
                            // update values of local variables
                            best_dist = curr_dist;
                            // update row and column location of the sodSpace center
                            // that is the closest of those searched so far 
                            best_row = i;
                            best_col = j;
                        }
                    }
                }
                // Now that we have determined which sodSpace center is closest to where you clicked,
                // place the object at that position, but first check to see if any plant has already 
                // been placed at this point; if not, then mark the position accordingly
                if (!sodSpaceOccupied[best_row][best_col]) {
                    addObject(new TriplePeaShooter(this.level_idx), sodSpaceCentreX[best_row][best_col], sodSpaceCentreY[best_row][best_col]);
                    Greenfoot.playSound("plant2.wav");
                    sodSpaceOccupied[best_row][best_col] = true;
                    // Decrease sunpoints accordingly
                    addSunPoints(-325);
                    // Now reset seed packet
                    triplepeashooterSeedPacketClicked = false;
                    List<TriplePeashooterSeedPacket> triplepeaShooterSeedPacketList = getObjects(TriplePeashooterSeedPacket.class);
                    for (TriplePeashooterSeedPacket packet : triplepeaShooterSeedPacketList) {
                        packet.resetImage();
                    }
                }
            }
        }
    }

    /**
     * Method for checking whether a potato card has been clicked
     */
    public void checkForPotatoPacketClicked () {
        if (potatoSeedPacketClicked && sunpoints >= 50) {
            if (Greenfoot.mouseClicked(this)) {
                int mouseClickX = Greenfoot.getMouseInfo().getX();
                int mouseClickY = Greenfoot.getMouseInfo().getY();
                // We need to search for the closest sodSpace center location (relative to our mouse click).
                // To do this, we first need to set up some local variables...
                double best_dist = Double.MAX_VALUE; // initialize this to some large value (large enough never to be exceeded)
                double curr_dist = 0.0; // initialize to zero
                int best_row = 0, best_col = 0; // Store best coordinates for both rows and columns

                // We can use a nested for loop to search for the nearest sodSpace center point
                // (relative to the location of the mouse click)
                for ( int i=0; i<5 ; i++) {
                    for (int j=0; j<9 ; j++) {
                        // Compute the Euclidean distance between the mouse click point and the current sodSpace center point (for row i and column j)
                        curr_dist = Math.sqrt(Math.pow(Math.abs(mouseClickX - sodSpaceCentreX[i][j]), 2) + Math.pow(Math.abs(mouseClickY - sodSpaceCentreY[i][j]), 2));
                        // Updates the location of the closest sodSpace center 
                        // (Note there is no else clause... instead we just keep iterating and updating until we have traversed the entire 2-D array!)
                        if (curr_dist < best_dist) {
                            // update values of local variables
                            best_dist = curr_dist;
                            // update row and column location of the sodSpace center
                            // that is the closest of those searched so far 
                            best_row = i;
                            best_col = j;
                        }
                    }
                }
                // Now that we have determined which sodSpace center is closest to where you clicked,
                // place the object at that position, but first check to see if any plant has already 
                // been placed at this point; if not, then mark the position accordingly
                if (!sodSpaceOccupied[best_row][best_col]) {
                    addObject(new Potato(this.level_idx), sodSpaceCentreX[best_row][best_col], sodSpaceCentreY[best_row][best_col]);
                    Greenfoot.playSound("plant2.wav");
                    sodSpaceOccupied[best_row][best_col] = true;
                    // Decrease sunpoints accordingly
                    addSunPoints(-50);
                    // Now reset seed packet
                    potatoSeedPacketClicked = false;
                    List<PotatoSeedPacket> potatoSeedPacketList = getObjects(PotatoSeedPacket.class);
                    for (PotatoSeedPacket thisSeedPacket : potatoSeedPacketList) thisSeedPacket.resetImage();
                }
            }
        }
    }

    /**
     * Method for checking whether a cherry bomb card has been clicked
     */
    public void checkForCherryBombPacketClicked () {
        if (cherrybombSeedPacketClicked && sunpoints >= 150) {
            if (Greenfoot.mouseClicked(this)) {
                int mouseClickX = Greenfoot.getMouseInfo().getX();
                int mouseClickY = Greenfoot.getMouseInfo().getY();
                // We need to search for the closest sodSpace center location (relative to our mouse click).
                // To do this, we first need to set up some local variables...
                double best_dist = Double.MAX_VALUE; // initialize this to some large value (large enough never to be exceeded)
                double curr_dist = 0.0; // initialize to zero
                int best_row = 0, best_col = 0; // Store best coordinates for both rows and columns

                // We can use a nested for loop to search for the nearest sodSpace center point
                // (relative to the location of the mouse click)
                for ( int i=0; i<5 ; i++) {
                    for (int j=0; j<9 ; j++) {
                        // Compute the Euclidean distance between the mouse click point and the current sodSpace center point (for row i and column j)
                        curr_dist = Math.sqrt(Math.pow(Math.abs(mouseClickX - sodSpaceCentreX[i][j]), 2) + Math.pow(Math.abs(mouseClickY - sodSpaceCentreY[i][j]), 2));
                        // Updates the location of the closest sodSpace center 
                        // (Note there is no else clause... instead we just keep iterating and updating until we have traversed the entire 2-D array!)
                        if (curr_dist < best_dist) {
                            // update values of local variables
                            best_dist = curr_dist;
                            // update row and column location of the sodSpace center
                            // that is the closest of those searched so far 
                            best_row = i;
                            best_col = j;
                        }
                    }
                }
                // Now that we have determined which sodSpace center is closest to where you clicked,
                // place the object at that position, but first check to see if any plant has already 
                // been placed at this point; if not, then mark the position accordingly
                if (!sodSpaceOccupied[best_row][best_col]) {
                    addObject(new CherryBomb(this.level_idx), sodSpaceCentreX[best_row][best_col], sodSpaceCentreY[best_row][best_col]);
                    Greenfoot.playSound("plant2.wav");
                    sodSpaceOccupied[best_row][best_col] = true;
                    // Decrease sunpoints accordingly
                    addSunPoints(-150);
                    // Now reset seed packet
                    cherrybombSeedPacketClicked = false;
                    List<CherryBombSeedPacket> cherrybombSeedPacketList = getObjects(CherryBombSeedPacket.class);
                    for (CherryBombSeedPacket thisSeedPacket : cherrybombSeedPacketList) thisSeedPacket.resetImage();
                }
            }
        }
    }
    
    /**
     * Method checks to see if the user has completed the current stage in the game
     * If the stage has completed, make sure to prepare for the next stage (use the other helper methods)
     */
    public void checkForStageCompletion () {
        List<Regular_Zombies> zombiesRemaining = getObjects(Regular_Zombies.class);
        // Check to see if there aren't any more zombies in world
        if (zombiesRemaining.isEmpty()) {
            stage_completed = true; // Set this to true => stage has ended
            List<SeedPacketEndStage> seedPacketStageEndList = getObjects(SeedPacketEndStage.class);
            SeedPacketEndStage seedPacketStageEnd = null;
            if (!seedPacketStageEndList.isEmpty()) {
                seedPacketStageEnd = seedPacketStageEndList.get(0);
                // if (seedPacketStageEndList.size() > 0) seedPacketStageEnd = seedPacketStageEndList.get(0);
                if (Greenfoot.mouseClicked(seedPacketStageEnd)) { // If the user has clicked on the end stage seed packet
                    if (music.equals(frontYardMusic)) { // Now stop the music from playing
                        music.stop();
                        music_playing = false;
                    }
                    // Only proceed with preparing for the next stage, if the music has ended
                    if (!music_playing) {
                        addObject(starburst, seedPacketStageEnd.getX(), seedPacketStageEnd.getY());
                        starburst_on_screen = true;
                        music = win_music;
                        // this.level_idx++; // Now increment the current level variable that is also used as an index by 1
                        this.stage_CycleCnt = 0; // Reset the stage cycle counter
                        music.play(); // Play the music
                        music_playing = true; // Set this to true
                        removeObjects(getObjects(SmoothMover.class));
                        removeObjects(getObjects(AlertMessage.class));
                    }
                }
            }
            // Hardcode in the starburst animation
            if (starburst_on_screen) {
                if (stage_CycleCnt % 2 == 0) {
                    starburst.setRotation(starburst.getRotation() + 1);
                }
                if (stage_CycleCnt == 100) {
                    this.alpha = 0;
                    screen = new Screen(whiteScreen);
                    screen.setAlpha(alpha);
                    addObject(screen, 556, 300);
                }
                else if (stage_CycleCnt > 100 && stage_CycleCnt < 150) {
                    alpha += 5;
                    alpha = Math.min(alpha, 255);
                    screen.setAlpha(alpha);
                }
                else if (stage_CycleCnt == 150) {
                    alpha = 255;
                    screen.setAlpha(alpha);
                }
                else if (stage_CycleCnt == 300) {
                    Greenfoot.playSound("lightfill.wav");
                }
                else if (stage_CycleCnt >= 300 && stage_CycleCnt < 700) {
                    alpha--;
                    alpha = Math.max(0, alpha);
                    starburst.setAlpha(alpha);
                    seedPacketStageEnd.setAlpha(alpha);  
                }
                if (stage_CycleCnt == 700) {
                    // For the fade-in to transition into the white screen
                    alpha = 0;
                    // Reset everything
                    Reset_Stage();
                    music_playing = false; // Set this to false to play music again
                    screen = new Screen(whiteScreen);
                    screen.setAlpha(alpha);
                    // First check if it is the last stage to complete
                    if (level_idx == total_num_levels) { // If it is the last level of the game
                        // Make sure object player_info isn't null
                        if (player_info == null) {
                            Update_Player_Info();
                        }
                        write_Scores(player_info); // Make sure to write the score to the text file first
                        Greenfoot.setWorld(new Win_Screen(this.sound_info, this.player_info)); // Set it to the winning screen instead
                    }
                    // If user still hasn't finished the game
                    else {
                        Greenfoot.setWorld(new Backyard(this.sound_info, this.player_info, this.level_idx + 1, true, this.score));
                    }
                }
                else if (stage_CycleCnt > 750 && stage_CycleCnt < 800) {
                    alpha += 5;
                    alpha = Math.min(alpha, 255);
                    screen.setAlpha(alpha);
                }
                else if (stage_CycleCnt == 800) {
                    alpha = 255;
                    screen.setAlpha(alpha);
                }
            }
        }
    }

    /**
     * Method resets the stage by clearing and removing almost all calsses
     */
    public void Reset_Stage () {
        removeObjects(getObjects(SeedPacketTray.class));
        removeObjects(getObjects(Seedpacket.class));
        removeObjects(getObjects(Text.class));
        removeObjects(getObjects(Plant.class));
        removeObjects(getObjects(Regular_Zombies.class));
        removeObjects(getObjects(Regular_Shovel.class));
        removeObjects(getObjects(Regular_Lawnmower.class));
        score_counter.setValue(0); // RESET
        removeObjects(getObjects(Counter.class));
        addObject(new SBoard(100, 0), 260, 68); // Add the object to the world => make sure it aligns with the seed packet tray
    }

    /**
     * Method is used to check for any zombies that have won the game
     */
    public void checkForZombiesWin () {
        SimpleTimer simple_timer = new SimpleTimer(); // Instantiate a new object of this class
        // First get a list of all of the Zombie objects in world
        List<Regular_Zombies> list_zombies = getObjects(Regular_Zombies.class);
        if (!list_zombies.isEmpty()) {
            for (Regular_Zombies zombies_rem : list_zombies) {
                // Check to see if the current zombie object has crossed the hardcoded position
                if (zombies_rem.getX() < -10) {
                    lose_music.play(); // Play the losing music
                    AlertMessage display_msg = new AlertMessage(ALERT_ZOMBIES_WIN); // Display the message for when the zombies have won
                    addObject(display_msg, 400, 300); // Now add the message to the screen
                    if (simple_timer.millisElapsed() >= 2000) { // Make sure to display for only 2 seconds
                        timer.mark(); // Make sure to reset the timer
                        // Now make sure to set it to the lose_screen/final_screen (where user can see the leaderboard)
                        // Update Player_Info object if it is null
                        if (this.player_info == null) Update_Player_Info();
                        if (this.sound_info != null && this.player_info != null) {
                            Greenfoot.setWorld(new GameOver(this.sound_info, this.player_info)); // Make sure to include the two parameters
                        }
                    }
                }
            }
        }
    }

    /**
     * Method displays the sequence of title screen images for the game and also prompts the user to click start when ready to begin playing
     * Had to once again, hardcode in the animation
     */
    public void displayTitleScreen () {
        if (stage_CycleCnt == 1) {
            alpha = 0;
            screen.setScreen(credits); // Display the credits first
            screen.setAlpha(alpha);
            music = titleScreenMusic;
            music.playLoop();
            music_playing = true;
        }
        else if (stage_CycleCnt > 1 && stage_CycleCnt < 25) {
            alpha += 10; // Make the screen darker
            screen.setAlpha(alpha);
        }
        // Should display a totally white screen
        else if (stage_CycleCnt >= 25 && stage_CycleCnt < 175) {
            alpha = 255;
            screen.setAlpha(alpha);
        }
        else if (stage_CycleCnt >= 175 && stage_CycleCnt < 200) {
            alpha -= 10; // Make the screen brighter
            screen.setAlpha(alpha);
        }
        else if (stage_CycleCnt >= 200 && stage_CycleCnt < 210) {
            alpha = 0;
            screen.setAlpha(alpha);
            screen.setScreen(popcapindent);
            screen.setAlpha(alpha);
        }
        else if (stage_CycleCnt >= 210 && stage_CycleCnt < 235) {
            alpha += 10;
            screen.setAlpha(alpha);
        }
        else if (stage_CycleCnt >= 235 && stage_CycleCnt < 385) {
            alpha = 255;
            screen.setAlpha(alpha);
        }
        else if (stage_CycleCnt >= 385 && stage_CycleCnt < 410) {
            alpha -= 10;
            screen.setAlpha(alpha);
        }
        else if (stage_CycleCnt >= 410 && stage_CycleCnt < 420) {
            alpha = 0;
            screen.setAlpha(alpha);
            screen.setScreen(titleScreen);
            screen.setAlpha(alpha);
        }
        else if (stage_CycleCnt >= 420 && stage_CycleCnt < 445) {
            alpha += 10;
            screen.setAlpha(alpha);
        }
        else if (stage_CycleCnt == 445) {
            alpha = 255;
            screen.setAlpha(alpha);
            if (!Greenfoot.mouseClicked(screen)) stage_CycleCnt--;
        }
        else if (stage_CycleCnt > 445 && stage_CycleCnt < 740) {
            alpha -= 4;
            screen.setAlpha(Math.max(0, alpha));
            // TODO: Start fading out the music as well!
            if (music_playing && music.equals(titleScreenMusic)) {
                music.stop();
                music = zombieLaughMusic;
                music.play();
                music_playing = false;
            }
            // Starts the evil zombie laugh here...
            if (stage_CycleCnt == 550) Greenfoot.playSound("evillaugh.wav");
        }
        else if (stage_CycleCnt >= 750) {
            stage_CycleCnt = 0;
            removeObject(screen);
            gameStatus = BG_SCROLL_SETUP;
        }
    }

    /**
     * Method is used to start the game
     * Handles all events that need to take place
     */
    public void started () {
        if (music_playing) {
            // Only play in a loop if it's not the win music
            if (music.equals(win_music)) music.play();
            else music.playLoop();
        }
    }

    /**
     * Method is used as a helper method for the scroll setup for the beginning of each stage
     */
    public void bgScrollSetup () {
        GreenfootImage background = getBackground(); 
        background.drawImage(fullBGImage, 0, 0);  
        //Randomize both the waiting zombie and zombie positions

        addObject(new BasicZombieWaiting(), 1180, 152);
        addObject(new BucketZombieWaiting(), 1120, 272);
        addObject(new BasicZombieWaiting(), 1200, 332);
        addObject(new BucketZombieWaiting(), 1220, 440);
        addObject(new BasicZombieWaiting(), 1210, 480);

        // Now update and display the alert message
        alertMessage = new AlertMessage(ALERT_YOUR_HOUSE);
        addObject(alertMessage, 400, 550);

        // Play the scroll music
        music = scrollMusic;
        music.setVolume(100);
        music.playLoop();
        gameStatus = BG_SCROLL_ANIMATE;
    }

    /**
     * Method is used to animate the scroll setup for this game
     * Basically hardcoded the animation, once again
     * TODO: Make the animation much more smoother
     */
    public void bgScrollAnimate () {
        if (stage_CycleCnt == 100) {
            removeObject(alertMessage);
        }
        else if (stage_CycleCnt > 100 && stage_CycleCnt < 350) {
            bgOffset -= bgOffsetDelta;
            bgOffset = Math.max(bgOffset, -500);
            scrollBgImage(bgOffset);
        }
        else if (stage_CycleCnt > 450 && stage_CycleCnt < 600) {
            bgOffset += bgOffsetDelta;
            bgOffset = Math.min(bgOffset, -218);
            scrollBgImage(bgOffset);
        }
        else if (stage_CycleCnt == 600) {
            // Now remove all waiting zombies from screen
            List<Zombie_Waiting> waiting_zombies = getObjects(Zombie_Waiting.class);
            for (Zombie_Waiting zombie : waiting_zombies) {
                removeObject(zombie);
            }

            // Reset the screen with the usual background
            GreenfootImage background = getBackground(); 
            background.drawImage(frontYardImage, 0, 0); 
            gameStatus = STAGE_SETUP;
        }
    }

    /**
     * Helper method is used to scroll the background image by the provided offset value
     * Is used in: bgScrollAnimate method from above
     * @param offset = integer specifying number of pixels to move background for each animation frame
     */
    public void scrollBgImage (int offset) {
        GreenfootImage background = getBackground(); 
        background.drawImage(fullBGImage, offset, 0);  
        // Now get all objects and move them by the offset delta value
        List<Actor> curr_objects = getObjects(null);
        for (Actor object : curr_objects) {
            if (stage_CycleCnt > 100 && stage_CycleCnt < 350) {
                object.setLocation(object.getX() - bgOffsetDelta, object.getY());
            }
            else if (stage_CycleCnt > 450 && stage_CycleCnt < 700) {
                object.setLocation(object.getX() + bgOffsetDelta, object.getY());
            }
        }
    }

    /**
     * Method is used to let the sunlight fall randomly to the ground
     */
    public void letSunFallRandomly () {
        // TODO: Change this so falling suns appear at more randomized intervals
        int sunfallInterval = 200 * (1 + rng.nextInt(3));
        if (stage_CycleCnt % sunfallInterval == 0) {
            // Now pick a random x-coordinate between 100 and 699
            int fallingSunStartX = 100 + rng.nextInt(600);
            addObject(new FallingSun(), fallingSunStartX, -50);
            if (!fallingSunAlertAlreadyDisplayed && !fallingSunAlertRemoved) {
                fallingSunAlertAlreadyDisplayed = true;
                fallingSunAlertRemoved = false;
                // Make sure to update and display the alert message
                alertMessage = new AlertMessage(ALERT_CLICK_SUN);
                addObject(alertMessage, 400, 550); 
            }
        }
    }

    /**
     * Method is used to make the zombies make sounds
     */
    public void makeZombieSounds () {
        List<Regular_Zombies> zombies_rem = getObjects(Regular_Zombies.class);
        if (!zombies_rem.isEmpty()) {
            Regular_Zombies zombie_init = zombies_rem.get(0);
            for (Regular_Zombies zombie : zombies_rem) {
                // Update the leftmost zombie
                if (zombie.getExactX() < zombie_init.getExactX()) zombie_init = zombie;
            }
            // If sound hasn't been played, play it
            if (!awoogaAlreadyPlayed) {
                // Only play the sound if the zmobie is just about to enter the field!!!
                if (zombie_init.getExactX() >= getWidth() + 20 && zombie_init.getExactX() <= getWidth() + 40) {
                    awooga.play(); // Play the sound effect
                    awoogaAlreadyPlayed = true;
                }
            }
            else {
                // Otherwise, play a randomly selected "zombie groan" sound at some random interval
                int randomGroanInterval = 500 * (1 + rng.nextInt(3));
                if (stage_CycleCnt % randomGroanInterval == 0) { 
                    int groanIndex = rng.nextInt(13); // Use Java's Random class
                    zombieGroan[groanIndex].play(); // Play a random zombie graon sound effect that can be found in the array
                }
            }
        }
    }

    /**
     * Method runs the game
     */
    public void Run_Game () {
        // If the current game status equals the title screen
        if (gameStatus == TITLE_SCREEN) displayTitleScreen();

        // If the current game status equals the scroll setup
        else if (gameStatus == BG_SCROLL_SETUP) bgScrollSetup();

        // If the current game status equals the scroll animate
        else if (gameStatus == BG_SCROLL_ANIMATE) bgScrollAnimate();

        // If the current game status equals the stage setup
        else if (gameStatus == STAGE_SETUP) {
            for (int i=0; i<5; i++) {
                mower[i].set_CanMoveStatus(false);
            }
            setUpStage();
            gameStatus = READY_MESSAGE;
            // Make sure to reset the stage_CycleCnt for upcoming stage
            stage_CycleCnt = 0;
        }
        // If the current game status equals the ready message, go ahead and display the message
        else if (gameStatus == READY_MESSAGE) {
            startStage();
        }
        // If the current game status equals the constant for starting the game, go ahead and begin playing the game
        else if (gameStatus == BEGIN_PLAY) {
            if (!flag) {
                zombie_waveCycleCnt = 0; // Set this to 0 initially
                flag = true;
            }
            // For added delay time
            if (this.easy) {
                if (zombie_waveCycleCnt >= 1200 && !flag1) {
                    Generate_Zombie_Wave();
                    flag1 = true;
                }
            }
            else if (this.medium) {
                if (zombie_waveCycleCnt >= 1000 && !flag1) {
                    Generate_Zombie_Wave();
                    flag1 = true;
                }
            }
            else if (this.hard) {
                if (zombie_waveCycleCnt >= 800 && !flag1) {
                    Generate_Zombie_Wave();
                    flag1 = true;
                }
            }
            for (Regular_Zombies zombie : getObjects(Regular_Zombies.class)) {
                if (zombie.getX() <= 40) {
                    if (Math.abs((zombie.getY() - 125)) <= 25) {
                        mower[0].set_CanMoveStatus(true);
                    }
                    else if (Math.abs((zombie.getY() - 225)) <= 25) {
                        mower[1].set_CanMoveStatus(true);
                    }
                    else if (Math.abs((zombie.getY() - 325)) <= 25) {
                        mower[2].set_CanMoveStatus(true);
                    }
                    else if (Math.abs((zombie.getY() - 425)) <= 25) {
                        mower[3].set_CanMoveStatus(true);
                    }
                    else if (Math.abs((zombie.getY() - 525)) <= 25) {
                        mower[4].set_CanMoveStatus(true);
                    }
                }
            }
            
            addObject(shovel, 735, 40); // Don't forget the shovel!!!
            if (!music_playing) {
                music.playLoop();
                music_playing = true;
            }
            // NOTE: If one seed packet is clicked and active, you first need to de-activate any other active seed packets
            checkForStageCompletion(); // Check for stage completion
            checkForZombiesWin(); // Check for if the zombies have won
            if (!starburst_on_screen) { // If the starburst is not on-screen
                checkForPeaShooterPacketClicked(); // Check if user has clicked on peashooter seed packet
                checkForSunflowerPacketClicked(); // Check if user has clicked on sunflower seed packet
                checkForTriplePeaShooterPacketClicked();
                checkForDoublePeaShooterPacketClicked();
                checkForMachineGunPeaShooterPacketClicked();
                checkForPotatoPacketClicked();
                checkForIcePeaShooterPacketClicked();
                checkForCherryBombPacketClicked();

                // Finally, make sure to update the 2d boolean array if any plants have indeed been removed
                update_RemovedStatus();
                letSunFallRandomly(); // Have the sun fall randomly down the screen (still need to complete the method)
                makeZombieSounds(); // Have the zombies make sounds (still need to complete the method)
            }
            sunCount.update(sunpoints); // Don't forget to update amount of sunpoints at the end
        }
    }

    /**
     * Method calculates user's score
     */
    public void Calc_Score () {
        List<Regular_Zombies> zombies_list = getObjects(Regular_Zombies.class);
        // Always update the player's score
        if (!zombies_list.isEmpty()) {
        // System.out.println("HI!!!");
            for (Regular_Zombies zombie : zombies_list) {
                if (zombie.getDying()) {
                    if (this.easy) {
                        this.score += 25;
                    }
                    else if (this.medium) {
                        this.score += 45;
                    }
                    else if (this.hard) {
                        this.score += 65;
                    }
                }
            }
            // this.score = zombies_list.get(0).get_Points();
        }
    }
    
    /**
     * Act - do whatever the Button wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act () {
        stage_CycleCnt++; // Increment this variable to cycle through
        zombie_waveCycleCnt++; // Increment this variable for generatnig new zombie waves
        // Needed for spawning zombie waves intermittently
        if (gameStatus == BEGIN_PLAY && !has_lost) { // Have a max of 3 waves
            // Calc_Score();
            if ((this.easy && this.zombie_waveCycleCnt % 3500 == 0) || (getObjects(Regular_Zombies.class).size() == 1)) {
                if (this.num_waves < 1) { // Have only 1 wave
                    num_waves++;
                    Generate_Zombie_Wave(); // Call on this method instead
                    this.zombie_waveCycleCnt = 0; // Make sure to reset it
                }
                else this.done_waves = true;
            }
            else if ((this.medium && this.zombie_waveCycleCnt % 2500 == 0) || (getObjects(Regular_Zombies.class).size() == 1)) {
                if (this.num_waves < 2) { // Have only 2 waves
                    num_waves++;
                    Generate_Zombie_Wave(); // Call on this method instead
                    this.zombie_waveCycleCnt = 0; // Make sure to reset it
                }
                else this.done_waves = true;
                num_waves++;
                Generate_Zombie_Wave(); // Call on this method instead
                this.zombie_waveCycleCnt = 0; // Make sure to reset it
            }
            else if ((this.hard && this.zombie_waveCycleCnt % 2000 == 0) || (getObjects(Regular_Zombies.class).size() == 1)) {
                if (this.num_waves < 3) { // Have 3 waves
                    num_waves++;
                    Generate_Zombie_Wave(); // Call on this method instead
                    this.zombie_waveCycleCnt = 0; // Make sure to reset it
                }
                else this.done_waves = true;
                num_waves++;
                Generate_Zombie_Wave(); // Call on this method instead
                this.zombie_waveCycleCnt = 0; // Make sure to reset it
            }
            // Always display the score counter
            score_counter.setValue(this.score);
        }
        Calc_Score();
        Update_Player_Info(); // Always update player info
        action++;
        Run_Game();
    }
}
