import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * This class is used for the peashooter plant in the game
 * May decide to add more plants into the game for future updates
 * 
 * @author Eric Liu
 * @version May 8, 2018
 */

public class PeaShooter extends Plants {
    
    private GifImage plant_gif = new GifImage("pea_shooter.gif");
    private GifImage dead_gif = new GifImage("pea_shooter_dying.gif");
    
    /**
     * Constructor for this class
     */
    public PeaShooter () {
        setImage(plant_gif.getCurrentImage());
    }
    
    private long last_time = System.currentTimeMillis();
    
    public void Shoot_Peas (int num_time) {
        // Check to see if there are zombie objects on screen first
        // TODO: Make sure to only shoot if the zombie object is in same row as peashooter
        if (!getWorld().getObjects(Zombies.class).isEmpty()) {
            long currTime = System.currentTimeMillis();
            if (currTime >= last_time + num_time) {
                last_time = currTime;
                Pea pea = new Pea(); // Instantiate a new Pea object
                World world = getWorld();
                world.addObject(pea, getX() + 28, getY() - 11);
            }
        }
    }
    
    /**
     * Act - do whatever the PeaShooter wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act () {
        setImage(plant_gif.getCurrentImage());
        Shoot_Peas(2000); // Shoot a pea every 2 seconds
        Plant_Hit("pea_shooter_dying.gif", 900); // Run the helper method in the super class to see if peashooter is hit by zombie
    }    
}