import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * This class is used to display the about screen which has information on the game in general
 * 
 * @author Eric Liu
 * @version May 16, 2018
 */

public class About extends World {

    private Button back; // Declare Button object for the back button
    private GreenfootSound music = new GreenfootSound("menu.wav");
    private Sound_Info sound_info = null;
    private Player_Info player_info;
    
    /**
     * 1st Constructor for objects of class About.
     * 
     */
    /*
    public About () {    
        // Create a new world with 1111x700 cells with a cell size of 1x1 pixels.
        super(1111, 700, 1);
        back = new Button("back", new GreenfootImage("back_button.png"));
        addObject(back, 1111 / 2, 650);
        Display();
        music.playLoop();
    }*/
    
    /**
     * 2nd Constructor for objects of class About.
     * 
     */
    public About (Sound_Info soundinfo, Player_Info playerinfo) {
        // Create a new world with 1111x700 cells with a cell size of 1x1 pixels.
        super(1111, 700, 1);
        this.sound_info = soundinfo;
        this.player_info = playerinfo;
        back = new Button("back", new GreenfootImage("back_button.png"));
        addObject(back, 1111 / 2, 650);
        Display();
        music.playLoop();
    }
    
    public void stopBackgroundMusic () {
        this.music.stop();
    }
    
    /**
     * Method is used to display the text on-screen
     */
    public void Display () {
        addObject(new Text("ABOUT", 30, "yellow"), 1111 / 2, 150);
        addObject(new Text("Plants vs. Zombies is a tower-defense and strategy", 20, "white"), 550, 200);
        addObject(new Text("game which involves an army of zombies trying to enter the house", 20, "white"), 550, 250);
        addObject(new Text("and eat your brains. The only way you can stop them is by using your", 20, "white"), 550, 300);
        addObject(new Text("arsenal of plants that will attack the zmobies at your command. The plants: ", 20, "white"), 550, 350);
        addObject(new Text("Pea-shooters, Beetroots, Walnuts, Sunflowers, and much more - are all", 20, "white"), 550, 400);
        addObject(new Text("set to destroy and handle the zombies who dare to enter your backyard.", 20, "white"), 550, 450);
        addObject(new Text("There are two game modes - Regular and Blitz. In Regular, you will be", 20, "white"), 550, 500);
        addObject(new Text("levelling-up as you play while in Blitz, you will be continously playing.", 20, "white"), 550, 550);
    }
    
    /**
     * Act - do whatever the Player_Info wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act () {
        if (back.isPressed()) {
           stopBackgroundMusic();
           Greenfoot.setWorld(new Instructions(this.sound_info, this.player_info)); // Go back to the starting screen
        }
    }
}
