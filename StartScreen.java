import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Class is used to display the screen that is shown on startup
 * Also plays the background music
 * 
 * @author Eric Liu, Cindy Cao
 * @version May 4, 2018
 */

public class StartScreen extends World {
    
    private GreenfootSound music = new GreenfootSound("menu.wav");
    private InsButton ins; // For the InsButton object
    private SettingsButton set; // For the SettingsButton object
    private Sound_Info sound_info = new Sound_Info(1, 80);
    private Player_Info player_info = new Player_Info("", 0, 1, 1); // This will be overwritten later on when the player enters input

    /**
     * 1st Constructor for objects of class StartScreen.
     * 
     */
    public StartScreen () {    
        // Create a new world with 1111x602 cells with a cell size of 1x1 pixels.
        super(1111, 602, 1);
        ClickToStart start = new ClickToStart("click_to_start.gif", "modes", "start_screen");
        addObject(start, 580, 534);
        ins = new InsButton();
        set = new SettingsButton();
        addObject(ins, 1050, 44);
        addObject(set, 61, 44);
        
        
        this.sound_info = new Sound_Info(1, 80);
        this.player_info.setDifficulty(1);
        start.Set_SoundInfo(this.sound_info);
        start.Set_PlayerInfo(this.player_info);
        if (!start.isPressed()) {
            music.playLoop(); // Play it in a loop
        }
    }
    
    /**
     * 2nd Constructor for objects of class StartScreen.
     * 
     */
    public StartScreen (Sound_Info soundinfo, Player_Info playerinfo) {    
        // Create a new world with 1111x602 cells with a cell size of 1x1 pixels.
        super(1111, 602, 1);
        this.sound_info = soundinfo;
        this.player_info = playerinfo;
        ClickToStart start = new ClickToStart("click_to_start.gif", "modes", "start_screen");
        addObject(start, 641, 534);
        ins = new InsButton();
        set = new SettingsButton();
        addObject(ins, 1050, 44);
        addObject(set, 61, 44);
        this.sound_info = soundinfo;
        this.player_info = playerinfo;
        start.Set_SoundInfo(this.sound_info);
        start.Set_PlayerInfo(this.player_info);
        if (!start.isPressed()) {
            music.playLoop(); // Play it in a loop
        }
    }
    
    public void stopBackgroundMusic () {
        this.music.stop();
    }
    
    /**
     * Act - do whatever the Player_Info wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act () {
        if (Greenfoot.mouseClicked(ins)) {
            stopBackgroundMusic();
            Instructions i = new Instructions(this.sound_info, this.player_info);
            Greenfoot.setWorld(i);
        }
        else if (Greenfoot.mouseClicked(set)) {
            stopBackgroundMusic();
            Greenfoot.setWorld(new Settings_Screen(this.sound_info, this.player_info));
        }
    }
}
