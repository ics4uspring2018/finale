import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * This class is used for displaying a frozen normal zombie
 * 
 * @author Eric Liu
 * @version June 12th, 2018
 */

public class Frozen_Normalzombie extends Frozen_Zombie {
    
    private int cycleCnt = 0; // For timing
    
    private long lastTime = System.currentTimeMillis(); // Get last time
    
    /**
     * Constructor
     * @param health = health of zombie object
     */
    public Frozen_Normalzombie (int health, int level) {
        super(health, level);
    }
    
    /**
     * Act - do whatever the Frozen_Normalzombie wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act () {
        this.cycleCnt++;
        if (this.cycleCnt % 200 == 0 && !getWorld().getObjects(Frozen_Normalzombie.class).isEmpty()) { // Remove the explosion after approximately 2 seconds
            Normalzombie zombie = new Normalzombie(Normalzombie.sound_info, Normalzombie.player_info, Normalzombie.difficulty, Normalzombie.level_idx, Normalzombie.done_waves);
            getWorld().addObject(zombie, this.getX(), this.getY());
            getWorld().removeObject(this);
            zombie.set_Health(this.zombie_health);
            this.cycleCnt = 0;
        }
    }     
}
