import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.io.*;
import java.util.*;

/**
 * This class is used for the cherry bomb plant
 * 
 * @author Eric Liu
 * @version June 8th, 2018
 */

public class CherryBomb extends Plant {
    
    private GreenfootImage imgFrames[];
    private int numFrames = 23;
    private int currImgNum;
    private int cycleCnt = 0;
    // For keeping track of the coordinates
    private int x, y;
    private boolean start_shooting;
    private GifImage gif = new GifImage("cherry_bomb.gif");
    private Explosion explosion = new Explosion();
    private boolean died = false; // Has the plant died yet
    private GreenfootSound explode = new GreenfootSound("Cherrybomb.mp3"); // Exploding sound effect
    
    /**
     * Constructor
     */
    public CherryBomb (int level) {
        super(level);
        setImage(gif.getCurrentImage());// Add your action code here.
    }
    
    /**
     * Used for the exploding animation of the cherry bomb
     */
    public void Explode () {
        Backyard world = (Backyard) getWorld();
        if (!world.getObjects(Incinerated_Zombie.class).isEmpty()) {
            if (this.last) { // Spawn the end seed packet if it's the last zombie
                // Now reward user with new seedpacket/trophy
                if (world.getObjects(SeedPacketEndStage.class).isEmpty()) {
                    world.addObject(new SeedPacketEndStage(this.level_idx), this.getX(), this.getY());
                }
            }
            explode.play();
            world.addObject(explosion, this.getX(), this.getY());
            getWorld().removeObject(this);
        }
    }
    
    /**
     * Method checks if the plant (cherry bomb) is in same row as the zombie
     * NOTE: USE THIS INSTEAD OF ISTOUCHING()
     */
    public boolean Check_In_Same_Row () {
        this.same_row = false;
        int x = getX();
        int y = getY();
        List<Regular_Zombies> zombies_world = getWorld().getObjects(Regular_Zombies.class);
        if (!zombies_world.isEmpty()) {
            // Only shoot if zombie is in same row as peashooter
            for (Regular_Zombies zombie : zombies_world) {
                if (this.getX() < getWorld().getWidth() - 1 && (Math.abs(this.getX() - zombie.getX()) <= 50 || Math.abs(zombie.getY() - y) <= 160)) {
                    // Make sure to update booleans
                    this.same_row = true;
                    return true;
                }
            }
        }
        return false;
    }
    
    private long lastTime = System.currentTimeMillis();
    /**
     * Act - do whatever the TriplePeaShooter wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act () {
        this.cycleCnt++;
        check_LastZombie();
        setImage(gif.getCurrentImage());// Add your action code here.
        Backyard world = (Backyard) getWorld();
        if (Check_In_Same_Row() || !world.getObjects(Incinerated_Zombie.class).isEmpty()) { //&& this.cycleCnt % 50 == 0) { // Should be approximately 1.2 seconds
            // System.out.println("HIIIIIII");
            Explode();
            this.died = true;
            this.cycleCnt = 0;
        }
        
        if (!this.died) {
            check_Zombies_in_Row(); // First check for zombies that are in the same row as the plant object
            Plant_Hit(); // Check for hit by any zombies
        }
    }    
}
