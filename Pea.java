import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * This class is used for the pea bullets that the peashooters shoot
 * 
 * @author Eric Liu
 * @version May 9, 2018
 */

public class Pea extends Bullet {
    /**
     * Act - do whatever the Pea wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act () {
        move(this.speed);
        Check_Boundaries();
    }    
}
