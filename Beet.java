import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * This class is used for the beet bullets that the beet plants shoot
 * 
 * @author Eric Liu
 * @version May 9, 2018
 */

public class Beet extends Bullet {
    
    public Beet () {
        SetSpeed(6); // Set the speed of the beet bullets to 6: may decide to change it later
    }
    
    /**
     * Act - do whatever the Beet wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act () {
        move(this.speed); // Invoke the field variable from the super class
        Check_Boundaries();
    }    
}
