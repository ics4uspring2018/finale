import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * This class is used for the beetroot icon that is displayed on the sidebar in-game
 * 
 * @author Eric Liu, Charles Cai
 * @version May 7, 2018
 */

public class Sidebar extends Actor {
    
    // Declare PROTECTED variables here!!!
    protected int plant_cost = 0; // For the cost of the plant
    protected boolean grabbed = false; // For whether the user has grabbed the plant or not
    protected boolean isActive = false; // For whether the plant item is active or not
    protected int xcoor, ycoor; // Declare x and y coordinates of the plant icon
    protected GreenfootImage activeImage, inactiveImage;
    
    protected void Set_Plant_Cost (int cost) {
        this.plant_cost = cost;
    }
    
    protected void Set_Icon_Coordinates (int x, int y) {
        this.xcoor = x;
        this.ycoor = y;
    }
    
    protected void Create_Plant (String game_mode, Plants plant) {
        if (game_mode.equalsIgnoreCase("regular")) {
            Backyard world = (Backyard) getWorld();
            Counter sun_cnt = world.Get_SunCounter();
            if (plant_cost <= sun_cnt.getValue()) {
                setImage(activeImage);
                Create_PlantDraggable(plant, "regular");
            }
            else setImage(inactiveImage);
        }
        else if (game_mode.equalsIgnoreCase("blitz")) {
            Blitz_Backyard world = (Blitz_Backyard) getWorld();
            Counter sun_cnt = world.Get_SunCounter();
            if (plant_cost <= sun_cnt.getValue()) {
                setImage(activeImage);
                Create_PlantDraggable(plant, "blitz");
            }
            else setImage(inactiveImage);
        }
    }
    
    /**
     * Helper method to execute the dragging and dropping for the plant object
     * It uses the same logic as that of the dragging method for the shovel class!
     */
    protected void Create_PlantDraggable (Plants plant, String game_mode) {
        if (game_mode.equalsIgnoreCase("regular")) {
            Backyard world = (Backyard) getWorld();
            Counter sun_cnt = world.Get_SunCounter();
            
            // Check for the initial pressing down of the moust button
            if (Greenfoot.mousePressed(this) && !grabbed) {
                grabbed = true; // Set the boolean value to true
                MouseInfo mouse_info = Greenfoot.getMouseInfo();
                world.removeObject(this);
                world.addObject(this, mouse_info.getX(), mouse_info.getY());
                return;
            }
            
            // Now check for the actual dragging of the object
            if (Greenfoot.mouseDragged(this) && grabbed) {
                // Set the location to the current coordinates of the mouse object
                MouseInfo mouse_info = Greenfoot.getMouseInfo();
                setLocation(mouse_info.getX(), mouse_info.getY());
                return;
            }
            
            // Now check for the release from the mouse button
            if (Greenfoot.mouseDragEnded(this) && grabbed) {
                // Release the object
                MouseInfo mouse_info = Greenfoot.getMouseInfo();
                setLocation(this.xcoor, this.ycoor);
                int row = world.Get_Row_Position(mouse_info.getY());
                int col = world.Get_Col_Position(mouse_info.getX());
                if (row != -1 && col != -1) { // If the coordinates are valid
                    if (world.getObjectsAt(col, row, Actor.class).isEmpty()) {
                        world.addObject(plant, col, row);
                        sun_cnt.add(-plant_cost); // Deduct the amount of sunlight by the sunlight cost of the plant
                    }
                }
                grabbed = false; // Reset the boolean variable to false
                return;
            }
        }
        else if (game_mode.equalsIgnoreCase("blitz")) {
            Blitz_Backyard world = (Blitz_Backyard) getWorld();
            Counter sun_cnt = world.Get_SunCounter();
            
            // Check for the initial pressing down of the moust button
            if (Greenfoot.mousePressed(this) && !grabbed) {
                grabbed = true; // Set the boolean value to true
                MouseInfo mouse_info = Greenfoot.getMouseInfo();
                world.removeObject(this);
                world.addObject(this, mouse_info.getX(), mouse_info.getY());
                return;
            }
            
            // Now check for the actual dragging of the object
            if (Greenfoot.mouseDragged(this) && grabbed) {
                // Set the location to the current coordinates of the mouse object
                MouseInfo mouse_info = Greenfoot.getMouseInfo();
                setLocation(mouse_info.getX(), mouse_info.getY());
                return;
            }
            
            // Now check for the release from the mouse button
            if (Greenfoot.mouseDragEnded(this) && grabbed) {
                // Release the object
                MouseInfo mouse_info = Greenfoot.getMouseInfo();
                setLocation(this.xcoor, this.ycoor);
                int row = world.Get_Row_Position(mouse_info.getY());
                int col = world.Get_Col_Position(mouse_info.getX());
                if (row != -1 && col != -1) { // If the coordinates are valid
                    if (world.getObjectsAt(col, row, Actor.class).isEmpty()) {
                        world.addObject(plant, col, row);
                        sun_cnt.add(-plant_cost); // Deduct the amount of sunlight by the sunlight cost of the plant
                    }
                }
                grabbed = false; // Reset the boolean variable to false
                return;
            }
        }
    }
    
    protected void Set_Active_Image (String fileName) {
        activeImage = new GreenfootImage(fileName);
    }
    
    protected void Set_Inactive_Image (String fileName) {
        inactiveImage = new GreenfootImage(fileName);
    }
    
    /**
     * Act - do whatever the Sidebar wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act () {
        
    }    
}
