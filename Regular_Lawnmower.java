import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.*;
import java.io.*;

/**
 * This class is used for the lawnmower objects for Regular mode
 * 
 * @author Eric Liu
 * @version June 5th, 2018
 */

public class Regular_Lawnmower extends SmoothMover {
    
    public boolean is_moving; // Is the lawnmower currently moving?
    public GreenfootSound limbs_popped = new GreenfootSound("limbs_pop_fixed.wav");
    public GreenfootSound lawnmowing = new GreenfootSound("lawnmower.wav");
    public boolean is_move; // For whether lawnmower should move
    public int speed = 0; // Speed of lawnmower
    public GifImage gif = new GifImage("lawn_mower.gif");
    
    /**
     * Constructor
     */
    public Regular_Lawnmower () {
        super(new Vector(0, 0.0)); // Have to instantiate a new Vector instance
        setImage(gif.getCurrentImage());
        is_moving = false;
    }

    /**
     * Method is used for object-collision detection with other zombie objects
     */
    public void check_Zombies () {
        List<Regular_Zombies> zombies_rem = getObjectsAtOffset(2, -40, Regular_Zombies.class); // Return a list of all zombies that intersect at (2, -40)
        if (!zombies_rem.isEmpty()) {
            Regular_Zombies zombie_init = zombies_rem.get(0); // Get the nearest zombie
            // If the zombie hasn't died yet
            if (!zombie_init.getDying()) {
                super.stop();
                limbs_popped.play();
                Backyard world = (Backyard) getWorld();
                if (zombie_init.getX() < world.getWidth() - 1) {
                    zombie_init.die(); // Remove the zombie from the world
                    // Now move the lawnmower and play the sound
                    if (!is_moving) {
                        lawnmowing.play();
                        is_moving = true;
                    }
                }
            }
        }
    }
    
    /**
     * Update the status of whether or not the lawnmower should move
     */
    public void set_CanMoveStatus (boolean status) {
        this.is_move = status;
    }
    

    /**
     * Act - do whatever the Regular_Lawnmower wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act () {
        check_Zombies(); // Make sure to check for zombies -> if intersecting, remove it from the world
        if (this.is_move) move(this.speed);
        if (is_moving) {
            this.speed = 12;
        }
        if (this.speed > 0) {
            setImage(gif.getCurrentImage()); 
            move(this.speed);// Only show the animation if the speed is a positive integer
        }
        /**if (is_moving || is_move) {
            super.addForce(new Vector(0, 0.1)); // Add to the magnitude of the vector
            if (this.getX() > getWorld().getWidth() + 100) { // Make sure it disappears from the screen if it passes the rightmost side
                getWorld().removeObject(this);
            }
        }**/
    }    
}
