import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.*;
import java.io.*;

/**
 * This class is used to define the behavior for the machine gun peashooter that is used for the regular mode
 * 
 * @author Eric Liu, Charles Cai
 * @version May 30, 2018
 */

public class MachineGunPeaShooter extends Plant {
    
    // Declare all of the variables needed for this class
    private GreenfootImage imgFrames[];
    private int numFrames = 23;
    private int currImgNum;
    private int cycleCnt;
    // For keeping track of the coordinates
    private int x, y;
    private boolean start_shooting;
    private GifImage gif = new GifImage("machinegun_peashooter.gif"); // Main gif file
    private final int num_time = 100; // Set the timer interval to 1 second
    
    /**
     * 1st Constructor for this class
     */
    public MachineGunPeaShooter (int level) {
        super(level);
        setImage(gif.getCurrentImage());
        imgFrames = new GreenfootImage[numFrames];
        currImgNum = 0;
        cycleCnt = 1;
    }
    
    /**
     * Method is used to shoot the peas towards the zombie objects
     * Should shoot peas much faster
     */
    public void shootPea () {
        Regular_Pea pea = new Regular_Pea("machinegunpeashooter");
        int x_offset = 40; // Used to position the pea correctly
        getWorld().addObject(pea, this.getX() + x_offset, this.getY() - 5);
    }
    
    /**
     * Updates the image for the next animation frame in sequence
     */
    public void updateImage () {
        setImage(imgFrames[currImgNum]);// For correct animation, only shoot the pea at frame 19
        if (currImgNum == 19 && start_shooting) {
            shootPea();
        }
        // Reset currImgNum for the next animation cycle
        if (currImgNum == 22) {
            currImgNum = 0;
        }
        else {
            currImgNum++;
        }
    }
    
    /**
     * Act - do whatever the Regular_Peashooter wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act () {
        setImage(gif.getCurrentImage());
        if (this.cycleCnt % 20 == 0) { // Have a time delay of only 20
            if (check_Zombies_in_Row()) {
               shootPea();
            }
            this.cycleCnt = 1;
        }
        else {
            this.cycleCnt++;
        }
        Plant_Hit(); // Check for hit by any zombies
    }    
}
