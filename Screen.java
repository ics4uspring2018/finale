import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * This class is used 
 * 
 * @author Eric Liu
 * @version May 30, 2018
 */

public class Screen extends Actor {
    
    /**
     * Constructor for this class
     * @param screenImage = screen's updated image
     */
    public Screen (GreenfootImage screenImage) {
        setScreen(screenImage);
        getImage().setTransparency(0);
    }
    
    /**
     * Set the transparency (opacity) for the current screen image
     * @param alpha = percentage opacity level (0 = transparent, 100 = opaque)
     */
    public void setAlpha (int alpha) {
        getImage().setTransparency(alpha);
    }
    
    /**
     * Set the screen to the desired image
     */
    public void setScreen (GreenfootImage screenImage) {
        setImage(screenImage);
    }
    
    /**
     * Act - do whatever the Screen wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act () {
        
    }    
}
