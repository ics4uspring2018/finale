import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * This class is used for the regular plant bullets
 * 
 * @author Eric Liu
 * @version June 11th, 2018
 */

public class Regular_Bullet extends SmoothMover {
    
    protected boolean is_exploding = false;
    protected int explode_cycleCnt; // Variable for keeping track of the cycle count
    protected String plant_type; // Store which type of plant hit the zombie
    
    /**
     * Constructor
     * @param plant_type = which plant hit the zombie
     */
    public Regular_Bullet (String type) {
        super(new Vector(0, 3.0)); // Instantiates a new vector object from the super class
        this.plant_type = type;
    }
    
    /**
     * Method's used for updating exploding status for pea bullet
     * @param explode_status
     */
    protected void setExplode (boolean explode_status) {
        this.is_exploding = explode_status;
        if (this.is_exploding) {
            this.explode_cycleCnt = 10;
        }
    }
    
    /**
     * Returns whether the pea bullet has exploded or is currently exploding
     */
    protected boolean getExploded () {
        return this.is_exploding;
    }

    
    /**
     * Returns the type of the plant that hit the zombie
     */
    protected String getPlantType () {
        return this.plant_type;
    }
    /**
     * Act - do whatever the Regular_Pea wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act () {
        // Remove any bullets if they go out of bounds
        if (getExactX() >= getWorld().getWidth() - 5) {
            getWorld().removeObject(this);
        }
        // NOTE: Still need to change &/ work on this code, since the animation isn't that good...
        if (is_exploding) { // If the pea bullet has made contact with a zombie
            // Make sure to change this image later on (animation is sketchy)
            setImage("peaSplat.png");
            explode_cycleCnt--;
            // Make sure to remove the bullet from the world
            if (explode_cycleCnt == 0) {
                getWorld().removeObject(this);
            }
        }
        // Else move forwards
        else move();
    }
}