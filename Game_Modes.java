import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * This class is used to display the screen where the user chooses the game mode
 * There are two game modes: regular and blitz
 * Regular is where the game gradually levels up and the user gains more plants along the way
 * Blitz is where the game doesn't level up and the user continually plays the game until he/she dies
 * 
 * @author Eric Liu
 * @version May 7, 2018
 */

public class Game_Modes extends World {

    private GreenfootSound music = new GreenfootSound("menu.wav");
    private String filename1 = "game_mode1.gif", filename2 = "game_mode2.gif";
    private String filename3 = "Instructions.png";
    private ClickToStart regular = new ClickToStart(filename1, "mode1", "game_modes");
    private ClickToStart blitz = new ClickToStart(filename2, "mode2", "game_modes");
    private Sound_Info sound_info = null;
    private Player_Info player_info;
    
    /**
     * 1st Constructor for objects of class Game_Modes.
     * 
     */
    public Game_Modes () {    
        // Create a new world with 1111x602 cells with a cell size of 1x1 pixels.
        super(1111, 602, 1);
        addObject(regular, 300, 520);
        addObject(blitz, 800, 520);
        if (!regular.isPressed() && !blitz.isPressed()) music.playLoop();
    }
    
    /**
     * 2nd Constructor for objects of class Game_Modes.
     * param soundinfo = Sound_Info object
     * @param s=playerinfo = Player_Info object
     */
    public Game_Modes (Sound_Info soundinfo, Player_Info playerinfo) {
        // Create a new world with 1111x602 cells with a cell size of 1x1 pixels.
        super(1111, 602, 1);
        addObject(regular, 300, 520);
        addObject(blitz, 800, 520);
        this.sound_info = soundinfo;
        this.player_info = playerinfo;
        regular.Set_SoundInfo(this.sound_info);
        regular.Set_PlayerInfo(this.player_info);
        blitz.Set_SoundInfo(this.sound_info);
        blitz.Set_PlayerInfo(this.player_info);
        if (!regular.isPressed() && !blitz.isPressed()) music.playLoop();
    }
    
    public void stopBackgroundMusic () {
        this.music.stop();
    }
}
