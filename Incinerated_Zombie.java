import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * This class is used to display the dead/incinerated zombie
 * 
 * @author Eric Liu
 * @version Juen 12th, 2018
 */

public class Incinerated_Zombie extends Actor {
    
    private int cycleCnt = 0; // For timing
    
    private long lastTime = System.currentTimeMillis(); // Get last time
    
    /**
     * Act - do whatever the Incinerated_Zombie wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act () {
        this.cycleCnt++;
        if (this.cycleCnt % 100 == 0 && !getWorld().getObjects(Incinerated_Zombie.class).isEmpty()) { // Remove the explosion after approximately 3 seconds
            getWorld().removeObject(this);
            this.cycleCnt = 0;
        }
    }    
}
