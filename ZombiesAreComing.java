import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * This class is used to display the image/message for incoming zombie wave and plays the sound
 * 
 * @author Eric Liu
 * @version May 10, 2018
 */

public class ZombiesAreComing extends Actor {
    
    private GreenfootSound zombies_coming = new GreenfootSound("zombies_coming.wav");
    private long last_time;
    
    /**
     * Constructor for this class
     */
    public ZombiesAreComing () {
        zombies_coming.play();
        last_time = System.currentTimeMillis();
    }
    
    /**
     * Act - do whatever the ZombiesAreComing wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act () {
        long currTime = System.currentTimeMillis();
        // Should be displayed for 1.5 seconds
        if (currTime >= last_time + 1500) getWorld().removeObject(this);
    }    
}
