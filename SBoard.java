import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * This class is used for displaying the sun counter for regular mode
 * 
 * @author Charles Cai, Eric Liu
 * @version June 10th, 2018
 */

public class SBoard extends Actor {
    // Declare Objects
    private GreenfootImage scoreBoard;
    private Color bg;
    private Color content;
    private Font font;
    private String text;

    // Declare Variables:
    private int width;

    /**
     * Constructor for this class
     * @param width = width of image
     * @param height = height of image
     */
    public SBoard(int width, int height) {
        scoreBoard = new GreenfootImage(width, 30);
        content = new Color(0, 0, 0);
        font = new Font ("New Time Roman", false, true, 15);
        bg = new Color (0, 0, 0, 0);
        scoreBoard.setColor(bg);
        scoreBoard.fill();
        scoreBoard.setFont(font);
        scoreBoard.setColor(content);  
        this.setImage (scoreBoard);
        this.width = width;
    }

    /**
     * Updates this ScoreBar when game starts.
     * Re-write each value changed while game it playing
     * 
     */
    public void update (int score) {
        scoreBoard.clear();
        text = ""+score+"";
        this.update(text, 11);
    }
    
    /**
     * Second method, with different parameter and/or argument
     */
    public void update (String output) {
        // Refill the background with background color        
        scoreBoard.setColor(bg);
        scoreBoard.fill();

        // Write text over the solid background
        scoreBoard.setColor(content);

        // code that centers text
        int centeredY = (width/2) - ((output.length() * 7)/2);

        // Draw the text onto the image
        scoreBoard.drawString(output, centeredY, 16);
    }
    
    /**
     * Third method, with different parameters and/or arguments
     */
    public void update (String output, int one) {
        // Refill the background with background color        
        scoreBoard.setColor(bg);
        scoreBoard.fill();
        // code that centers text
        int centeredY = (width/2) - ((output.length() * one)/4);
        scoreBoard.setColor(content);
        // Draw the text onto the image
        scoreBoard.drawString(output, centeredY, 24);
        this.setImage(scoreBoard);
    }
}
