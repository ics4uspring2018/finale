import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.io.*;
import java.util.*;

/**
 * This super class is used for all of the frozen zombies that appear on screen
 * 
 * @author Eric Liu
 * @version June 12th, 2018
 */

public class Frozen_Zombie extends SmoothMover {
    
    protected int zombie_health = 0; // Store health of zombie object
    protected int width, height;
    protected int transparency = 255; // Should be white
    protected int rotation = 0;
    private int cycleCnt = 0; // For timing the explosion
    protected boolean died_bomb = false; // Has the zombie died by a bomb/land mine?
    public static int level_idx; // Store the current level the user is on
    
    /**
     * Constructor
     * @param health = health of zombie object
     * @param level = current stage that player is on
     */
    public Frozen_Zombie (int health, int level) {
        super(new Vector(180, 0.1)); // Instantiates a new vector object from the super class
        this.zombie_health = health;
        this.level_idx = level;
    }
    
    /**
     * Method is used to animate the death of the zombie
     * For the last zombie, make sure to spawn the appropriate seed packet for that stage
     */
    protected void animateDeath () {
        this.cycleCnt++;
        List<Regular_Zombies> zombies_rem = getWorld().getObjects(Regular_Zombies.class);
        // NOTE: Still need to improve the dying animation, since it's a little sketchy right now...
        // Have to hardcode this in
        if (!died_bomb) {
            if (this.rotation < 90) {
                this.rotation += 3;
                setRotation(Math.min(this.rotation, 90));
                setLocation(getExactX() + 2.5, getExactY() + 2.5);
            }
            // Have to reduce the transparency of the object
            this.transparency -= 5;
            // Check if the transparency goes below or equal to zero
            if (this.transparency <= 0) {
                // Now check if zombie is last one in the world
                if (zombies_rem.size() == 1) {
                    // Now reward user with new seedpacket/trophy
                    getWorld().addObject(new SeedPacketEndStage(this.level_idx), this.getX(), this.getY());
                }
                // Now remove the dead zombie
                getWorld().removeObject(this);
            }
            // Make sure to scale the image and set transparency
            width = (int) (width * 0.95);
            getImage().scale(Math.max(20, width), 100);
            getImage().setTransparency(Math.max(0, this.transparency));
        }
    }
    
    /**
     * Act - do whatever the Frozen_Zombie wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act () {
        
    }    
}
