import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * This class is used to display the difficulty settings screen
 * Here, the user is allowed to adjust the difficulty level for the game by using a slider
 * 
 * @author Eric Liu
 * @version May 17, 2018
 */

public class Music_Settings extends World {

    private Button back, song1_button, song2_button, song3_button, song4_button, song5_button, song6_button;
    private StopButton stop;
    //private Slider s = new Slider();
    private final int rows = 6, rowHeight = getHeight() / (rows * 2 + 2);
    private final int cols = 3, colWidth = getWidth() / cols;
    public static boolean song1_pressed = false, song2_pressed = false, song3_pressed = false, song4_pressed = false, song5_pressed = false, song6_pressed = false;
    private Sound_Info sound_info = null;
    private Player_Info player_info;
    private GreenfootSound music = new GreenfootSound("menu.wav");
    private GreenfootSound song1 = new GreenfootSound("background.wav");
    private GreenfootSound song2 = new GreenfootSound("03 Choose Your Seeds.mp3");
    private GreenfootSound song3 = new GreenfootSound("05 Loonboon.mp3");
    private GreenfootSound song4 = new GreenfootSound("06 Moongrains.mp3");
    private GreenfootSound song5 = new GreenfootSound("10 Ultimate Battle.mp3");
    private GreenfootSound song6 = new GreenfootSound("09 Watery Graves (fast).mp3");

    /**
     * Constructor for objects of class Music_Settings.
     *
     **/
    public Music_Settings (Sound_Info soundinfo, Player_Info playerinfo) {    
        // Create a new world with 1111x700 cells with a cell size of 1x1 pixels.
        super(1111, 700, 1);
        // music.playLoop();
        this.sound_info = soundinfo;
        this.player_info = playerinfo;
        back = new Button("back", new GreenfootImage("back_button.png"));
        stop = new StopButton();
        song1_button = new Button("song1", new GreenfootImage("song1_button.png"));
        song2_button = new Button("song2", new GreenfootImage("song2_button.png"));
        song3_button = new Button("song3", new GreenfootImage("song3_button.png"));
        song4_button = new Button("song4", new GreenfootImage("song4_button.png"));
        song5_button = new Button("song5", new GreenfootImage("song5_button.png"));
        song6_button = new Button("song6", new GreenfootImage("song6_button.png"));
        addObject(song1_button, 400, 150);
        addObject(song2_button, 400, 230);
        addObject(song3_button, 400, 310);
        addObject(song4_button, 400, 390);
        addObject(song5_button, 400, 470);
        addObject(song6_button, 400, 550);
        addObject(back, 1111 / 2, 630);
        addObject(stop, 678, 275);

        // Add label
        //addObject(new Text("Volume:", 17, "white"), 562, 530);
        //s.showValue(true);
        //addObject(s, 700, (getHeight() + rowHeight) / 2 + 150);
        //s.setValue(80);

        // Add the instructions
        // The user is supposed to start on Song 1 and end on Song 3
        addObject(new Text("Pick a song starting from Song 1", 20, "white"), 680, 330);
        addObject(new Text("and ending on Song 6.", 20, "white"), 680, 370);

        addObject(new Text("The six songs are listed below. Simply press the button to choose your song!", 22, "white"), 550, 30);
        addObject(new Text("Press the STOP button to stop the playing music. It will then be set to default", 22, "white"), 560, 60);
        addObject(new Text("Make sure to STOP the music first ", 20, "white"), 660, 470);
        addObject(new Text("before you listen to another one!", 20, "white"), 660, 490);
        //addObject(new Text("Slider", 20, "white"), 680, 500);
        Display();
    }

    public void Display () {
        addObject(new Text("MUSIC SETTINGS", 30, "yellow"), 1111 / 2, 100);
    }

    public void stopBackgroundMusic () {
        this.music.stop();
    }

    /**
     * Act - do whatever the Player_Info wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act () {
        if (song1_button.isPressed()) {
            removeObjects(getObjects(Text.class));
            Display();
            //addObject(new Text("Slider", 20, "white"), 680, 500);
            addObject(new Text("Pick a song starting from Song 1", 20, "white"), 680, 330);
            addObject(new Text("and ending on Song 6.", 20, "white"), 680, 370);
    
            addObject(new Text("The six songs are listed below. Simply press the button to choose your song!", 22, "white"), 550, 30);
            addObject(new Text("Press the STOP button to stop the playing music. It will then be set to default", 22, "white"), 560, 60);
            addObject(new Text("Make sure to STOP the music first ", 20, "white"), 660, 470);
            addObject(new Text("before you listen to another one!", 20, "white"), 660, 490);
            //addObject(new Text("Volume:", 17, "white"), 562, 530);
            //addObject(new Text("Use this slider to adjust the", 17, "white"), 660, 170);
            //addObject(new Text("volume for the song you picked.", 17, "white"), 660, 200);
            addObject(new Text("You have chosen Song 1!", 20, "white"), 660, 450);
            addObject(new Text("Make sure to STOP the music first ", 20, "white"), 660, 470);
            addObject(new Text("before you listen to another one!", 20, "white"), 660, 490);
            //sound_info = new Sound_Info(1, s.getPercentage());
            song1_pressed = true;
            /**if (song2_pressed) {
            song2.stop();
            song2_pressed = false;
            }
            else if (song3_pressed) {
            song3.stop();
            song3_pressed = false;
            }
            else if (song4_pressed) {
            song4.stop();
            song4_pressed = false;
            }
            else if (song5_pressed) {
            song5.stop();
            song5_pressed = false;
            }
            if (song6_pressed) {
            song6.stop();
            song6_pressed = false;
            }**/
            //song1.setVolume(s.getPercentage());
            song1.playLoop();
        }
        if (song2_button.isPressed()) {
            removeObjects(getObjects(Text.class));
            Display();
            //addObject(new Text("Slider", 20, "white"), 680, 500);
            addObject(new Text("Pick a song starting from Song 1", 20, "white"), 680, 330);
            addObject(new Text("and ending on Song 6.", 20, "white"), 680, 370);
    
            addObject(new Text("The six songs are listed below. Simply press the button to choose your song!", 22, "white"), 550, 30);
            addObject(new Text("Press the STOP button to stop the playing music. It will then be set to default", 22, "white"), 560, 60);
            addObject(new Text("Make sure to STOP the music first ", 20, "white"), 660, 470);
            addObject(new Text("before you listen to another one!", 20, "white"), 660, 490);
            //addObject(new Text("Volume:", 17, "white"), 562, 530);
            //addObject(new Text("Use this slider to adjust the", 17, "white"), 660, 170);
            //addObject(new Text("volume for the song you picked.", 17, "white"), 660, 200);
            addObject(new Text("You have chosen Song 2!", 20, "white"), 660, 450);
            addObject(new Text("Make sure to STOP the music first ", 20, "white"), 660, 470);
            addObject(new Text("before you listen to another one!", 20, "white"), 660, 490);
            song2_pressed = true;

            /**if (song1_pressed) {
            song1.stop();
            song1_pressed = false;
            }
            else if (song3_pressed) {
            song3.stop();
            song3_pressed = false;
            }
            else if (song4_pressed) {
            song4.stop();
            song4_pressed = false;
            }
            else if (song5_pressed) {
            song5.stop();
            song5_pressed = false;
            }
            else if (song6_pressed) {
            song6.stop();
            song6_pressed = false;
            }**/
            //song2.setVolume(s.getPercentage());
            song2.playLoop();
        }
        if (song3_button.isPressed()) {
            removeObjects(getObjects(Text.class));
            Display();
            //addObject(new Text("Slider", 20, "white"), 680, 500);
            addObject(new Text("Pick a song starting from Song 1", 20, "white"), 680, 330);
            addObject(new Text("and ending on Song 6.", 20, "white"), 680, 370);
    
            addObject(new Text("The six songs are listed below. Simply press the button to choose your song!", 22, "white"), 550, 30);
            addObject(new Text("Press the STOP button to stop the playing music. It will then be set to default", 22, "white"), 560, 60);
            addObject(new Text("Make sure to STOP the music first ", 20, "white"), 660, 470);
            addObject(new Text("before you listen to another one!", 20, "white"), 660, 490);
            //addObject(new Text("Volume:", 17, "white"), 562, 530);
            //addObject(new Text("Use this slider to adjust the", 17, "white"), 660, 170);
            //addObject(new Text("volume for the song you picked.", 17, "white"), 660, 200);
            addObject(new Text("You have chosen Song 3!", 20, "white"), 660, 450);
            addObject(new Text("Make sure to STOP the music first ", 20, "white"), 660, 470);
            addObject(new Text("before you listen to another one!", 20, "white"), 660, 490);
            song3_pressed = true;
            /**if (song1_pressed) {
            song1.stop();
            song1_pressed = false;
            }
            else if (song2_pressed) {
            song2.stop();
            song2_pressed = false;
            }
            else if (song4_pressed) {
            song4.stop();
            song4_pressed = false;
            }
            else if (song5_pressed) {
            song5.stop();
            song5_pressed = false;
            }
            else if (song6_pressed) {
            song6.stop();
            song6_pressed = false;
            }**/
            //song3.setVolume(s.getPercentage());
            song3.playLoop();
        }
        if (song4_button.isPressed()) {
            removeObjects(getObjects(Text.class));
            Display();
            //addObject(new Text("Slider", 20, "white"), 680, 500);
            addObject(new Text("Pick a song starting from Song 1", 20, "white"), 680, 330);
            addObject(new Text("and ending on Song 6.", 20, "white"), 680, 370);
    
            addObject(new Text("The six songs are listed below. Simply press the button to choose your song!", 22, "white"), 550, 30);
            addObject(new Text("Press the STOP button to stop the playing music. It will then be set to default", 22, "white"), 560, 60);
            addObject(new Text("Make sure to STOP the music first ", 20, "white"), 660, 470);
            addObject(new Text("before you listen to another one!", 20, "white"), 660, 490);
            //addObject(new Text("Volume:", 17, "white"), 562, 530);
            //addObject(new Text("Use this slider to adjust the", 17, "white"), 660, 170);
            //addObject(new Text("volume for the song you picked.", 17, "white"), 660, 200);
            addObject(new Text("You have chosen Song 4!", 20, "white"), 660, 450);
            addObject(new Text("Make sure to STOP the music first ", 20, "white"), 660, 470);
            addObject(new Text("before you listen to another one!", 20, "white"), 660, 490);
            song4_pressed = true;
            /**if (song1_pressed) {
            song1.stop();
            song1_pressed = false;
            }
            else if (song2_pressed) {
            song2.stop();
            song2_pressed = false;
            }
            else if (song3_pressed) {
            song3.stop();
            song3_pressed = false;
            }
            else if (song5_pressed) {
            song5.stop();
            song5_pressed = false;
            }
            else if (song6_pressed) {
            song6.stop();
            song6_pressed = false;
            }**/
            //song4.setVolume(s.getPercentage());
            song4.playLoop();
        }
        if (song5_button.isPressed()) {
            removeObjects(getObjects(Text.class));
            Display();
            //addObject(new Text("Slider", 20, "white"), 680, 500);
            addObject(new Text("Pick a song starting from Song 1", 20, "white"), 680, 330);
            addObject(new Text("and ending on Song 6.", 20, "white"), 680, 370);
    
            addObject(new Text("The six songs are listed below. Simply press the button to choose your song!", 22, "white"), 550, 30);
            addObject(new Text("Press the STOP button to stop the playing music. It will then be set to default", 22, "white"), 560, 60);
            addObject(new Text("Make sure to STOP the music first ", 20, "white"), 660, 470);
            addObject(new Text("before you listen to another one!", 20, "white"), 660, 490);
            //addObject(new Text("Volume:", 17, "white"), 562, 530);
            //addObject(new Text("Use this slider to adjust the", 17, "white"), 660, 170);
            //addObject(new Text("volume for the song you picked.", 17, "white"), 660, 200);
            addObject(new Text("You have chosen Song 5!", 20, "white"), 660, 450);
            addObject(new Text("Make sure to STOP the music first ", 20, "white"), 660, 470);
            addObject(new Text("before you listen to another one!", 20, "white"), 660, 490);
            song5_pressed = true;
            /**if (song1_pressed) {
            song1.stop();
            song1_pressed = false;
            }
            else if (song2_pressed) {
            song2.stop();
            song2_pressed = false;
            }
            else if (song3_pressed) {
            song3.stop();
            song3_pressed = false;
            }
            else if (song4_pressed) {
            song4.stop();
            song4_pressed = false;
            }
            else if (song6_pressed) {
            song6.stop();
            song6_pressed = false;
            }**/
            //song5.setVolume(s.getPercentage());
            song5.playLoop();
        }
        if (song6_button.isPressed()) {
            removeObjects(getObjects(Text.class));
            Display();
            //addObject(new Text("Slider", 20, "white"), 680, 500);
            addObject(new Text("Pick a song starting from Song 1", 20, "white"), 680, 330);
            addObject(new Text("and ending on Song 6.", 20, "white"), 680, 370);
    
            addObject(new Text("The six songs are listed below. Simply press the button to choose your song!", 22, "white"), 550, 30);
            addObject(new Text("Press the STOP button to stop the playing music. It will then be set to default", 22, "white"), 560, 60);
            addObject(new Text("Make sure to STOP the music first ", 20, "white"), 660, 470);
            addObject(new Text("before you listen to another one!", 20, "white"), 660, 490);
            //addObject(new Text("Volume:", 17, "white"), 562, 530);
            //addObject(new Text("Use this slider to adjust the", 17, "white"), 660, 170);
            //addObject(new Text("volume for the song you picked.", 17, "white"), 660, 200);
            addObject(new Text("You have chosen Song 6!", 20, "white"), 660, 450);
            addObject(new Text("Make sure to STOP the music first ", 20, "white"), 660, 470);
            addObject(new Text("before you listen to another one!", 20, "white"), 660, 490);
            song6_pressed = true;
            /**if (song1_pressed) {
            song1.stop();
            song1_pressed = false;
            }
            else if (song2_pressed) {
            song2.stop();
            song2_pressed = false;
            }
            else if (song3_pressed) {
            song3.stop();
            song3_pressed = false;
            }
            else if (song4_pressed) {
            song4.stop();
            song4_pressed = false;
            }
            else if (song5_pressed) {
            song5.stop();
            song5_pressed = false;
            }**/
            //song6.setVolume(s.getPercentage());
            song6.playLoop();
        }
        if (Greenfoot.mouseClicked(stop)) {
            removeObjects(getObjects(Text.class));
            Display();
            addObject(new Text("No music chosen.", 20, "white"), 660, 450);
            Button.pressedsound.play();
            song1.stop();
            song2.stop();
            song3.stop();
            song4.stop();
            song5.stop();
            song6.stop();
            song1_pressed = false;
            song2_pressed = false;
            song3_pressed = false;
            song4_pressed = false;
            song5_pressed = false;
            song6_pressed = false;
        }
        if (Greenfoot.mouseClicked(back)) {
            song1.stop();
            song2.stop();
            song3.stop();
            song4.stop();
            song5.stop();
            song6.stop();
            Greenfoot.setWorld(new Settings_Screen(this.sound_info, this.player_info));
        }
    }
}
