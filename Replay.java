import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * This class is used for the replay button
 * 
 * @author Eric Liu
 * @version May 10, 2018
 */

public class Replay extends Actor {
    
    private GifImage gif = new GifImage("gameover.gif");
    private Sound_Info sound_info;
    private Player_Info player_info;
    
    public Replay (Sound_Info soundinfo, Player_Info playerinfo) {
        this.sound_info = soundinfo;
        this.player_info = playerinfo;
    }
    
    /**
     * Act - do whatever the Replay wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act () {
        setImage(gif.getCurrentImage());
        // Transition to the start screen
        if (Greenfoot.mouseClicked(this)) {
            GameOver world = (GameOver) getWorld();
            world.stopBackgroundMusic();
            Greenfoot.setWorld(new StartScreen(this.sound_info, this.player_info));
        }
    }    
}
