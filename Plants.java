import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * This super class is used for the plants objects
 * 
 * @author Eric Liu
 * @version May 9, 2018
 */

public class Plants extends Actor {
    
    // Declare the PROTECTED variables here
    protected int live_time = 2000; // Initial live time/health of plant objects
    protected GreenfootSound plant_eaten = new GreenfootSound("chomp.wav");
    
    /**
     * Helper method to set the life of the plant
     */
    protected void Set_LiveTime (int time) {
        this.live_time = time;
    }
    
    /**
     * Helper method to check if the zombie is eating up the plant
     */
    protected void Plant_Hit (String fileName, int num_time) {
        // If the zombie object is intersecting with the plant object
        if (isTouching(Zombies.class)) {
            live_time -= 20; // Decrease time/health by 20
            plant_eaten.play(); // Play the plant-eating sound
        }
        // If the health/live time of the plant object drops to below 0, execute the dying animation
        if (live_time <= 0) {
            World world = getWorld();
            Dying_Animation(fileName, num_time);
            world.removeObject(this);
            plant_eaten.stop();
        }
    }
    
    /**
     * Method is used to execute the dying animation of the zombie, using the DeadActor class
     */
    protected void Dying_Animation (String fileName, int num_time) {
        DeadActor dead = new DeadActor(fileName, num_time);
        World world = getWorld();
        world.addObject(dead, getX(), getY());
    }
    
    /**
     * Act - do whatever the Plants wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act () {
    
    }    
}
