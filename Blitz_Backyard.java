import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import javax.swing.*; // For JFrame and JOptionPane
import java.io.*;
import java.util.*;

/**
 * This class serves as the primary class for the blitz game mode
 * This game mode has no levelling up, instead it increases the number of zombies.
 * The game ends when the player has died
 * Need to be careful of when there are too much zombies on the screen -> too much lag
 * 
 * @author Eric Liu
 * @version May 7, 2018
 */

public class Blitz_Backyard extends World {

    private GreenfootImage background = new GreenfootImage("background.png"); // Background picture
    private Button pause; // Pause button
    private Button settings; // Settings button to direct the user to the settings screen
    private Button view_scoreboard; // For viewing the scoreboard
    private Button restart; // For restarting the game
    private String username; // The player's username
    private Stack<GreenfootSound> sounds = new Stack<GreenfootSound>(); // Store a stack of sounds
    private Queue<GreenfootImage> images = new Queue<GreenfootImage>(); // Store a queue of images
    private String sound1 = "menu.wav", sound2 = "chomp.wav", sound3 = "atebrains.wav", sound4 = "zombies_coming.wav", sound5 = "lawnmower.wav", sound6 = "menu.wav", sound7 = "game_end.wav", music_file = "background.wav"; // Initialize these String variables later
    // Theoretically, we only need one background picture since there is only 1 level for now
    private String background1 = "background.png", background2 = "background.png", background3 = "background.png", background4 = "background.png"; // Initialize these String variables later
    private GreenfootSound music = new GreenfootSound(music_file); // Initialize this later
    private SimpleTimer timer; // Use this for the main SimpleTimer object
    private boolean has_lost, has_won; // Boolean for whether the user has lost and won the game
    private boolean new_wave_zombies; // Boolean for whether there is a new wave of zombies -> same as levelling up
    private int level; // Current level of player
    private Player_Info player_info; // Main Player_Info object -> use this to write the scores to the text file
    private final String scores_file = "scores.txt";
    private Reading_and_Writing_Text_Files reading_writing_files_1 = new Reading_and_Writing_Text_Files(scores_file); // Used to write to the scores.txt file
    private int score; // Current score of player
    private ArrayList<Player_Info> players; // ArrayList to store the list of a certain number of players from the scores.txt file
    private int [] rows; // int array to store the coordinates of the rows (y coordinates)
    private int [] cols; // int array to store the coordinates of the columns (x coordinates)
    private Counter sun_counter = new Counter();
    private Counter score_counter = new Counter();
    // Declare and initialize all of the sound effects for the game
    private GreenfootSound menu_sound = new GreenfootSound(sound1), brains_ate = new GreenfootSound(sound3), eating_effect = new GreenfootSound(sound2), zombie_sound = new GreenfootSound(sound4), pea_shooting, zombie_died, plant_died, mower_sound = new GreenfootSound(sound5), lose_sound = new GreenfootSound(sound7), win_sound;
    private Random rng = new Random(); // Use RNG for random spawning
    private boolean paused = false; // Has the user paused the game or not
    private int difficulty; // Current difficulty level chosen for game

    private Sound_Info sound_info; // Object stores the music information
    private boolean easy, medium, hard; // Booleans for which difficulty level is currently active

    /**
     * 1st Constructor for objects of class Backyard.
     */
    public Blitz_Backyard () {    
        // Create a new world with 1111x602 cells with a cell size of 1x1 pixels.
        super(1111, 602, 1);
        int xcoor = 0, ycoor = 50;
        setPaintOrder(Text.class, Plants.class, Zombies.class, Bullet.class, Sun.class); // Change the paint order later
        // getBackground().drawImage(background, xcoor, ycoor);
        GenerateSounds();
        GenerateBackgrounds();
        Set_Row_Coordinates();
        Set_Col_Coordinates();
        Get_Ready();
        if (Music_Settings.song1_pressed) music = new GreenfootSound("background.wav");
        else if (Music_Settings.song2_pressed) music = new GreenfootSound("03 Choose Your Seeds.mp3");
        else if (Music_Settings.song3_pressed) music = new GreenfootSound("05 Loonboon.mp3");
        else if (Music_Settings.song4_pressed) music = new GreenfootSound("06 Moongrains.mp3");
        else if (Music_Settings.song5_pressed) music = new GreenfootSound("10 Ultimate Battle.mp3");
        else if (Music_Settings.song6_pressed) music = new GreenfootSound("09 Watery Graves (fast).mp3");
        else music = new GreenfootSound("background.wav");
        music.playLoop();
        askUsername(); // Ask the user for their username before starting
    }

    /**
     * 2nd Constructor for objects of class Backyard.
     * 
     */
    public Blitz_Backyard (Sound_Info soundinfo, Player_Info playerinfo) {    
        // Create a new world with 1111x602 cells with a cell size of 1x1 pixels.
        super(1111, 602, 1);
        this.sound_info = soundinfo;
        this.player_info = playerinfo;
        this.difficulty = player_info.getDifficulty();
        if (this.difficulty == 1) {
            this.easy = true;
        }
        else if (this.difficulty == 2) {
            this.medium = true;
        }
        else if (this.difficulty == 3) {
            this.hard = true;
        }
        int xcoor = 0, ycoor = 50;
        setPaintOrder(Text.class, Plants.class, Zombies.class, Bullet.class, Sun.class); // Change the paint order later
        // getBackground().drawImage(background, xcoor, ycoor);
        GenerateSounds();
        GenerateBackgrounds();
        Set_Row_Coordinates();
        Set_Col_Coordinates();
        Get_Ready();

        if (Music_Settings.song1_pressed) music = new GreenfootSound("background.wav");
        else if (Music_Settings.song2_pressed) music = new GreenfootSound("03 Choose Your Seeds.mp3");
        else if (Music_Settings.song3_pressed) music = new GreenfootSound("05 Loonboon.mp3");
        else if (Music_Settings.song4_pressed) music = new GreenfootSound("06 Moongrains.mp3");
        else if (Music_Settings.song5_pressed) music = new GreenfootSound("10 Ultimate Battle.mp3");
        else if (Music_Settings.song6_pressed) music = new GreenfootSound("09 Watery Graves (fast).mp3");
        else music = new GreenfootSound("background.wav");

        music.playLoop();
        askUsername(); // Ask the user for their username before starting
    }

    /**
     * Method initializes the rows array with specific y coordinates
     */
    public void Set_Row_Coordinates () {
        rows = new int[5];
        rows[0] = 78;
        rows[1] = 184;
        rows[2] = 306;
        rows[3] = 418;
        rows[4] = 523;
    }

    /**
     * Method initializes the cols array with specific x coordinates
     */
    public void Set_Col_Coordinates () {
        cols = new int[9];
        cols[0] = 326;
        cols[1] = 413;
        cols[2] = 500;
        cols[3] = 588;
        cols[4] = 679;
        cols[5] = 765;
        cols[6] = 857;
        cols[7] = 943;
        cols[8] = 1042;
    }

    /**
     * Method gets the row position for the current y coordinate
     */
    public int Get_Row_Position (int y_coor) {
        int [] row_grid = {0, 137, 246, 357, 462, 569};
        for (int i=0; i<5; i++) {
            if (y_coor > row_grid[i] && y_coor < row_grid[i + 1]) return rows[i];
        }
        return -1;
    }

    /**
     * Method gets the column position for the current x coordinate
     */
    public int Get_Col_Position (int x) {
        int [] col_grid = {280, 364, 449, 543, 632, 721, 812, 897, 985, 1089};
        for (int i=0; i<9; i++) {
            if (x > col_grid[i] && x < col_grid[i + 1]) return cols[i];
        }
        return -1;
    }

    /**
     * Method sets up the lawnmowers on the lawn
     */
    public void Set_Lawnmowers () {
        int [] r = {77, 183, 309, 422, 522};
        int [] c = {242, 240, 226, 219, 214};
        for (int i=0; i<5; i++) addObject(new Lawnmower(), c[i], r[i]);
    }

    /**
     * Method sets up the sidebar
     */
    public void Set_Sidebar () {
        addObject(new WalnutIcon("blitz", 49, 140), 49, 140);
        addObject(new PeaShootIcon("blitz", 49, 236), 49, 236);
        addObject(new SunFlowerIcon("blitz", 49, 332), 49, 332);
        addObject(new BeetRootIcon("blitz", 49, 428), 49, 428);

        addObject(score_counter, 49, 78);
        addObject(sun_counter, 49, 567);
        sun_counter.setValue(400);
    }

    /**
     * Method prepares everything
     * Set up the lawnmowers, sidebar, and shovel
     */
    public void Get_Ready () {
        Set_Lawnmowers();
        Set_Sidebar();
        addObject(new Shovel(1035, 40), 1035, 40);
    }

    /**
     * Method adds a zombie to the world
     */
    public void Add_Zombie (Zombies zombie, int row) {
        addObject(zombie, cols[8] + 70, rows[row]);
    }

    /**
     * Method adds a random zombie to the world onto the specified row
     */
    public void Randomize_Zombie (int row) {
        if (rng.nextInt(2) == 0) Add_Zombie(new Normal_Zombie(this.sound_info, this.player_info, "blitz"), row);
        else Add_Zombie(new Football_Zombie(this.sound_info, this.player_info, "blitz"), row);
    }

    /**
     * Method generates a new wave of zombies
     */
    public void Generate_Wave () {
        for (int i=0; i<5; i++) Randomize_Zombie(i);
    }

    /**
     * Method is used to randomly spawn the sunlight at certain time intervals
     * Use SimpleTimer class for calculating the time intervals
     */
    public void Spawn_Sunlight () {
        Sun sun = new Sun("blitz");
        addObject(sun, cols[rng.nextInt(8)], 0);
        sun.Drop_Sun(); // Call the method contained within the class: Sun
    }

    // Method for getting the value of the sun counter
    public Counter Get_SunCounter () {
        return this.sun_counter;
    }

    // Method for getting the value of the score counter
    public Counter Get_ScoreCounter () {
        return this.score_counter;
    }

    /**
     * Method is used to spawn the new waves of zombies at certain time intervals
     * Use SimpleTimer class for calculating the time intervals
     */
    public void Spawn_Zombie_Wave () {

    }

    /**
     * Method is used to check whether the player has lost the game
     * Occurs when the zombies enter the house (once the lawnmowers have been removed)
     */
    public void Check_Loss () {
        // First check to see if the user has already lost
        if (has_lost) {
            if (this.player_info != null) Greenfoot.setWorld(new GameOver(this.sound_info, this.player_info)); // Only set the game over screen if the user has a positive score
        }
    }

    /**
     * Method is used to update the information that is stored in the Player_Info class
     */
    public void Update_Player_Info () {
        player_info = new Player_Info(this.username, this.score_counter.getValue(), this.level, this.difficulty);
    }

    /**
     * Use this method to write the Player information to the scores.txt file
     */
    public void write_Scores (Player_Info info) {
        reading_writing_files_1.Write(info); // Write the info to the file
    }

    /**
     * Method asks the player for his/her username at the start of the game using jFrame
     */
    public void askUsername () {
        username = JOptionPane.showInputDialog("Please input your username");
    }

    /**
     * Use this to generate all of the upcoming sounds into the stack/queue
     * Only store the objects if it's currently empty
     */
    public void GenerateSounds () {
        if (sounds.isEmpty()) {
            sounds.push(new GreenfootSound(sound1));
            sounds.push(new GreenfootSound(sound2));
            sounds.push(new GreenfootSound(sound3));
        }
    }

    /**
     * Use this to generate all of the upcoming backgrounds into the stack/queue
     * Only store the objects if it's currently empty
     */
    public void GenerateBackgrounds () {
        if (images.isEmpty()) {
            images.enqueue(new GreenfootImage(background1));
            images.enqueue(new GreenfootImage(background2));
            images.enqueue(new GreenfootImage(background3));
        }
    }

    public void stopBackgroundMusic () {
        this.music.stop();
    }

    private long last_added = System.currentTimeMillis(); // Store previous time in milliseconds
    private int unit_time = 1; // 1 unit_time = 10 seconds
    private int num_waves = 0; // For number of waves
    private int curr_wave = 0; // For current wave numbers

    /**
     * Act - do whatever the Button wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act () {
        // Code for pause feature here
        if (Greenfoot.isKeyDown("p")) {
            addObject(new Text("PAUSED", 30, "Red"), 400, 350); // Add the red text in the middle of the screen
            // Add pause code here...
            paused = true;
        }
        // Run all of the other game actvities here
        else {
            Update_Player_Info(); // Always update player info
            long currTime = System.currentTimeMillis();
            // If it's easy mode
            if (this.easy) {
                if (currTime >= last_added + 10000) { // If 10 seconds have passed
                    last_added = currTime;
                    unit_time++;
                    Spawn_Sunlight();
                    Randomize_Zombie(rng.nextInt(5));
                }
                // Only generate a new wave of zombies if the current unit time is a multiple of 3 and is greater than 3
                if (unit_time % 3 == 0 && unit_time != 3) {
                    if (currTime <= last_added + 25) {
                        addObject(new ZombiesAreComing(), 639, 273);
                        num_waves = (unit_time - 3) / 3;
                        Generate_Wave();
                        this.level++; // Increment the current level by 1
                        curr_wave = 1;
                    }
                    if (currTime > last_added + curr_wave * 1000 && currTime < last_added + num_waves * 1000) {
                        curr_wave++;
                        this.level++; 
                        Generate_Wave();
                    }
                }
            }
            // If it's medium mode
            else if (this.medium) {
                if (currTime >= last_added + 8000) { // If 8 seconds have passed
                    last_added = currTime;
                    unit_time++;
                    Spawn_Sunlight();
                    Randomize_Zombie(rng.nextInt(5));
                }
                // Only generate a new wave of zombies if the current unit time is a multiple of 3 and is greater than 3
                if (unit_time % 3 == 0 && unit_time != 3) {
                    if (currTime <= last_added + 25) {
                        addObject(new ZombiesAreComing(), 639, 273);
                        num_waves = (unit_time - 3) / 3;
                        Generate_Wave();
                        this.level++; // Increment the current level by 1
                        curr_wave = 1;
                    }
                    // Set it to 900, instead of 1000 to make it harder
                    if (currTime > last_added + curr_wave * 900 && currTime < last_added + num_waves * 900) {
                        curr_wave++;
                        this.level++; 
                        Generate_Wave();
                    }
                }
            }
            // If it's hard mode
            else if (this.hard) {
                if (currTime >= last_added + 10000) { // If 10 seconds have passed
                    last_added = currTime;
                    unit_time++;
                    Spawn_Sunlight();
                    Randomize_Zombie(rng.nextInt(5));
                }
                // Only generate a new wave of zombies if the current unit time is a multiple of 3 and is greater than 3
                if (unit_time % 3 == 0 && unit_time != 3) {
                    if (currTime <= last_added + 25) {
                        addObject(new ZombiesAreComing(), 639, 273);
                        num_waves = (unit_time - 3) / 3;
                        Generate_Wave();
                        this.level++; // Increment the current level by 1
                        curr_wave = 1;
                    }
                    // Set it to 800, instead of 1000 and 900 to make it harder
                    if (currTime > last_added + curr_wave * 800 && currTime < last_added + num_waves * 8080) {
                        curr_wave++;
                        this.level++; 
                        Generate_Wave();
                    }
                }
            }
        }
    }
}
