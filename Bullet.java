import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * This class is used for the bullets that the plants shoot against zombies
 * 
 * @author Eric Liu
 * @version May 9, 2018
 */

public class Bullet extends Actor {
    
    protected int speed = 4; // Set the speed of the bullets to be 4 initially
    
    protected void SetSpeed (int spd) {
        this.speed = spd;
    }
    
    protected void Check_Boundaries () {
        if (getX() > getWorld().getWidth() - 10) getWorld().removeObject(this);
    }
    
    /**
     * Act - do whatever the Bullet wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act () {
        
    }    
}
