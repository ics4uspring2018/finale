import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.*;
import java.io.*;

/**
 * This class is used for the triple pea shooter
 * 
 * @author Charles Cai, Eric Liu
 * @version June 8th, 2018
 */

public class TriplePeaShooter extends Plant {
    private GreenfootImage imgFrames[];
    private int numFrames = 23;
    private int currImgNum;
    private int cycleCnt = 1;
    // For keeping track of the coordinates
    private int x, y;
    private boolean start_shooting;
    private GifImage gif = new GifImage("triple_peashooter.gif");
    
    /**
     * Constructor
     */
    public TriplePeaShooter (int level) {
        super(level);
        setImage(gif.getCurrentImage());// Add your action code here.
    }
    
    public void shootPea () {
        Regular_Pea pea1 = new Regular_Pea("triplepeashooter");
        Regular_Pea pea2 = new Regular_Pea("triplepeashooter");
        Regular_Pea pea3 = new Regular_Pea("triplepeashooter");
        int x_offset = 30; // Used to position the pea correctly
        getWorld().addObject(pea1, this.getX() + x_offset, this.getY() - 20);
        getWorld().addObject(pea2, this.getX() + x_offset + 30, this.getY() - 10);
        getWorld().addObject(pea3, this.getX() + x_offset + 60, this.getY());
    }
    
    /**
     * Act - do whatever the TriplePeaShooter wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act () {
        setImage(gif.getCurrentImage());// Add your action code here.
        if (this.cycleCnt % 200 == 0) { // Make sure it's half a second
            if (check_Zombies_in_Row()) {
               shootPea();
            }
            this.cycleCnt = 1;
        }
        else {
            this.cycleCnt++;
        }
        Plant_Hit(); // Check for hit by any zombies
    }    
    
}