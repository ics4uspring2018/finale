import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * This class is used for the Snow Pea bullet featured in Regular mode
 * 
 * @author Eric Liu, Charles Cai
 * @version June 12th, 2018
 */

public class SnowPea extends Regular_Bullet {
    /**
     * Constructor
     * @param plant_type = which plant hit the zombie
     */
    public SnowPea (String type) {
        super(type);
    }
    
    /**
     * Act - do whatever the SnowPea wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act () {
        // Remove any bullets if they go out of bounds
        if (getExactX() >= getWorld().getWidth() - 5) {
            getWorld().removeObject(this);
        }
        // NOTE: Still need to change &/ work on this code, since the animation isn't that good...
        if (is_exploding) { // If the pea bullet has made contact with a zombie
            // Make sure to change this image later on (animation is sketchy)
            setImage("peaSplatF.png"); // For splat image
            
            explode_cycleCnt--;
            // Make sure to remove the bullet from the world
            if (explode_cycleCnt == 0) {
                getWorld().removeObject(this);
            }
        }
        // Else move forwards
        else move();
    }    
}
