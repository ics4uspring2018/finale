import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * This class is used for the waiting zombie that is part of the game
 * 
 * @author Eric Liu
 * @version May 30, 2018
 */

public class Zombie_Waiting extends Actor {
    
    public Zombie_Waiting () {
    }
    
    /**
     * Act - do whatever the Zombie_Waiting wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act () {
        // Add your action code here.
    }    
}
