import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * This class is used for the cherry bomb plant
 * 
 * @author Eric Liu
 * @version June 8th, 2018
 */

public class IcePeaShooter extends Plant {
    private GreenfootImage imgFrames[];
    private int numFrames = 23;
    private int currImgNum;
    private int cycleCnt = 0;
    // For keeping track of the coordinates
    private int x, y;
    private boolean start_shooting;
    private GifImage gif = new GifImage("ice_peashooter.gif");
    
    /**
     * Constructor
     */
    public IcePeaShooter (int level) {
        super(level);
        setImage(gif.getCurrentImage());// Add your action code here.
    }
    
    public void shootPea () {
        SnowPea pea1 = new SnowPea("icepeashooter");
        int x_offset = 20; // Used to position the pea correctly
        getWorld().addObject(pea1, this.getX() + x_offset, this.getY() - 5);
    }
    
    /**
     * Act - do whatever the TriplePeaShooter wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act () {
        this.cycleCnt++;
        setImage(gif.getCurrentImage());// Add your action code here.
        if (this.cycleCnt % 250 == 0) {
            if (check_Zombies_in_Row()) {
               shootPea();               
            }
            this.cycleCnt = 0;
        }
        Plant_Hit(); // Check for hit by any zombies
    }    
}
