import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * This class is used for the shovel object that can be used to remove plants in-game
 * 
 * @author Eric Liu
 * @version May 7, 2018
 */

public class Shovel extends Actor {
    
    protected boolean grabbed = false;
    protected int xcoor = 0, ycoor = 0;
    
    public Shovel (int x, int y) {
        this.xcoor = x;
        this.ycoor = y;
    }
    
    public void Remove_Plant () {
        Blitz_Backyard world = (Blitz_Backyard) getWorld();
        // Checks for any initial pressing down from the mouse button
        if (Greenfoot.mousePressed(this) && !grabbed) {
            grabbed = true; // Update the boolean value to true
            MouseInfo mouse_info = Greenfoot.getMouseInfo();
            world.removeObject(this);
            if (mouse_info != null) world.addObject(this, mouse_info.getX(), mouse_info.getY());
            return;
        }
        
        // Now check for any dragging
        if (Greenfoot.mouseDragged(this) && grabbed) {
            MouseInfo mouse_info = Greenfoot.getMouseInfo();
            // Set the location of the shovel to the current mouse coordinates
            if (mouse_info != null) setLocation(mouse_info.getX(), mouse_info.getY());
            return;
        }
        // Now check for the release from the mouse button
        if (Greenfoot.mouseDragEnded(this) && grabbed) {
            // Release the object
            MouseInfo mouse_info = Greenfoot.getMouseInfo();
            int row = mouse_info.getY();
            int col = mouse_info.getX();
            // Check for intersection with the plany objects - if there is any, remove them
            if (!world.getObjectsAt(col, row, Actor.class).isEmpty()) removeTouching(Plants.class);
            setLocation(xcoor, ycoor);
            grabbed = false; // Reset the boolean value
            return;
        }
    }
    
    /**
     * Act - do whatever the Shovel wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act () {
        Remove_Plant();
    }    
}
