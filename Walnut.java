import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * This class is used for the walnut object in the game
 * 
 * @author Eric Liu
 * @version May 9, 2018
 */

public class Walnut extends Plants {
    
    // Declare all of the PROTECTED variables here
    private GifImage walnut_full_life = new GifImage("walnut_full_life.gif");
    private GifImage walnut_half_life = new GifImage("walnut_half_life.gif");
    private int halfLife = 5000; // Health/live time of walnut objects
   
    /**
     * Constructor for Walnut
     */
    public Walnut () {
        setImage(walnut_full_life.getCurrentImage());
        Set_LiveTime(halfLife * 2);
    }
    
    /**
     * Helper method executes the half life animation when the walnut at half life
     */
    private void Half_Dead () {
        // If the live time/health is less than the half life of the walnut, execute the gif animation
        if (live_time < halfLife) setImage(walnut_half_life.getCurrentImage());
        else setImage(walnut_full_life.getCurrentImage());
    }
    
    /**
     * Act - do whatever the Walnut wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act () {
        Plant_Hit("walnut_dead.gif", 1000); // Run the helper method in the super class to see if walnut is hit by zombie
        Half_Dead(); // Run the half_dead animation
    }    
}
