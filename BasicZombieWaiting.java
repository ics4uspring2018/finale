import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * This class is used for the waiting feature for the basic zombie that is used in-game
 * 
 * @author Eric Liu
 * @version May 30, 2018
 */

public class BasicZombieWaiting extends Zombie_Waiting {
    
    private GifImage gif = new GifImage("normal_zombie_waiting.gif");
    
    public BasicZombieWaiting () {
        setImage(gif.getCurrentImage());
    }
    
    /**
     * Act - do whatever the BucketZombieWaiting wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act () {
        setImage(gif.getCurrentImage());
    }    
}
