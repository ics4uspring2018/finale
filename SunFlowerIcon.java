import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * This class is used for the sunflower icon that is displayed on the sidebar in-game
 * 
 * @author Eric Liu
 * @version May 7, 2018
 */

public class SunFlowerIcon extends Sidebar {
    
    private String game_mode; // Store version of game mode
    
    public SunFlowerIcon (String mode, int xcoor, int ycoor) {
        Set_Plant_Cost(50); // Has a cost of 50
        Set_Icon_Coordinates(xcoor, ycoor); // Set the coordinates for the plant icon
        Set_Active_Image("active_sunflower.png");
        Set_Inactive_Image("inactive_sunflower.png");
        this.game_mode = mode;
    }
    
    /**
     * Act - do whatever the BeetRootIcon wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act () {
        Create_Plant(this.game_mode, new Sunflower(this.game_mode)); // Create a new instance of the beetroot class
    }
}
