import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * This class is used for the beetroot icon that is displayed on the sidebar in-game
 * 
 * @author Eric Liu
 * @version May 7, 2018
 */

public class BeetRootIcon extends Sidebar {
    
    private String game_mode; // Store version of game mode
    
    public BeetRootIcon (String mode, int xcoor, int ycoor) {
        Set_Plant_Cost(125); // Has a cost of 125
        Set_Icon_Coordinates(xcoor, ycoor); // Set the coordinates for the plant icon
        Set_Active_Image("active_beetroot.png");
        Set_Inactive_Image("inactive_beetroot.png");
        this.game_mode = mode;
    }
    
    /**
     * Act - do whatever the BeetRootIcon wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act () {
        Create_Plant(this.game_mode, new Beetroot()); // Create a new instance of the beetroot class
    }
}
