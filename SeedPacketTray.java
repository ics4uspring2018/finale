import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * This class is used for the seed packet tray that is used in regular mode
 * 
 * @author Eric Liu
 * @version June 11th, 2018
 */

public class SeedPacketTray extends Actor {
    
    /**
     * Act - do whatever the SeedPacketTray wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act () {
        
    }    
}
