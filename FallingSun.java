import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * This class is used for the falling sunlight objects for the regular mode
 * 
 * @author Eric Liu
 * @version June 6th, 2018
 */

public class FallingSun extends Sun1 {
    
    private boolean is_falling = false; // Is the sunlight object falling to the ground?
    // NOTE: Still need to add an actual gif file!
    private GreenfootImage img = new GreenfootImage("sun.png"); // For the main image
    private boolean has_collected = false; // Has the user collected/clicked on the sunlight?
    private int health = 150;
    
    /**
     * 1st Constructor for this class
     */
    public FallingSun () {
        super.stop();
        super.addForce(new Vector(0.0, 1.5)); // Direction: 0.0, Magnitude: 1.5
    }
    
    /**
     * 2nd Constructor for this class
     * @param x = x coordinate (column pixel for starting position)
     * @param y = y coordinate (row pixel for starting position)
     */
    public FallingSun (double x, double y) {
        setLocation(x, y);
        super.stop();
        super.addForce(new Vector(0.0, 1.5)); // Direction: 0.0, Magnitude: 1.5
    }
    
    /**
     * Act - do whatever the FallingSun wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act () {
        // Rotate it by 1 degree to give it more animation
        setRotation(getRotation() + 1);
        if (Greenfoot.mouseClicked(this) || isMovingToTray) {
            super.moveToTray(); // For when the user has collected the sunlight object
        }
        else {
            // If the cyrrent y coordinate is on the first half of the screen
            if (this.getY() < 500) move();
            else { // Make sure to decrement the health counter by 1 each time
                this.health--;
                if (this.health <= 25) {
                    this.getImage().setTransparency(Math.max(0, 10 * health));
                }
                else if (this.health == 0) {
                    getWorld().removeObject(this);
                }
            }
        }
    }    
}
