import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * This class is used for the cherry bomb plant
 * 
 * @author Eric Liu
 * @version June 8th, 2018
 */

public class Potato extends Plant {
    
    private GifImage gif = new GifImage("potato.gif");
    
    public Potato (int level) {
        super(level);
        setImage(gif.getCurrentImage());
        Set_Health(2000);
    }
    
    /**
     * Act - do whatever the Potato wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act () {
        setImage(gif.getCurrentImage());
        check_Zombies_in_Row(); // First check for zombies that are in the same row as the plant object
        Plant_Hit(); // Check for hit by any zombies
    }    
}
