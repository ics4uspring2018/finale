import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * This class is used for the seed packet for this game
 * The packet is featured at the end of the game
 * 
 * @author Eric Liu
 * @version May 30, 2018
 */

public class Seedpacket extends Actor {
    
    public Sound_Info sound_info; // Sound_Info object
    public Player_Info player_info; // Player_Info object
    /**
     * 1st Constructor for this class
     */
    public Seedpacket () {
        
    }
    
    /**
     * 2nd Constructor for this class
     * @param soundinfo = Sound_Info object
     * @param playerinfo = Player_Info object
     * Use this in the subclasses
     */
    public Seedpacket (Sound_Info soundinfo, Player_Info playerinfo) {
        this.sound_info = soundinfo;
        this.player_info = playerinfo;
    }
    
    /**
     * Act - do whatever the Seedpacket wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act () {
        
    }    
}
