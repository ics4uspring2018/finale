import greenfoot.*; // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * This is the class that is used for the normal zombie objects
 * 
 * @author Eric Liu, Charles Cai
 * @version May 9, 2018
 */

public class Normal_Zombie extends Zombies {
    
    private int counter = 0;
    private GifImage gif = new GifImage("zombie_normal.gif");
    
    /**
     * Constructor for this class
     * @param game_mode = game mode as String
     * Use the param for the super class constructor
     */
    public Normal_Zombie (Sound_Info soundinfo, Player_Info info, String game_mode) {
        super(soundinfo, info, game_mode);
        setImage(gif.getCurrentImage());
        SetHealth(100); // Set health as 100 initially
    }
    
    /**
     * Act - do whatever the Normal_Zombie wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act () {
        counter++;
        setImage(gif.getCurrentImage());
        move(zombie_speed);
        Check_Loss(); // Check if the zombie has entered the house/game has ended
        SetSpeed(-1);  // Set the speed to -0.3
        ZombieHit("zombie_normal_dying.gif", 25, 800, 10); // Run the method from the super class to check if the zombie object is hit
    }    
}
