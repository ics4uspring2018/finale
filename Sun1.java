import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * This class is used for the sunlight objects that are in the Regular game mode
 * 
 * @author Eric Liu
 * @version May 31, 2018
 */

public class Sun1 extends SmoothMover {
    
    protected boolean isMovingToTray;
    protected double deltaX, deltaY; // Store values of delta x and delta y as doubles
    protected final int num_amount = 25; // Number of points the user is awarded with when collecting a falling sunlight object
    protected boolean has_collected = false; // Has the user collected the sunlight object yet?
    
    public Sun1 () {
        // Instantiate a new instance of a Vector
        super(new Vector(0, 0.0));
    }
    
    public void Update_Score () {
        Backyard world = (Backyard) getWorld();
        world.addSunPoints(num_amount); // Make sure to add to the current score by 25
    }
    
    /**
     * Method contains most of the logic for clicking a Sun object
     * as well as moving it onto the tray
     */
    public void moveToTray () {
        deltaX = 50 - this.getExactX();
        deltaY = 40 - this.getExactY();
        super.stop();
        // If the Euclidean distance is less than or equal to 25.0
        if (Math.sqrt(Math.pow(deltaX, 2) + Math.pow(deltaY, 2)) <= 25.0) {
            this.isMovingToTray = false;
            Update_Score(); // Make sure to update the score counter
            Greenfoot.playSound("points.wav");
            getWorld().removeObject(this); // Make sure to remove it from the world if clicked
            this.has_collected = true;
        }
        else {
            int dir = (int) Math.toDegrees(Math.atan2(deltaY, deltaX));
            super.addForce(new Vector(dir, 30.0)); // Add a vector with the specified direction (angle in degrees) and a magnitude of 30
            this.isMovingToTray = true; // Set this to true
        }
        // Now move the sun down to the bottom of the screen
        move();
    }
    
    /**
     * Act - do whatever the Sun1 wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act () {
        
    }    
}
