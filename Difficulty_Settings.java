import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * This class is used to display the difficulty settings screen
 * Here, the user is allowed to adjust the difficulty level for the game by using a slider
 * 
 * @author Eric Liu
 * @version May 17, 2018
 */

public class Difficulty_Settings extends World {
    
    private Button easy, medium, hard, back;
    private Slider s = new Slider();
    private final int rows = 6, rowHeight = getHeight() / (rows * 2 + 2);
    private final int cols = 3, colWidth = getWidth() / cols;
    private boolean song1_pressed, song2_pressed, song3_pressed, song4_pressed, song5_pressed, song6_pressed;
    private Sound_Info sound_info = null;
    private Player_Info player_info;
    private String difflevel;
    private GreenfootSound music = new GreenfootSound("menu.wav");
    
    /**
     * 1st Constructor for objects of class Difficulty_Settings.
     * 
     */
    /*
    public Difficulty_Settings () {    
        // Create a new world with 1111x700 cells with a cell size of 1x1 pixels.
        super(1111, 700, 1);
        // music.playLoop();
        back = new Button("back", new GreenfootImage("back_button.png"));
        easy = new Button("easy", new GreenfootImage("easy_mode.png"));
        medium = new Button("medium", new GreenfootImage("medium_mode.png"));
        hard = new Button("hard", new GreenfootImage("hard_mode.png"));
        back = new Button("back", new GreenfootImage("back_button.png"));
        addObject(easy, 1111/ 2, 310);
        addObject(medium, 1111 / 2, 390);
        addObject(hard, 1111 / 2, 470);
        addObject(back, 1111 / 2, 650);
        
        Display();
    }*/
    
    /**
     * 2nd Constructor for objects of class Difficulty_Settings.
     * 
     */
    public Difficulty_Settings (Sound_Info soundinfo, Player_Info playerinfo) {    
        // Create a new world with 1111x700 cells with a cell size of 1x1 pixels.
        super(1111, 700, 1);
        // music.playLoop();
        this.sound_info = soundinfo;
        this.player_info = playerinfo;
        back = new Button("back", new GreenfootImage("back_button.png"));
        easy = new Button("easy", new GreenfootImage("easy_mode.png"));
        medium = new Button("medium", new GreenfootImage("medium_mode.png"));
        hard = new Button("hard", new GreenfootImage("hard_mode.png"));
        back = new Button("back", new GreenfootImage("back_button.png"));
        addObject(easy, 1111/ 2, 310);
        addObject(medium, 1111 / 2, 390);
        addObject(hard, 1111 / 2, 470);
        addObject(back, 1111 / 2, 650);
        
        Display();
    }
    
    public void Display () {
        addObject(new Text("DIFFICULTY SETTINGS", 30, "yellow"), 1111 / 2, 150);
        // Add the instructions
        addObject(new Text("Pick one of the three difficulty levels: Easy, Medium, and Hard", 20, "white"), 1111 / 2, 250);
    }
    
    public void stopBackgroundMusic () {
        this.music.stop();
    }
    
    /**
     * Method updates the text once the user presses the easy button
     */
    public void Display_Easy () {
        removeObjects(getObjects(Text.class));
        Display();
        addObject(new Text("You have chosen the easy difficulty level!", 20, "white"), 1111 / 2, 200);
    }
    
    /**
     * Method updates the text once the user presses the medium button
     */
    public void Display_Medium () {
        removeObjects(getObjects(Text.class));
        Display();
        addObject(new Text("You have chosen the medium difficulty level!", 20, "white"), 1111 / 2, 200);
    }
    
    /**
     * Method updates the text once the user presses the hard button
     */
    public void Display_Hard () {
        removeObjects(getObjects(Text.class));
        Display();
        addObject(new Text("You have chosen the hard difficulty level!", 20, "white"), 1111 / 2, 200);
    }
    
    /**
     * Act - do whatever the Player_Info wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act () {
        if (Greenfoot.mouseClicked(easy)) {
            Display_Easy();
            this.difflevel = "easy";
            player_info.setDifficulty(1);
        }
        else if (Greenfoot.mouseClicked(medium)) {
            Display_Medium();
            this.difflevel = "medium";
            player_info.setDifficulty(2);
        }
        else if (Greenfoot.mouseClicked(hard)) {
            Display_Hard();
            this.difflevel = "hard";
            player_info.setDifficulty(3);
        }
        if (Greenfoot.mouseClicked(back)) Greenfoot.setWorld(new Settings_Screen(this.sound_info, this.player_info)); // Greenfoot.setWorld(new Ex_Start_Screen(sound_info, player, background_info));
   }
}
