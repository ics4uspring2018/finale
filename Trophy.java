import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Main trophy object that is awarded to user at the end
 * 
 * @author Eric Liu
 * @version June 10th, 2018
 */

public class Trophy extends Actor {
    
    private int transparency = 0; // Set the transparency level to 0
    
    /**
     * Method's used to set the transparency of the image
     */
    public void setAlpha (int alpha) {
        getImage().setTransparency(alpha);
    }
    
    /**
     * Act - do whatever the Trophy wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act () {
        
    }    
}
