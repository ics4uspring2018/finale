import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * This class is used for the cherry bomb seed packet that is displayed onto the seedpacket tray during in-game
 * 
 * @author Eric Liu
 * @version May 30, 2018
 */

public class CherryBombSeedPacket extends Seedpacket {
    
    // Standard card
    private GreenfootImage img = new GreenfootImage("CherrybombSeedPacket.png");
    // For if user clicks on/selects this card
    private GreenfootImage img_selected = new GreenfootImage("CherrybombSeedPacket_selected.png");
    // For if user has already clicked on the card
    public static boolean has_selected;
    
    /**
     * 1st Constructor for this class
     */
    public CherryBombSeedPacket () {
        setImage(img);
        has_selected = false;
    }
    
    /**
     * Method's used to reset the image
     */
    public void resetImage () {
        setImage(img);
    }
    
    /**
     * Act - do whatever the PeashooterSeedpacket wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act () {
        Backyard game = (Backyard) getWorld();
        if (Greenfoot.mouseClicked(this)) {
            if (!has_selected) {
                has_selected = true;
                game.setCherryBombSeedPacketClickStatus(true);
                setImage(img_selected);
            }
            else{ // If already selected, then "de-select" the card
                has_selected = false;
                game.setCherryBombSeedPacketClickStatus(false);
                resetImage();
            }
        }
        
    }    
}