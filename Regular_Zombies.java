import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.*;
import java.io.*;

/**
 * This class is used to define the behavior for the regular zombie
 * 
 * @author Eric Liu
 * @version May 30, 2018
 */

public class Regular_Zombies extends SmoothMover {
    
    // NOTE: The use of the protected keyword!!!
    public static Sound_Info sound_info;
    public static Player_Info player_info;
    protected int zombie_health = 100; // Set it to full health iniitally
    protected boolean isDying; // Is the zombie object dying?
    protected boolean has_won = false; // Have the zomibes won? => just set it to false initially
    protected boolean is_touching = false; // Is the zombie object touching another plant object?
    protected int zombie_speed = 0; // Field variable for zombies' speed
    protected int width, height;
    protected int transparency = 255; // Should be white
    protected int rotation = 0;
    protected Random rng = new Random();
    protected GreenfootSound splat1 = new GreenfootSound("splat2.wav");
    protected GreenfootSound splat2 = new GreenfootSound("splat3.wav");
    protected GreenfootSound scream = new GreenfootSound("scream.wav");
    protected boolean is_alive = true;
    public static int level_idx; // Store the current level the user is on
    public static int difficulty; // Difficulty level
    protected boolean good; // Is Player_Info object null?
    protected int points; // Amount of points that player has
    protected String plant_type; // Store which type of plant hit the zombie
    protected Incinerated_Zombie dead_zombie = new Incinerated_Zombie();
    protected boolean died_bomb = false; // Has the zombie died by a bomb/land mine?
    private int cycleCnt = 0; // For timing the explosion
    protected boolean same_row = false; // Is the zombie in a same row as a plant
    protected boolean ice_pea = false; // Is it an ice/snow pea
    public static boolean done_waves = false; // Have all of the waves already been generated
    
    /**
     * Constructor
     * @param soundinfo = Sound_Info object
     * @param playerinfo = Player_Info object
     * @param diff_level = difficulty chosen by user (1-3)
     * @param level = current stage/level user is on
     * @param done = are all of the waves finished
     */
    public Regular_Zombies (Sound_Info soundinfo, Player_Info playerinfo, int diff_level, int level, boolean done) {
        super(new Vector(180, 0.1)); // Instantiates a new vector object from the super class
        isDying = false;
        this.sound_info = soundinfo;
        this.player_info = playerinfo;
        this.level_idx = level;
        this.difficulty = diff_level;
        this.done_waves = done;
    }
    
    /**
     * Method checks if the plant (cherry bomb) is in same row as the zombie
     * NOTE: USE THIS INSTEAD OF ISTOUCHING()
     * NOTE: Cherry bomb should kill zombies in above and below rows as well!!!
     */
    protected boolean Check_In_Same_Row () {
        this.same_row = false;
        int x = getX();
        int y = getY();
        List<CherryBomb> cherries_world = getWorld().getObjects(CherryBomb.class);
        if (!cherries_world.isEmpty()) {
            // Only shoot if zombie is in same row as peashooter
            for (CherryBomb plant : cherries_world) {
                // Have to check if plant bom is in same row as zombie and if the zombie is right in front of the plant
                if (this.getX() < getWorld().getWidth() - 1 && Math.abs(this.getX() - plant.getX()) <= 50 && Math.abs(plant.getY() - y) <= 160) {
                    // Make sure to update booleans
                    this.same_row = true;
                    return true;
                }
            }
        }
        List<Explosion> explosions_world = getWorld().getObjects(Explosion.class);
        if (!explosions_world.isEmpty()) {
            // Only shoot if zombie is in same row as peashooter
            for (Explosion explosion : explosions_world) {
                // Have to check if plant bom is in same row as zombie and if the zombie is right in front of the plant
                if (this.getX() < getWorld().getWidth() - 1 && Math.abs(this.getX() - explosion.getX()) <= 50 && Math.abs(explosion.getY() - y) <= 160) {
                    // Make sure to update booleans
                    this.same_row = true;
                    return true;
                }
            }
        }
        return false;
    }
    
    /**
     * Method checks if the plant is in front of the zombie
     * NOTE: USE THIS INSTEAD OF ISTOUCHING()
     * NOTE: Cherry bomb should kill zombies in above and below rows as well!!!
     */
    protected boolean Check_Plant_In_Front () {
        this.same_row = false;
        int x = getX();
        int y = getY();
        List<Plant> plants_world = getWorld().getObjects(Plant.class);
        if (!plants_world.isEmpty()) {
            // Only shoot if zombie is in same row as peashooter
            for (Plant plant : plants_world) {
                // Have to check if plant bom is in same row as zombie and if the zombie is right in front of the plant
                if (this.getX() < getWorld().getWidth() - 1 && Math.abs(this.getX() - plant.getX()) <= 50 && Math.abs(plant.getY() - y) <= 25) {
                    // Make sure to update booleans
                    this.same_row = true;
                    return true;
                }
            }
        }
        return false;
    }
    
    /**
     * Method checks if the plant is in same row as the zombie
     * NOTE: USE THIS INSTEAD OF ISTOUCHING()
     * NOTE: Cherry bomb should kill zombies in above and below rows as well!!!
     */
    protected boolean Check_Plant_In_Same_Row () {
        this.same_row = false;
        int x = getX();
        int y = getY();
        List<Plant> plants_world = getWorld().getObjects(Plant.class);
        if (!plants_world.isEmpty()) {
            // Only shoot if zombie is in same row as peashooter
            for (Plant plant : plants_world) {
                // Have to check if plant bom is in same row as zombie and if the zombie is right in front of the plant
                if (this.getX() < getWorld().getWidth() - 1 && Math.abs(plant.getY() - y) <= 25) {
                    // Make sure to update booleans
                    this.same_row = true;
                    return true;
                    // break;
                }
            }
        }
        return false;
    }
    
    /**
     * Method's used to check to see if the zombie has been hit by any plant bullets
     */
    protected void checkForHit () {
        List<Regular_Bullet> bullet_list = getObjectsInRange(25, Regular_Bullet.class);
        Regular_Bullet bullet = null; // Bullet
        // Can optimize to O(1) time, rather than O(N)
        for (Regular_Bullet this_bullet : bullet_list) {
            bullet = this_bullet;
        }
        if (bullet != null) {
            if (!bullet.getExploded()) {
                // Make sure to play the splat sound
                if (rng.nextInt(100) < 50) { // 1st splat sound
                    splat1.play();
                }
                else { // 2nd splat sound
                    splat2.play();
                }
                bullet.setExplode(true); // Since it has now exploded
                plant_type = bullet.getPlantType(); // Get which plant type it is
                // Now deduct health from zombie object, depending on which kind of plant hit it
                if (plant_type.equalsIgnoreCase("peashooter")) {
                    this.zombie_health -= 20;
                }
                else if (plant_type.equalsIgnoreCase("doublepeashooter")) {
                    this.zombie_health -= 22;
                }
                else if (plant_type.equalsIgnoreCase("triplepeashooter")) {
                    this.zombie_health -= 24;
                }
                else if (plant_type.equalsIgnoreCase("icepeashooter")) {
                    this.zombie_health -= 26;
                    this.ice_pea = true;
                }
                else if (plant_type.equalsIgnoreCase("machinegunpeashooter")) {
                    this.zombie_health -= 30;
                }
                else if (plant_type.equalsIgnoreCase("cornshooter")) {
                    this.zombie_health -= 20;
                }
                else if (plant_type.equalsIgnoreCase("watermelon")) {
                    this.zombie_health -= 24;
                }
                else if (plant_type.equalsIgnoreCase("icewatermelon")) {
                    this.zombie_health -= 28;
                }
            }
        }
        
        // Now check for any contact with an cherrby bombs (for now) or landmines
        if (Check_In_Same_Row() && isTouching(CherryBomb.class)) {
            died_bomb = true;
            this.zombie_health -= 100; // Instant death
            // setImage(dead_zombie);
        }
        
        // If zombie is dead (and it is inside the garden), reward player with the specified amount of points
        Backyard world = (Backyard) getWorld();
        if (this.zombie_health <= 0 && this.getX() < world.getWidth() - 25) {
            if (this.player_info != null) {
                good = false;
                this.player_info.addScore(this.points);
            }
            else if (this.player_info == null) good = true;
            this.die();
        }
    }
    
    /**
     * Method is used to animate the death of the zombie
     * For the last zombie, make sure to spawn the appropriate seed packet for that stage
     */
    protected void animateDeath () {
        this.cycleCnt++;
        List<Regular_Zombies> zombies_rem = getWorld().getObjects(Regular_Zombies.class);
        // NOTE: Still need to improve the dying animation, since it's a little sketchy right now...
        // Have to hardcode this in
        if (!died_bomb) {
            if (this.rotation < 90) {
                this.rotation += 3;
                setRotation(Math.min(this.rotation, 90));
                setLocation(getExactX() + 2.5, getExactY() + 2.5);
            }
            // Have to reduce the transparency of the object
            this.transparency -= 5;
            // Check if the transparency goes below or equal to zero
            if (this.transparency <= 0) {
                // Now check if zombie is last one in the world
                if (zombies_rem.size() == 1) {
                    // Now reward user with new seedpacket/trophy
                    if (getWorld().getObjects(SeedPacketEndStage.class).isEmpty()) {
                        getWorld().addObject(new SeedPacketEndStage(this.level_idx), this.getX(), this.getY());
                    }
                }
                // Now remove the dead zombie
                getWorld().removeObject(this);
            }
            // Make sure to scale the image and set transparency
            width = (int) (width * 0.95);
            getImage().scale(Math.max(20, width), 100);
            getImage().setTransparency(Math.max(0, this.transparency));
        }
        // Else if the zombie has died by a bomb
        else {
            getWorld().addObject(dead_zombie, this.getX(), this.getY()); // Add the sprite
            getWorld().removeObject(this);
        }
    }
    
    /**
     * Method's used to check if the zombie has won by entering into the house
     */
    protected void Check_Win () {
        if (!isDying) { // If the zombie hasn't died yet
            if (this.getX() < 5) { // NOTE: Still need to adjust value to check if it's accurate
                this.has_won = true; // Now make sure to set the world screen to the game over
                Backyard world = (Backyard) getWorld();
                world.stopBackgroundMusic(); // stop music
                scream.play();
                this.has_won = true;
                if (player_info == null) {
                    world.Update_Player_Info();
                }
                world.write_Scores(player_info); // Make sure to write the score to the text file first
                if (!good) Greenfoot.setWorld(new GameOver(this.sound_info, this.player_info)); // Now go to the GameOver screen => make sure that player_info isn't null
            }
        }
    }
    

    /**
     * Method is used to update whether the zombie has died
     */
    protected void die () {
        this.isDying = true;
    }
    
    /**
     * Return whether or not the zombie is dying
     */
    protected boolean getDying() {
        return this.isDying;
    }
    
    /**
     * ThIs helper method is used to set the current health of the zombie object
     */
    protected void set_Health (int health) {
        this.zombie_health = health;
    }
    
    /**
     * Method for setting the amount of points that player should be rewarded with for killing a zombie
     */
    protected void set_Points (int num_points) {
        this.points = num_points;
    }
    
    /**
     * Method for returning the amount of points that player currently has
     */
    protected int get_Points () {
        return this.points;
    }
    
    /**
     * Method for setting the type of plant whose bullet that the zombie has been hit with
     */
    protected void set_Plant_Type (String type) {
        this.plant_type = type;
    }
    
    /**
     * Method for returning the health of the zombie
     */
    protected int get_Health () {
        return this.zombie_health;
    }
    
    /**
     * Method for returning whether zombies have won
     */
    protected boolean has_Won () {
        return this.has_won;
    }
    
    /**
     * Method for returning whether zombie is alive or not
     */
    protected boolean is_Alive () {
        return this.is_alive;
    }
    
    /**
     * Is used to set the walking speed of the zombie
     * If it's in collision with a plant subclass, then stop moving
     */
    protected void set_Speed (int speed_num) {
        if (!isDying && Check_Plant_In_Front()) { // isTouching(Plant.class)) {
            this.zombie_speed = 0;
            is_touching = true; // Update boolean value
        }
        else this.zombie_speed = speed_num;
    }
    
    /**
     * Method for returning the speed of the zombie
     */
    protected int get_Speed () {
        return this.zombie_speed;
    }
    
    /**
     * Act - do whatever the Regular_Zombies wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act () {
        
    }    
}
