import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.*;
import java.io.*;

/**
 * This class does not contain any actual code whatsoever, but it is used as a superclass
 * Implement the logic for the zombie eating the plants here...
 * 
 * @author Eric Liu
 * @version May 30, 2018
 */

public class Plant extends Actor {
  
    // NOTE: The use of the protected keyword!!!
    protected int health = 2000; // Set it to full health iniitally
    protected boolean is_alive; // Just set it to true initially
    protected boolean has_won; // Just set it to false initially
    protected int speed = 0; // FIeld variable for speed for the plants' bullets
    protected GreenfootSound plant_eaten = new GreenfootSound("chomp.wav"); // Play chomp sound effect
    protected boolean start_attacking; // Whether plant object can shoot peas/attack
    protected boolean same_row; // Whether plant is in same row as zombie
    protected int x, y; // Store x and y coordinates of plant object
    protected int level_idx = 0; // Store current stage user is on
    protected boolean last = false; // Is zombie the last one?
    
    /**
     * 1st Constructor
     */
    public Plant (int level) {
        this.is_alive = true;
        this.has_won = false;
        this.level_idx = level;
    }
    
    /**
     * Use this to set the live time/health of the plant object from the subclasses
     */
    protected void Set_Health (int live_time) {
        this.health = live_time;
    }
    
    /**
     * Method is used to check for zombies that are in the same row as the plant
     */
    protected boolean check_Zombies_in_Row () {
        this.start_attacking = false;
        this.same_row = false;
        this.x = getX();
        this.y = getY();
        List<Regular_Zombies> zombies_world = getWorld().getObjects(Regular_Zombies.class);
        if (!zombies_world.isEmpty()) {
            // Only shoot if zombie is in same row as peashooter
            for (Regular_Zombies zombie : zombies_world) {
                if (zombie.getX() < getWorld().getWidth() - 1 && Math.abs(zombie.getY() - this.y) <= 25) {
                    // Make sure to update booleans
                    this.same_row = true;
                    this.start_attacking = true;
                    return true;
                }
            }
        }
        return false;
    }
    
    /**
     * Method is used to check for zombies that are in the same row as the plant and returns a boolean
     */
    protected boolean check_Zombies_in_Row1 () {
        this.start_attacking = false;
        this.same_row = false;
        this.x = getX();
        this.y = getY();
       List<Regular_Zombies> zombies_world = getWorld().getObjects(Regular_Zombies.class);
        if (!zombies_world.isEmpty()) {
            // Only shoot if zombie is in same row as peashooter
            for (Regular_Zombies zombie : zombies_world) {
                // Have to check if plant bom is in same row as zombie and if the zombie is right in front of the plant
                if (this.getX() < getWorld().getWidth() - 1 && Math.abs(this.getX() - zombie.getX()) <= 50 && Math.abs(zombie.getY() - y) <= 25) {
                    // Make sure to update booleans
                    return true;
                }
            }
        }
        return false;
    }
    
    /**
     * Use this to check for any zombie objects in world that are currently eating/damaging the plants
     */
    protected void Plant_Hit () {
        // Best to just hard code this bit in
        if (check_Zombies_in_Row1()) {
            // Amount of damage dealth has to do with the type of zombie obviously...
            // NOTE: Still need to experiment with constants for decrementing the health by (cannot be too fast or too slow)
            if (isTouching(Bucketzombie.class)) { // Have this deal damage 25 damage
                this.health -= 8;
                plant_eaten.play();
            }
            else if (isTouching(Conezombie.class)) { // Have this deal damage 30 damage
                this.health -= 6;
                plant_eaten.play();
            }
            else if (isTouching(Normalzombie.class)) { // Have this deal damage 20 damage
                this.health -= 4;
                plant_eaten.play();
            }
            else if (isTouching(Babyzombie.class)) { // Have this deal damage 15 damage
                this.health -= 3;
                plant_eaten.play();
            }
        }
         // If the health/live time of the plant object drops to below or equal to 0, execute the dying animation
        if (this.health <= 0) {
            Backyard world = (Backyard) getWorld();
            // NOTE: We don't actually have a dying animation yet...
            world.removeObject(this); // Just remove this object for now..
            plant_eaten.stop(); // Stop playing the sound effect
        }
    }
    
    /**
     * Method checks if the zombie is the last one
     */
    protected void check_LastZombie () {
        Backyard world = (Backyard) getWorld();
        List<Regular_Zombies> zombies_rem = world.getObjects(Regular_Zombies.class);
        if (zombies_rem.size() == 1) this.last = true;
    }
    
    /**
     * Method is used to execute the dying animation of the zombie, using the DeadActor class
     */
    protected void Dying_Animation (String fileName, int num_time) {
        DeadActor dead = new DeadActor(fileName, num_time);
        World world = getWorld();
        world.addObject(dead, getX(), getY());
    }
    
    // Method for returning the health of the plant
    protected int get_Health () {
        return this.health;
    }
    
    // Method for returning whether the user has won the game
    protected boolean has_Won () {
        return this.has_won;
    }
    
    // Method for returning whether the plant is still alive
    protected boolean is_Alive () {
        return this.is_alive;
    }
    
    // Method for returning the speed of the plant
    protected int get_Speed () {
        return this.speed;
    }
    
    /**
     * Act - do whatever the Plant wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act () {        
    }    
}
