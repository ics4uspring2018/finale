import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * This class is used for the sunlight objects that spawn randomly in-game
 * 
 * @author Eric Liu, Charles Cai
 * @version May 7, 2018
 */

public class Sun extends Actor {

    private GifImage gif = new GifImage("sun.gif");
    private boolean sun_fall = false;
    private int speed = 2;
    private String game_mode; // Store which game mode the user chose
    protected GreenfootSound point = new GreenfootSound("points.wav");
    
    /**
     * Constructor for this class
     */
    public Sun (String mode) {
        setImage(gif.getCurrentImage());
        this.game_mode = mode;
    }
    
    /**
     * Stop the sun when it reaches the bottom of the screen
     * Alternatively, we can also remove the sun when it gets to the bottom
     */
    public void Sun_Fall () {
        move(speed);
        if (getY() >= getWorld().getHeight() - 30) {
            speed = 0;
        }
    }
    
    /**
     * Let the sun objects fall down and turns them 90 degrees CCW
     */
    public void Drop_Sun () {
        sun_fall = true;
        turn(90);
    }
    
    public void act () {
        setImage(gif.getCurrentImage()); // Set the current image for the gif
        if (sun_fall) Sun_Fall(); // Let the sunlight fall
        // If the user collects the sun object by clicking on it then increment the sun counter by 25
        if (Greenfoot.mouseClicked(this)) {
            point.play(); // Play the point-collection song 
            // If the current game mode is regular
            if (this.game_mode.equalsIgnoreCase("regular")) {
                Backyard world = (Backyard) getWorld();
                Counter sun_cnt = world.Get_SunCounter();
                world.removeObject(this);
                sun_cnt.add(25);
            }
            else {
                Blitz_Backyard world = (Blitz_Backyard) getWorld();
                Counter sun_cnt = world.Get_SunCounter();
                world.removeObject(this);
                sun_cnt.add(25);
            }
        }
    }
}
