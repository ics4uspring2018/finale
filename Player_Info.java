import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Stores info for the player, including the username
 * 
 * @author Eric Liu
 * @version March 9, 2018
 */
public class Player_Info extends Actor implements Comparable<Player_Info> {
    
    private String name;
    private int score, level, diff; // score = score of player, level = current level of player, and diff = chosen difficulty level
   
    /**
     * Constructor
     * @param username = username of player
     * @param scorenum = score of player
     * @param leveltype = level that the player was at before dying
     * @param diff_level = difficulty level chosen by player
     **/
    public Player_Info (String username, int scorenum, int leveltype, int diff_level) {
        this.name = username;
        this.score = scorenum;
        this.level = leveltype;
        this.diff = diff_level;
    }
    
    /**
     * Return the username of the player
     */
    public String getName () {
        return this.name;
    }
    
    /**
     * Return the score of the player
     */
    public int getScore () {
        return this.score;
    }
    
    /**
     * Used to increment the player's score
     * Most notably used when a zombie object dies in world
     */
    public void addScore (int points) {
        this.score += points;
    }
    
    public int getLevel () {
        return this.level;
    }
    
    public int getDifficulty () {
        return this.diff;
    }
    
    /**
     * Use this method to set the difficulty level
     * @param difflevel: 1 = easy, 2 = medium, 3 = hard
     */
    public void setDifficulty (int difflevel) {
        this.diff = difflevel;
    }
    
    @Override
    public int compareTo (Player_Info s) { // Is used to sort the list in reverse order (greatest to least)
        return s.score - this.score;
    }
    
    /**
     * Act - do whatever the Player_Info wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act () {
        
    }    
}
