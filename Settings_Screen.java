import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.*;
import java.io.*;

/**
 * This class is used to display the settings screen when the user clicks the button in startscreen
 * The features include: changing music, adjusting volume, resetting the scoreboard, and adjusting the difficulty level 
 * 
 * @author Eric Liu
 * @version May 16, 2018
 */

public class Settings_Screen extends World {

    private Button back, music_settings, difficulty_settings, reset_scores; // Declare all of the Button objects
    private Sound_Info sound_info; // Store the sound_info - e.g.: volume and chosen song
    private Player_Info player_info; // Store the player information - store only the chosen difficulty for now
    private GreenfootSound music = new GreenfootSound("menu.wav");
    private FileWriter fileWriter;
    private BufferedWriter bufferedWriter;
    
    /**
     * 1st Constructor for objects of class Settings_Screen.
     * 
     */
    /*
    public Settings_Screen () {    
        // Create a new world with 1111x700 cells with a cell size of 1x1 pixels.
        super(1111, 700, 1);
        back = new Button("back", new GreenfootImage("back_button.png"));
        music_settings = new Button("music_settings", new GreenfootImage("music_settings_button.png"));
        difficulty_settings = new Button("difficulty_settings", new GreenfootImage("difficulty_settings_button.png"));
        reset_scores = new Button("reset_scores", new GreenfootImage("reset_scores_button.png"));
        addObject(music_settings, 1111 / 2, 300);
        addObject(difficulty_settings, 1111 / 2, 400);
        addObject(reset_scores, 1111 / 2, 500);
        addObject(back, 1111 / 2, 650);
        Display();
        music.playLoop();
    }*/
    
    /**
     * 2nd Constructor for objects of class Settings_Screen.
     * 
     */
    public Settings_Screen (Sound_Info soundinfo, Player_Info playerinfo) {    
        // Create a new world with 1111x700 cells with a cell size of 1x1 pixels.
        super(1111, 700, 1);
        this.sound_info = soundinfo;
        this.player_info = playerinfo;
        back = new Button("back", new GreenfootImage("back_button.png"));
        music_settings = new Button("music_settings", new GreenfootImage("music_settings_button.png"));
        difficulty_settings = new Button("difficulty_settings", new GreenfootImage("difficulty_settings_button.png"));
        reset_scores = new Button("reset_scores", new GreenfootImage("reset_scores_button.png"));
        addObject(music_settings, 1111 / 2, 300);
        addObject(difficulty_settings, 1111 / 2, 400);
        addObject(reset_scores, 1111 / 2, 500);
        addObject(back, 1111 / 2, 650);
        Display();
        music.playLoop();
    }
    
    /**
     * This method is used to display the text on-screen
     */
    public void Display () {
        addObject(new Text("SETTINGS", 30, "yellow"), 1111 / 2, 150);
    }
    
    /**
     * Method is used to indicate that the user has resetted the highscores/scoreboard
     */
    public void Display_Reset_Scores () {
        removeObjects(getObjects(Text.class));
        Display();
        addObject(new Text("You have cleared the scoreboard!", 20, "white"), 1111 / 2, 250);
    }
    
    public void stopBackgroundMusic () {
        this.music.stop();
    }
    
    /**
     * Act - do whatever the Player_Info wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act () {
        if (back.isPressed()) {
           stopBackgroundMusic();
           Greenfoot.setWorld(new StartScreen(this.sound_info, this.player_info)); // Go back to the starting screen
        }
        else if (music_settings.isPressed()) {
           stopBackgroundMusic();
           Greenfoot.setWorld(new Music_Settings(this.sound_info, this.player_info)); // Go back to the starting screen
        }
        else if (difficulty_settings.isPressed()) {
           stopBackgroundMusic();
           Greenfoot.setWorld(new Difficulty_Settings(this.sound_info, this.player_info)); // Go back to the starting screen
        }
        else if (reset_scores.isPressed()) {
           Display_Reset_Scores();
            // Now reset the scores
            try {
                fileWriter = new FileWriter("scores.txt", false); // Overwrite and clear the data
                bufferedWriter = new BufferedWriter(fileWriter);
                bufferedWriter.write(""); // Clear everything
            }
            catch (IOException ex) {
                System.out.println("Error writing to file 'scores.txt'");
            }
        }
   }
}
