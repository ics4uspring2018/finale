import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * This class is used for displaying the starburst effect of this game
 * 
 * @author Eric Liu
 * @version May 30, 2018
 */

public class Starburst extends Actor {
    
    public Sound_Info sound_info; // Sound_Info object
    public Player_Info player_info; // Player_Info object
    
    /**
     * 1st Constructor for this class
     */
    public Starburst () {
        
    }
    
    /**
     * 2nd Constructor for this class
     * @param soundinfo = Sound_Info object
     * @param playerinfo = Player_Info object
     */
    public Starburst (Sound_Info soundinfo, Player_Info playerinfo) {
        this.sound_info = soundinfo;
        this.player_info = playerinfo;
    }
    
    /**
     * Method's used to set the transparency of the image
     */
    public void setAlpha (int alpha) {
        getImage().setTransparency(alpha);
    }
    
    /**
     * Act - do whatever the Starburst wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act () { 
        
    }    
}
