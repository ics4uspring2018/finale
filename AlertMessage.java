import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * This class is used to display the alert message onto the world screen
 * 
 * @author Eric Liu
 * @version May 30, 2018
 */

public class AlertMessage extends Actor {
    
    private String [] imgMessages = {"StartReady.png", "StartSet.png", "StartPlant.png", "message_clickOnFallingSun.png", "message_yourHouse.png", "ZombiesWon.png"};
    
    /**
     * Constructor for this class
     */
    public AlertMessage (int message_ind) {
        setImgMessage(message_ind);
    }
    
    /**
     * Method sets the message image
     * @param message_ind = index of message to be displayed in String array
     */
    public void setImgMessage (int message_ind) {
        setImage(imgMessages[message_ind]);
    }
    
    /**
     * Act - do whatever the AlertMessage wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act () {
        
    }    
}
