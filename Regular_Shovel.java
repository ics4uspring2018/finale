import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * This class is used for the shovel that is used in the regular mode
 * The code for this is exactly the same as that for the shovel used in blitz mode
 * 
 * @author Cindy Cao, Eric Liu
 * @version June,11,2018
 */

public class Regular_Shovel extends Actor {
    
    protected boolean grabbed = false; // Has the shovel been grabbed by the user
    protected boolean removed = false; // Has the shovel removed any plants?
    protected int xcoor = 0, ycoor = 0; // Initial x and y coordinates
    protected int removed_x,  removed_y; // Make sure to store the coordinates of the removed plant (for resetting the boolean 2d array)
    
    /**
     * Constructor
     * @param x = x coordinate
     * @param y = y coordinate
     */
    public Regular_Shovel (int x, int y) {
        this.xcoor = x;
        this.ycoor = y;
    }
    
    public void Remove_Plant () {
        Backyard world = (Backyard) getWorld();
        // Checks for any initial pressing down from the mouse button
        if (Greenfoot.mousePressed(this) && !grabbed) {
            grabbed = true; // Update the boolean value to true
            MouseInfo mouse_info = Greenfoot.getMouseInfo();
            world.removeObject(this);
            if (mouse_info != null) world.addObject(this, mouse_info.getX(), mouse_info.getY());
            return;
        }
        
        // Now check for any dragging
        if (Greenfoot.mouseDragged(this) && grabbed) {
            MouseInfo mouse_info = Greenfoot.getMouseInfo();
            // Set the location of the shovel to the current mouse coordinates
            if (mouse_info != null) setLocation(mouse_info.getX(), mouse_info.getY());
            return;
        }
        
        // Now check for the release from the mouse button
        if (Greenfoot.mouseDragEnded(this) && grabbed) {
            // Release the object
            MouseInfo mouse_info = Greenfoot.getMouseInfo();
            int row = mouse_info.getY();
            int col = mouse_info.getX();
            // Check for intersection with the plany objects - if there is any, remove them
            if (!world.getObjectsAt(col, row, Actor.class).isEmpty()) {
                // Make sure to store the coordinates
                this.removed_x = col;
                this.removed_y = row;
                removeTouching(Plant.class);
                this.removed = true;
            }
            // world.addObject(new Regular_Shovel(1035, 40), this.xcoor, this.ycoor); // Now reset the position of the shovel
            world.addObject(new Regular_Shovel(this.xcoor, this.ycoor), this.xcoor, this.ycoor);
            setLocation(xcoor, ycoor);
            this.grabbed = false; // Reset the boolean value
            return;
        }
    }
    
    /**
     * Method is used to return whether or not any plants have been removed by the shovel
     */
    public boolean getRemoved () {
        return this.removed;
    }
    
    /**
     * Method is used to get the x coordinate of the removed plant
     */
    public int getRemovedRow () {
        return this.removed_x;
    }
    
    /**
     * Method is used to get the y coordinate of the removed plant
     */
    public int getRemovedCol () {
        return this.removed_y;
    }
    
    /**
     * Act - do whatever the Shovel wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act () {
        this.removed = false;
        Remove_Plant();
    }    
}
