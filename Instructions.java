import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * This class is used to display the instructions on the instructions screen
 * 
 * @author Eric Liu
 * @version May 16, 2018
 */

public class Instructions extends World {

    private Button back; // Declare Button object for the back button
    private Button about, help; // Declare Button objects for the about and help buttons
    private GreenfootSound music = new GreenfootSound("menu.wav");
    private Sound_Info sound_info = null;
    private Player_Info player_info;
    

    
    /**
     * Constructor for objects of class Instructions.
     * 
     */
    public Instructions (Sound_Info soundinfo, Player_Info playerinfo) {
        // Create a new world with 1111x700 cells with a cell size of 1x1 pixels.
        super(1111, 700, 1);
        this.sound_info = soundinfo;
        this.player_info = playerinfo;
        back = new Button("back", new GreenfootImage("back_button.png"));
        about = new Button("about", new GreenfootImage("about_button.png"));
        help = new Button("help", new GreenfootImage("help_button.png"));
        addObject(about, 1111 / 2, 250);
        addObject(help, 1111 / 2, 350);
        addObject(back, 1111 / 2, 550);
        Display();
        music.playLoop();
    }
    
    /**
     * This method is used to display the text on-screen
     */
    public void Display () {
        addObject(new Text("INSTRUCTIONS", 30, "yellow"), 1111 / 2, 150);
    }
    
    public void stopBackgroundMusic () {
        this.music.stop();
    }
    
    /**
     * Act - do whatever the Player_Info wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act () {
        if (back.isPressed()) {
           stopBackgroundMusic();
           Greenfoot.setWorld(new StartScreen(this.sound_info, this.player_info)); // Go back to the starting screen
        }
        else if (about.isPressed()) {
           stopBackgroundMusic();
           Greenfoot.setWorld(new About(this.sound_info, this.player_info)); // Go to the about screen
        }
        else if (help.isPressed()) {
            stopBackgroundMusic();
            Greenfoot.setWorld(new Help(this.sound_info, this.player_info)); // Go to the help screen
        }
    }
}
