import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * This class is used for executing the dying animations of dead objects/actors
 * 
 * @author Eric Liu
 * @version May 9, 2018
 */

public class DeadActor extends Actor {
    
    private GifImage gif;
    private int num_time;
    
    public DeadActor (String fileName, int amount) {
        gif = new GifImage(fileName);
        setImage(gif.getCurrentImage());
        this.num_time = amount;
    }
    
    private long last_time = System.currentTimeMillis();
    
    /**
     * Act - do whatever the DeadActor wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act () {
        // Now remove the dead actors
        long currTime = System.currentTimeMillis();
        setImage(gif.getCurrentImage());
        if (currTime >= last_time + num_time) {
            last_time = currTime;
            getWorld().removeObject(this);
        }
    }    
}
