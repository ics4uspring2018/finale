import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * This class defines most of the behaviour for the sunflower plant that can be found in the regular mode
 * 
 * @author Eric Liu
 * @version May 30, 2018
 */

public class Regular_Sunflower extends Plant {
    
    private GreenfootImage [] imgFrames;
    private int numFrames = 53; // Number of frames for hardcoding the animation
    private int curr_ImgNum;
    private int cycleCnt;
    private int x, y;
    private boolean can_spawn;
    private GifImage sunflower_gif = new GifImage("sunflower.gif"); // Use this for the actual gif animation
    private SimpleTimer timer = new SimpleTimer(); // Used for spawning new sunlight objects into the world
    
    /**
     * 1st Constructor for this class
     */
    public Regular_Sunflower (int level) {
        super(level);
        imgFrames = new GreenfootImage[numFrames];
        setImage(sunflower_gif.getCurrentImage()); // Use gif file -> considerably easier than hardcoding it
        curr_ImgNum = 0;
        cycleCnt = 1;
        can_spawn = true;
        Set_Health(2000);
    }
    
    /**
     * Method for spawning sunlight objects
     */
    public void spawnSun (Regular_Sunflower sunflower) {
        getWorld().addObject(new SpawnedSun(sunflower), getX(), getY());
    }
    
    /**
     * Method for updating whether or not a sunflower has fully recharged enough to spawn for a new SpawnedSun object
     */
    public void updateCanSpawn (boolean spawn_status) {
        can_spawn = spawn_status;
    }
    
    /**
     * Method is used for updating the images in the animation for objects of this class
     * Can also use the gif file for sunflowers => method becomes necessary
     */
    public void updateImage () {
        setImage(imgFrames[curr_ImgNum]);
        if (curr_ImgNum == numFrames - 1) curr_ImgNum = 0; // Cycle through
        else curr_ImgNum++;
    }
    
    /**
     * Act - do whatever the Regular_Sunflower wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act () {
        cycleCnt++;
        setImage(sunflower_gif.getCurrentImage());
        // Only spawn at 500 milliseconds => can use object timer instead
        if (cycleCnt % 500 == 0 && can_spawn) {
            spawnSun(this);
        }
        check_Zombies_in_Row(); // First check for zombies that are in the same row as the plant object
        Plant_Hit(); // Check for hit by any zombies
    }    
}
