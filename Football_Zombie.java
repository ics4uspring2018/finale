    import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * This class is used for the football zombie in the game
 * May decide to add more zombies into the game for future updates
 * 
 * @author Eric Liu, Charles Cai
 * @version May 8, 2018
 */

public class Football_Zombie extends Zombies {
    
    private GifImage zombie_gif = new GifImage("zombie_football.gif");
    private GifImage dead_gif = new GifImage("zombie_football_dying.gif"); // Not needed
   
    /**
     * Constructor for this class
     * @param game_mode = game mode as String
     * Use the param for the super class constructor
     */
    public Football_Zombie (Sound_Info soundinfo, Player_Info info, String mode) {
        super(soundinfo, info, mode);
        setImage(zombie_gif.getCurrentImage());
        SetHealth(100);
    }
    
    /**
     * Act - do whatever the Football_Zombie wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act () {
        setImage(zombie_gif.getCurrentImage());
        move(zombie_speed);
        Check_Loss(); // Check if the zombie has entered the house/game has ended
        SetSpeed(-1);  // Set the speed to -1
        ZombieHit("zombie_football_dying.gif", 20, 600, 20); // Run the method from the super class to check if the zombie object is hit
    }    
}
