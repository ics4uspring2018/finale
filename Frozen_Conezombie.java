import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * This class is used for displaying a frozen cone zombie
 * 
 * @author Eric Liu
 * @version June 12th, 2018
 */

public class Frozen_Conezombie extends Frozen_Zombie {
    
    private int cycleCnt = 0; // For timing
    
    private long lastTime = System.currentTimeMillis(); // Get last time
    
    /**
     * Constructor
     * @param health = health of zombie object
     */
    public Frozen_Conezombie (int health, int level) {
        super(health, level);
    }
    
    /**
     * Act - do whatever the Frozen_Conezombie wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act () {
        this.cycleCnt++;
        if (this.cycleCnt % 200 == 0 && !getWorld().getObjects(Frozen_Conezombie.class).isEmpty()) { // Remove the explosion after approximately 2 seconds
            Conezombie zombie = new Conezombie(Conezombie.sound_info, Conezombie.player_info, Conezombie.difficulty, Conezombie.level_idx, Conezombie.done_waves);
            getWorld().addObject(zombie, this.getX(), this.getY());
            getWorld().removeObject(this);
            zombie.set_Health(this.zombie_health);
            this.cycleCnt = 0;
        }
    }     
}
