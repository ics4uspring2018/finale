import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * This class is used to spawn any sunlight objects
 * 
 * @author Eric Liu
 * @versionMay 31, 2018
 */

public class SpawnedSun extends Sun1 {
    
    // Amount of life left: 300
    private int lifeLeft = 300;
    private int cycleCnt = 1; // Keep track of the current count in the cycle (used for animation)
    private GreenfootImage [] imgFrames = new GreenfootImage[10];
    // Double arrays used for shifting
    private double [] xShift = new double[10];
    private double [] yShift = new double[10];
    // Double variables used for initializing both coordinates in doubles
    private double xInit;
    private double yInit;
    // Sunflower object used for spawning the sunlight
    private Regular_Sunflower spawningSunFlower;
    
    /**
     * Constructor for this class
     */
    public SpawnedSun (Regular_Sunflower sunflower) {
        this.spawningSunFlower = sunflower;
        // Fill up the array with GreenfootImages of sun
        for (int i=0; i<9; i++) {
            imgFrames[i] = new GreenfootImage("sun.png");
            // Compute the x and y values needed for the location shifts
            // Used for the initial animation when the sun is spawned by the sunflower
            double img_angle = -1 * (Math.PI / 180) * (120 + 18 * i); // Angle used for the iamge - convert from degrees to radians (multiply by PI / 180)
            xShift[i] = 0.8 * (i + 1) * Math.cos(img_angle);
            yShift[i] = 0.8 * (i + 1) * Math.sin(img_angle);
            
            // Compute the scale for this image frame
            int img_Scale = (int) (4 + i * 10);
            // Make sure to scale for the images
            imgFrames[i].scale(img_Scale, img_Scale);
        }
        // Hardcode this bit in: set the picture at the last index to the sunlight
        imgFrames[9] = new GreenfootImage("sun.png");
        setImage(imgFrames[0]); // Set the first image of the array for now
        super.stop();
    }
    
    /**
     * Act - do whatever the SpawnedSun wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act () {
        // Initialize the x and y coordinates
        xInit = this.getX();
        yInit = this.getY();
        // Make sure to hardcode this animation
        if (cycleCnt < 10) {
            setImage(imgFrames[cycleCnt]);
            setLocation((int) (xInit + xShift[cycleCnt]), (int) (yInit + yShift[cycleCnt]));
            cycleCnt++;
        }
        setRotation(getRotation() + 1);
        if (Greenfoot.mouseClicked(this) || isMovingToTray) {
            super.moveToTray(); // Update the score counter by 25
            spawningSunFlower.updateCanSpawn(true); // For future spawning
        }
        else {
            move();
            // Make sure to deduct the amount of life left by 1
            lifeLeft--; // Now decrement the "health" of the sunlight object by 1
            if (lifeLeft <= 25) {
                this.getImage().setTransparency(Math.max(0, 10 * lifeLeft));
            }
            // Make sure to remove the object from the world if its health drops to zero
            else if (lifeLeft == 0) {
                getWorld().removeObject(this);
            }
        }
    }
}
