import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * This class is used for the final explosions for both cherry bombs and landmines
 * Not supposed to contain any code
 * 
 * @author Eric Liu
 * @version June 12th, 2018
 */

public class Explosion extends Actor {
    
    private int cycleCnt = 0; // For timing
    
    private long lastTime = System.currentTimeMillis(); // Get last time
    
    /**
     * Act - do whatever the Explosion wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act () {
        this.cycleCnt++;
        if (this.cycleCnt % 100 == 0 && !getWorld().getObjects(Explosion.class).isEmpty()) { // Remove the explosion after approximately 3 seconds
            getWorld().removeObject(this);
            this.cycleCnt = 0;
        }
    }    
}
