import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.awt.font.FontRenderContext; // For drawstring
import java.awt.Graphics2D; // For drawstring

/**
 * This class is used for displaying the Text objects that appear in Backyard
 * Is different from the other Text class
 * 
 * @author Eric Liu
 * @version May 30, 2018
 */

public class Text1 extends Actor {
    
    /**
     * 1st Constructor for class
     * @param length = the length of the text
     */
    public Text1 (int length) {
        setImage(new GreenfootImage(length * 12, 16));
    }
    
    /**
     * 2nd Constructor for class
     * @param text = String variable for message
     */
    public Text1 (String text) {
        this(text.length()); // Uses this String constructor: String s = String("20");
        setText(text); // Make sure to display the text
    }
    
    /**
     * Method for displaying the String text as a message
     * @param text = message to be displayed
     */
    public void setText (String text) {
        GreenfootImage image = getImage();
        image.clear();
        image.drawString(text, 2, 12);
    }
    
    /**
     * Method for setting the location of the object
     */
    public void setLocation (int x, int y) {
        super.setLocation(x + getImage().getWidth() / 2, y);
    }
    
    /**
     * Act - do whatever the Text1 wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act () {
        
    }    
}
