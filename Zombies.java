import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * This class is used for the zombie objects that are added to the world
 * Class defines the behaviour for any walking zombie
 * For more info, see the subclasses FootballZombie, Normal_Zombie, ConeheadZombie, BucketZombie
 * The subclasses: ConehadZombie and BucketZombie, as well as the super class all uses the vector class
 * NOTE: Still need to include graphics and code to enable zombie to eat up plants that it comes into contact with
 * 
 * @author Eric Liu
 * @version May 9, 2018
 */

public class Zombies extends SmoothMover {
    
    // Declare all of the PROTECTED variables here
    protected int zombie_speed;
    protected int zombie_health;
    protected GreenfootSound incoming_zombie = new GreenfootSound("zombies_coming.wav");
    protected GreenfootSound hitsound = new GreenfootSound("hit.wav");
    protected GreenfootSound scream = new GreenfootSound("scream.wav");
    protected static boolean game_over = false;
    protected static int zombie_count = 0;
    protected String game_mode; // Store the current game mode
    protected Player_Info player_info;
    protected Sound_Info sound_info;
    protected boolean is_dying = false;
    
    /**
     * First constructor for zombies class
     * @param soundinfo = Sound_Info object
     * @param info = Player_Info object
     * @param mode = game mode user chose as a String
     */
    public Zombies (Sound_Info soundinfo, Player_Info info, String mode) {
        if (zombie_count == 0) incoming_zombie.play();
        zombie_count++;
        this.sound_info = soundinfo;
        this.player_info = info;
        this.game_mode = mode;
        is_dying = false;
    }
    
    /**
     * This helper method is used to set the current speed of the zombie object
     */
    protected void SetSpeed (int speed) {
        // Shouldn't be moving if it is currently touching a plant object
        if (isTouching(Plants.class)) this.zombie_speed = 0;
        else this.zombie_speed = speed;
    }
    
    /**
     * ThIs helper method is used to set the current health of the zombie object
     */
    protected void SetHealth (int health) {
        this.zombie_health = health;
    }
    
    /**
     * This method is used whenever the zombie object is hit by the bullet objects and it is about to die
     * @param fileName = file name of the dying gif animation file
     * @param damage = the damage caused by the bullet
     * @param num_time = amount of time for the gif animation file
     * @param points = number of points awarded for killing the zombie object
     */
    protected void ZombieHit (String fileName, int damage, int num_time, int points) {
        // Check for intersection with a bullet object
        if (isTouching(Bullet.class)) {
            zombie_health -= damage; // Subtract the damage from the zombie's health
            removeTouching(Bullet.class); // Remove the bullet once it has hit the zombie
            hitsound.play(); // Play the sound when the bullet touches the zombie 
        }
        
        // Check if the zombie object is dead
        if (zombie_health <= 0) { // Check the zombie's health
            removeTouching(Bullet.class); // Remove the currently touching bullet object
            Dying_Animation(fileName, num_time); // Execute the dying animation for the zombie class
            if (this.game_mode.equalsIgnoreCase("blitz")) {
                Blitz_Backyard world = (Blitz_Backyard) getWorld();
                Counter score = world.Get_ScoreCounter();
                score.add(points); // Add the score to the counter object
                world.removeObject(this); // Remove the zombie object from the world
            }
            /*
            else if (this.game_mode.equalsIgnoreCase("regular")) {
                Backyard world = (Backyard) getWorld();
                Counter score = world.Get_ScoreCounter();
                score.add(points); // Add the score to the counter object
                world.removeObject(this); // Remove the zombie object from the world
            }*/
        }
    }
    
    /**
     * Method is used to execute the dying animation of the zombie, using the DeadActor class
     */
    protected void Dying_Animation (String fileName, int num_time) {
        DeadActor dead = new DeadActor(fileName, num_time);
        World world = getWorld();
        world.addObject(dead, getX(), getY());
    }
    
    /**
     * This method checks to see if the player has lost when the zombie crosses the garden and enters the house
     * The game should end when this happens
     */
    protected void Check_Loss () {
        if (this.game_mode.equalsIgnoreCase("blitz")) {
                Blitz_Backyard world = (Blitz_Backyard) getWorld();
                int col = world.Get_Col_Position(getX());
                if (getX() < 260) { // Check if the current x coordinate is less than 260 -> zombie has crossed the lawn
                    SetHealth(-1); // Set health to -1
                    Counter score = world.Get_ScoreCounter(); // Get the value of the score counter
                    world.stopBackgroundMusic(); // Stop the background music
                    scream.play();
                    game_over = true;
                    Greenfoot.setWorld(new GameOver(this.sound_info, this.player_info)); // Transition to the GameOver screen and make sure to display the current score
                }
            }
            /*
            else if (this.game_mode.equalsIgnoreCase("regular")) {
                Backyard world = (Backyard) getWorld();
                int col = world.Get_Col_Position(getX());
                if (getX() < 260) { // Check if the current x coordinate is less than 260 -> zombie has crossed the lawn
                    SetHealth(-1); // Set health to -1
                    Counter score = world.Get_ScoreCounter(); // Get the value of the score counter
                    world.stopBackgroundMusic(); // Stop the background music
                    game_over = true;
                    Greenfoot.setWorld(new GameOver(this.sound_info, this.player_info)); // Transition to the GameOver screen and make sure to display the current score
                }
            }*/
        }
        
    protected boolean Game_Over () {
        return this.game_over;
    }
    
    /**
     * Act - do whatever the Zombies wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act () {

    }
}
