# WordsE
## FINAL TEAM PROJECT
* ICS4U3
* MR CHAN
* GROUP: ERIC LIU, CHARLES CAI, CINDY CAO
* PROJECT: PLANTS VS. ZOMBIES (MINI-VERSION)


### INTRODUCTION:
* Plants vs Zombies (https://en.wikipedia.org/wiki/Plants_vs._Zombies) is a tower-defense and strategy game which involves an army of zombies trying to enter the house and eat your brains. The only way you can stop them is by using your arsenal of plants that will attack the zombies at your command. The plants - Pea-shooters, Beetroots, Walnuts, and Sunflowers - are all set to destroy and handle the zombies who dare to enter your backyard.


### GUIDE:
* The playing field is divided into 5 horizontal lanes, a zombie will only move towards the player's house along one lane. Planting costs "sunlight" (sunlight acts as the currency for the game, which is required for buying plants) which can be gathered for free and also by planting the sunflower plant which generates sunlight at regular intervals. Plants can only attack or defend against zombies in the lane they are planted in.


### OBJECTIVE:
* Gain As Much Points As Possible By Trying To Survive For The Longest Period Of Time That You Can!


### FEATURES:
The are two modes in this game, Regular mode let you kill zombies and level up. The biltz mode will let you fight agianst zombies continuously and it will never stop!!

You Get To Choose From A List Of Plants:

Regular
Regular pea shooter - Peashooter is the first and the primary attacking plant the game. It is a peashooting plant that shoots peas on seeing a zombie. It costs 100 sunlight.

double/triple/machinegun/ice pea shooter - They are the more powerful pea shooters. More damage, more cost!

Walnut - The defensive plant that you can use. A shield for you attacking plants

Cherry Bomb - Causing instant death of the zombies

SunFlower - The sun-producing plant, you can't make it to the end without that one!

Blitz
PeaShooter - Peashooter is the first and the primary attacking plant the game. It is a peashooting plant that shoots peas on seeing a zombie. It costs 100 sunlight.

BeetRoot - The user has another attacking plant, the BeetRoot which shoots beets insteasd of peas which are faster and hence is priced a bit higher at 125 sunlight.

SunFlower - Sunflower is an essential sun-producing plant which is necessary for producing sunlight - the currency of the game. It costs 50 sunlight. Each sunlight produced from sunflower adds 25 units to your sunlight counter.

Walnut - Walnut is the defensive plant that acts as a shield for the player's plants. It takes a long time for zombies to eat it, providing an effective cover for the plants located behind it. It is mainly used to stall zombies to waste their time, letting other plants attack them.

You Will See a List Of Zombies:

Regular:
Normal zombie - takes 4 hits and walk slowly.

Baby zombie - takes 3 hits but walks faster than other zombies.

Cone/Bucket zombie - takes 5/6 hits. They walks very slowly.

Biltz:
Normal Zombie - Normal zombies and their time-themed variants in Plants vs. Zombies 2 are the most basic "zombie" units. In fact, it takes four shots for a pea plant to kill them. Having no special defensive equipment or travel abilities, these zombies are susceptible to any type of attack.

Football Zombie - It's a tough zombie and it takes the PeaShooter 5 shots to kill it.

Finally, You Get To Save Your Scores And See The Leaderboard At The End!


### UPDATES TO COME:
* ADD MORE CHARACTERS (MORE PLANTS AND ZOMBIES)
* IMPROVE ANIMATIONS TO MAKE THE MINI-GAME MORE REALISTIC TO THE REAL GAME