import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * This class is used for displaying a frozen baby zombie
 * 
 * @author Eric Liu
 * @version June 12th, 2018
 */

public class Frozen_Babyzombie extends Frozen_Zombie {
    
    private int cycleCnt = 0; // For timing
    
    private long lastTime = System.currentTimeMillis(); // Get last time
    
    /**
     * Constructor
     * @param health = health of zombie object
     */
    public Frozen_Babyzombie (int health, int level) {
        super(health, level);
    }
    
    /**
     * Act - do whatever the Frozen_Babyzombie wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act () {
        this.cycleCnt++;
        if (this.cycleCnt % 200 == 0 && !getWorld().getObjects(Frozen_Babyzombie.class).isEmpty()) { // Remove the explosion after approximately 2 seconds
            Babyzombie zombie = new Babyzombie(Babyzombie.sound_info, Babyzombie.player_info, Babyzombie.difficulty, Babyzombie.level_idx, Babyzombie.done_waves);
            getWorld().addObject(zombie, this.getX(), this.getY());
            getWorld().removeObject(this);
            zombie.set_Health(this.zombie_health);
            this.cycleCnt = 0;
        }
    }     
}
