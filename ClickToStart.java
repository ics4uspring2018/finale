import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class ClickToStart here.
 * 
 * @author Eric Liu
 * @version May 4, 2018
 */

public class ClickToStart extends Actor {
    
    private String file; // Name of .gif file
    private GifImage gif; // Use this for displaying gif files
    private String to_screen; // For the world screen that it should transition to
    private String from_screen; // For the current world screen that getWorld() returns
    private boolean pressed = false;
    private Sound_Info sound_info;
    private Player_Info player_info;
    
    /**
     * Constructor for this class
     * @@param filename = String name of file
     * @param world_screen1 = String name of source screen
     * @param world_screen2 = String name of destination screen
     */
    public ClickToStart (String filename, String world_screen1, String world_screen2) {
        // Set all of the variables to the parameters
        this.file = filename;
        if (file != null) gif = new GifImage(file);
        this.to_screen = world_screen1;
        this.from_screen = world_screen2;
    }
    
    public boolean isPressed () {
        return this.pressed;
    }
    
    public void Set_SoundInfo (Sound_Info soundinfo) {
        this.sound_info = soundinfo;
    }
    
    public void Set_PlayerInfo (Player_Info playerinfo) {
        this.player_info = playerinfo;
    }
    
    /**
     * Act - do whatever the click wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act () {
        setImage(gif.getCurrentImage());
        // Code for mouse activity and/ detection
        if (Greenfoot.mouseClicked(this) && !this.pressed) {
            if (this.from_screen.equalsIgnoreCase("start_screen")) {
                StartScreen world1 = (StartScreen) getWorld();
                if (world1 != null) world1.stopBackgroundMusic();
            }
            if (this.from_screen.equalsIgnoreCase("game_modes")) {
                Game_Modes world2 = (Game_Modes) getWorld();
                if (world2 != null) world2.stopBackgroundMusic();
            }
            this.pressed = true;
            // Have several string parameters that denote different game modes 
            if (this.to_screen.equalsIgnoreCase("modes")) Greenfoot.setWorld(new Game_Modes(this.sound_info, this.player_info));
            else if (this.to_screen.equalsIgnoreCase("settings")) Greenfoot.setWorld(new Settings_Screen(this.sound_info, this.player_info));
            else if (this.to_screen.equalsIgnoreCase("instructions")) Greenfoot.setWorld(new Instructions(this.sound_info, this.player_info));
            // else if (this.to_screen.equalsIgnoreCase("introduction")) Greenfoot.setWorld(new Introduction(this.sound_info, this.player_info));
            else if (this.to_screen.equalsIgnoreCase("mode1")) Greenfoot.setWorld(new Backyard(this.sound_info, this.player_info, 2, false, 0));
            else if (this.to_screen.equalsIgnoreCase("mode2")) Greenfoot.setWorld(new Blitz_Backyard(this.sound_info, this.player_info));
        }
    }
}
