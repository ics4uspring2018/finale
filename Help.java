import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * This class is used to display the help screen which has information on the game in general
 * 
 * @author Eric Liu
 * @version May 16, 2018
 */

public class Help extends World {

    private Button back; // Declare Button object for the back button
    private GreenfootSound music = new GreenfootSound("menu.wav");
    private Sound_Info sound_info = null;
    private Player_Info player_info;
    
    /**
     * 1st Constructor for objects of class About.
     * 
     */
    /*
    public Help () {    
        // Create a new world with 1111x700 cells with a cell size of 1x1 pixels.
        super(1111, 700, 1);
        back = new Button("back", new GreenfootImage("back_button.png"));
        addObject(back, 1111 / 2, 400);
        Display();
        music.playLoop();
    }*/
    
    /**
     * 2nd Constructor for objects of class Help.
     * 
     */
    public Help (Sound_Info soundinfo, Player_Info playerinfo) {
        // Create a new world with 1111x700 cells with a cell size of 1x1 pixels.
        super(1111, 700, 1);
        this.sound_info = soundinfo;
        this.player_info = playerinfo;
        back = new Button("back", new GreenfootImage("back_button.png"));
        addObject(back, 1111 / 2, 400);
        Display();
        music.playLoop();
    }
    
    public void stopBackgroundMusic () {
        this.music.stop();
    }
    
    /**
     * Method is used to display the text on-screen
     */
    public void Display () {
        addObject(new Text("HELP", 30, "yellow"), 1111 / 2, 150);
        addObject(new Text("The playing field is divided into 5 horizontal lanes, a zombie", 20, "white"), 550, 200);
        addObject(new Text("will only move towards the player's house along one lane. Planting", 20, "white"), 550, 250);
        addObject(new Text("costs sunlight which can be collected for free and from sunflower plants.", 20, "white"), 550, 300);
        addObject(new Text("Plants only attack or defend against zombies in the lane they are planted", 20, "white"), 550, 350);
        addObject(new Text("To plant, click on the seed packet FIRST, then click on the position", 20, "red"), 550, 440);
        addObject(new Text("you want it to be planted. Click the seed packet again to deselect.", 20, "red"), 550, 460);
        addObject(new Text("OBJECTIVE", 30, "yellow"), 550, 490);
        addObject(new Text("Gain as much points as possible by trying to survive for as long as you can!", 20, "white"), 550, 540);
        addObject(new Text("You can view the scoreboard in the end to see your scores!", 20, "white"), 550, 590);
    }
    
    /**
     * Act - do whatever the Player_Info wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act () {
        if (back.isPressed()) {
           stopBackgroundMusic();
           Greenfoot.setWorld(new Instructions(this.sound_info, this.player_info)); // Go back to the starting screen
        }
    }
}
