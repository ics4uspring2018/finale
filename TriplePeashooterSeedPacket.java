import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * This class is used for the triple peashooter seed packet that is displayed onto the seedpacket tray during in-game
 * 
 * @author Eric Liu
 * @version May 30, 2018
 */

public class TriplePeashooterSeedPacket extends Seedpacket {
    
    // Standard card
    private GreenfootImage img = new GreenfootImage("SeedPacket_TriplePeaShooter.png");
    // For if user clicks on/selects this card
    private GreenfootImage img_selected = new GreenfootImage("SeedPacket_TriplePeaShooter_selected.png");
    // For if user has already clicked on the card
    public static boolean has_selected;
    
    /**
     * 1st Constructor for this class
     */
    public TriplePeashooterSeedPacket () {
        setImage(img);
        has_selected = false;
    }
    
    /**
     * Method's used to reset the image
     */
    public void resetImage () {
        setImage(img);
    }
    
    /**
     * Act - do whatever the PeashooterSeedpacket wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act () {
        if (Greenfoot.mouseClicked(this)) {
            if (!has_selected) {
                has_selected = true;
                Backyard game = (Backyard) getWorld();
                game.setTriplePeaShooterSeedPacketClickStatus(true);
                setImage(img_selected);
            }
            else{ // If already selected, then "de-select" the card
                has_selected = false;
                Backyard game = (Backyard) getWorld();
                game.setTriplePeaShooterSeedPacketClickStatus(false);
                resetImage();
            }
        }
    }    
}
