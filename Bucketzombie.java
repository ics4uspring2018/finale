import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * This class is used to define the behaviour for the Buckethead zombie
 * This class is separate from the other Zombies class as this is to be used for the regular mode, not blitz mode
 * As such, it makes use of the SmoothMovement super class
 * 
 * @author Eric Liu
 * @version May 30, 2018
 */

public class Bucketzombie extends Regular_Zombies {
    
    private boolean is_dying = false; // Is the zombie object dying?
    private GifImage gif1 = new GifImage("easy_bucket_zombie_walking.gif"); // Main gif file
    private GifImage gif2 = new GifImage("medium_bucket_zombie_walking.gif");
    private GifImage gif3 = new GifImage("hard_bucket_zombie_walking.gif");
    private GreenfootImage fbz = new GreenfootImage("FBZ.png");
    private int cycleCnt = 0; // Used for timing

    /**
     * Constructor
     * @param diff_level = difficulty chosen by user (1-3)
     */
    public Bucketzombie (Sound_Info soundinfo, Player_Info playerinfo, int diff_level, int level, boolean done) {
        super(soundinfo, playerinfo, diff_level, level, done);
        if (this.difficulty == 1) {
            setImage(gif1.getCurrentImage());
        }
        else if (this.difficulty == 2) {
            setImage(gif2.getCurrentImage());
        }
        else if (this.difficulty == 3) {
            setImage(gif3.getCurrentImage());
        }
        set_Health(120); // Set to 200 health
    }
    
    /**
     * Act - do whatever the Bucketzombie wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act () {
        // Just make sure to check for any hits from any plants
        // As well as to animate the death of the zombie object, if its health goes below zero
        checkForHit(); // Check for hits from bullets
        if (this.difficulty == 1) {
            setImage(gif1.getCurrentImage());
        }
        else if (this.difficulty == 2) {
            setImage(gif2.getCurrentImage());
        }
        else if (this.difficulty == 3) {
            setImage(gif3.getCurrentImage());
        }
        is_dying = getDying();
        // Move as long as if it's still alive
        
        // if (!getIntersectingObjects(SnowPea.class).isEmpty()) {
        if (isTouching(SnowPea.class) && Check_Plant_In_Same_Row()) {
            this.setImage(fbz);
            this.stop();
        }
        
        if (!is_dying) {// && !isTouching(Plant.class)) {
            if (!Check_Plant_In_Front() && !(isTouching(SnowPea.class))) {
                if (this.ice_pea && this.cycleCnt % 200 == 0) {
                    setLocation(getExactX() - 0.18, getExactY());
                    this.cycleCnt = 0;
                }
                else {
                    setLocation(getExactX() - 0.18, getExactY());
                }
            }
        }
        else if (is_dying) animateDeath();
        // Make sure to stop moving, if it's in front of a plant
        Check_Win(); // Check for zombie win
        
        // Award player with points, depending on the difficulty level chosen by the player
        if (this.difficulty == 1) set_Points(150);
        else if (this.difficulty == 2) set_Points(160);
        else if (this.difficulty == 3) set_Points(170);
    }    
}
