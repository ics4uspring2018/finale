import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.io.*;
import java.util.*;

/**
 * Class is used to display the scoreboard/leaderboard at the end of the top scores -> should be top 10
 * 
 * @author Eric Liu
 * @version May 3, 2018
 */

public class Scoreboard extends World {

    // private GreenfootImage background = new GreenfootImage("gameover.png");
    private ArrayList<Player_Info> scores = new ArrayList<Player_Info>(); // ArrayList used to store the top scores from the .txt file
    private Text label = new Text("All Time Leaderboard", 30, "YELLOW"); // Main title
    private String filename = "scores.txt";
    private Reading_and_Writing_Text_Files reading_writing_files_1 = new Reading_and_Writing_Text_Files(filename); // Main object used to store the top scores from the file
    private Scanner sc = null; // Use Scanner to read the contents from the file for now
    private Button back;
    private Sound_Info sound_info;
    private Player_Info player_info;
    private int which_screen;
    
    /**
     * Constructor for objects of class Scoreboard.
     * @param type_screen = win or lose screen?
     *      1 = lose_screen
     *      2 = win_screen
     */
    public Scoreboard (Sound_Info soundinfo, Player_Info info, int type_screen) {    
        // Create a new world with 1030x700 cells with a cell size of 1x1 pixels.
        super(1030, 700, 1);
        // background.scale(getWidth(), getHeight()); // Scale the background picture
        // getBackground().drawImage(background, 0, 0); // Set the background picture
        back = new Button("back", new GreenfootImage("back_button.png"));
        addObject(back, 1000 / 2, 630);
        Read_From_File();
        Display();
        this.sound_info = soundinfo;
        this.player_info = info;
        this.which_screen = type_screen;
    }
    
    public Scanner getScanner (String filename) {
        InputStream myFile = getClass().getResourceAsStream(filename);
        if (myFile != null) return new Scanner(myFile);
        return null;
    }
    
    /**
     * Method is used to read the top scores from the file and store them into the ArrayList
     * Uses Java's Scanner to do this
     */
    public void Read_From_File () {
        int idx1 = 0, idx2 = 1, idx3 = 2;
        String nextline = null;
        try {
            if (sc == null) sc = getScanner(filename);
            while (sc.hasNextLine()) {
                String tokens[] = sc.nextLine().split(", "); // Split the strings on the line
                scores.add(new Player_Info(tokens[0], Integer.parseInt(tokens[1]), Integer.parseInt(tokens[2]), 0)); // Integer.parseInt(tokens[3])));
            }
            if (sc != null) sc.close();
        }
        catch (InputMismatchException ex) {
            ex.printStackTrace();
        }
    }
    
    /**
     * Method is used to display the scores onto the World
     */
    public void Display () {
        addObject(label, 500, 50); // Add the title at the top
        Collections.sort(scores); // Sort the ArrayList beginning from greatest to least scores
        int idx = 1; // Store the current index
        int ycoor = 100; // Initial y coordinate
        int xcoor = 150;
        for (Player_Info score : scores) {
            if (idx > 10) break; // Should store the top 10 scores
            String username = score.getName();
            int scorenum = score.getScore();
            int level = score.getLevel();
            addObject(new Text("Player " + String.valueOf(idx) + ":", 20, "WHITE"), xcoor, ycoor);
            addObject(new Text("Username: ", 20, "WHITE"), xcoor + 120, ycoor);
            addObject(new Text(username, 20, "WHITE"), xcoor + 260, ycoor);
            addObject(new Text("Level: ", 20, "WHITE"), xcoor + 380, ycoor);
            addObject(new Text(String.valueOf(level), 20, "WHITE"), xcoor + 440, ycoor);
            addObject(new Text("Score: ", 20, "WHITE"), xcoor + 530, ycoor);
            addObject(new Text(String.valueOf(scorenum), 20, "WHITE"), xcoor + 600, ycoor);
            ycoor += 50; // Text should be 50 units apart vertically
            idx++;
        }
    }
    
    /**
     * Act - do whatever the Player_Info wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act () {
        if (Greenfoot.mouseClicked(back)) {
            if (this.which_screen == 1) Greenfoot.setWorld(new GameOver(this.sound_info, this.player_info)); // Go back to the game over screen
            else if (this.which_screen == 2) Greenfoot.setWorld(new Win_Screen(this.sound_info, this.player_info)); // Go back to the game over screen
        }
    }
}
