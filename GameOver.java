import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Class is used to display the Game Over screen when the user dies
 * 
 * @author Eric Liu
 * @version May 3, 2018
 */

public class GameOver extends World {

    private GreenfootImage background = new GreenfootImage("gameover.png");
    private GreenfootImage pic = new GreenfootImage("ZombiesWon.png");
    private GreenfootSound music = new GreenfootSound("atebrains.wav");
    private Button restart; // Restart button
    private Button exit; // Exit button
    private Button view_scoreboard; // For viewing the scoreboard
    private Player_Info player_info;
    private Sound_Info sound_info;
    
    public GameOver (Sound_Info soundinfo, Player_Info info) {    
        // Create a new world with 1111x602 cells with a cell size of 1x1 pixels.
        super(1111, 602, 1);
        // restart = new Button("restart", new GreenfootImage("restart_button.png"));
        exit = new Button("exit", new GreenfootImage("exit_button.png"));
        view_scoreboard = new Button("view_scoreboard", new GreenfootImage("view_scoreboard.png"));
        music.play();
        Counter counter = new Counter();
        addObject(counter, 613, 553);
        this.sound_info = soundinfo;
        this.player_info = info;
        if (this.player_info != null) counter.setValue(this.player_info.getScore());
        Replay replay = new Replay(this.sound_info, this.player_info);
        addObject(replay, 900, 550);
        addObject(view_scoreboard, 1111 / 2, 495);
        // getBackground().drawImage(pic, 800, 300);
    }
    
    /**
     * Stops the background music after playing it once
     */
    public void stopBackgroundMusic () {
        music.stop();
    }
    
    /**
     * Act - do whatever the Player_Info wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act () {
        if (Greenfoot.mouseClicked(exit)) {
            music.stop();
            Greenfoot.stop();
        }
        else if (Greenfoot.mouseClicked(view_scoreboard)) {
            music.stop();
            Greenfoot.setWorld(new Scoreboard(this.sound_info, this.player_info, 1));
        }
    }
}
