import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * This class is used for the waiting feature for the bucket zombie that is used in-game
 * 
 * @author Eric Liu
 * @version May 30, 2018
 */

public class BucketZombieWaiting extends Zombie_Waiting {
    
    private GifImage gif = new GifImage("bucket_zombie_waiting.gif");
    
    public BucketZombieWaiting () {
        setImage(gif.getCurrentImage());
    }
    
    /**
     * Act - do whatever the BucketZombieWaiting wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act () {
        setImage(gif.getCurrentImage());
    }    
}
