import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * This subclass is used for the Beetroot object
 * 
 * @author Eric Liu
 * @version May 9, 2018
 */

public class Beetroot extends Plants {
    
    private GifImage gif = new GifImage("beetroot.gif");
    
    /**
     * Constructor for this class
     */
    public Beetroot () {
        setImage(gif.getCurrentImage());
    }
    
    private long last_time = System.currentTimeMillis();
    
    /**
     * Helper method to make the beetroot plant object to shoot the beet bullets
     */
    public void Shoot_Beets (int num_time) {
        if (!getWorld().getObjects(Zombies.class).isEmpty()) {
            long currTime = System.currentTimeMillis();
            if (currTime >= last_time + num_time) {
                last_time = currTime;
                Beet beet = new Beet(); // Instantiate a new object of the beet class
                World world = getWorld();
                world.addObject(beet, getX() + 29, getY() + 29);
            }
        }
    }
    
    /**
     * Act - do whatever the Beetroot wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act () {
        setImage(gif.getCurrentImage());
        Shoot_Beets(2000); // Shoot a beet for every 2 seconds
        Plant_Hit("beetroot_dying.gif", 1000);
    }    
}
