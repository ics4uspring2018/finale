import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * This class is used to define the behavior for the walking feature of the basic zombie
 * Is used at the end of the game - only award it to the user when the user has completed the current level
 * 
 * @author Eric Liu
 * @version May 30, 2018
 */

public class SeedPacketEndStage extends Actor {
    
    private int transparency = 0; // Set the transparency level to 0
    private boolean is_clicked = false; // Whether the card has been clicked
    private boolean is_active = false; // Whether the card is active/clicked by the user
    private GreenfootSound pressedsound = new GreenfootSound("select.mp3");
    private boolean has_won = false; // Whether the user has won
    private boolean game_over = false; // Whether the game has ended
    private int level_num;
    private GreenfootImage [] images = new GreenfootImage[11]; // Should be 11 end stage seed packets in total!!!
    
    /**
     * 1st Constructor for this class
     * @param level = current stage that user is at for regular mode
     * Used for determining which seed packet to award user
     */
    public SeedPacketEndStage (int level) {
        this.level_num = level;
        // Hard code everything in
        images[0] = new GreenfootImage("SeedPacket_Potato.png");
        images[1] = new GreenfootImage("CherrybombSeedPacket.png");
        images[2] = new GreenfootImage("SeedPacket_DoublePeaShooter.png");
        images[3] = new GreenfootImage("SeedPacket_TriplePeaShooter.png");
        images[4] = new GreenfootImage("SeedPacket_MachineGun_Peashooter.png");

        images[5] = new GreenfootImage("SeedPacket_IcePeaShooter.png");
        images[6] = new GreenfootImage("trophy.png");
        
        // Decided to put trophy ahead of these since, the functionalities of these plants are technically unfinished
        
        setImage(images[level_num - 2]); // NOTE: Should subtract by 2, since we ignore first 2 seed packets and array is 0-indexed
    }
    
    /**
     * Method's used to set the transparency of the image
     */
    public void setAlpha (int alpha) {
        getImage().setTransparency(alpha);
    }
    
    /**
     * Method is used to check if the user has clicked the seed packet
     * Also updates the boolean variables
     */
    public void User_Clicked () {
        MouseInfo mouse = Greenfoot.getMouseInfo();
        if (mouse != null && mouse.getActor() == this && mouse.getButton() == 1) {
            is_clicked = true;
            pressedsound.play();
            is_active = true;
        }
    }
    
    /**
     * Return whether or not the user has clicked on it during end-game
     */
    public boolean get_Clicked () {
        return this.is_clicked;
    }
    
    /**
     * Act - do whatever the SeedPacketEndStage wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act () {
        User_Clicked(); // Check if user has clicked the seed packet
    }    
}
