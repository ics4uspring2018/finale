import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * This class is used for the lawnmower object
 * 
 * @author Eric Liu
 * @version May 9, 2018
 */

public class Lawnmower extends Actor {
    
    private GifImage gif = new GifImage("lawn_mower.gif");
    private int speed = 0;
    GreenfootSound sound = new GreenfootSound("lawnmower.wav");
    
    public Lawnmower () {
        setImage(gif.getCurrentImage());
    }
    
    protected void Check_Boundaries () {
        if (getX() > getWorld().getWidth() - 10) getWorld().removeObject(this);
    }
    
    /**
     * Act - do whatever the Lawnmower wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act () {
        move(speed);
        // If a zombie object is touching it, then execute the animation via the gif file and play the sound
        if (isTouching(Zombies.class)) {
            removeTouching(Zombies.class); // Remove the zombie object from the world
            speed = 12; // Set the speed to 12 for now
            sound.play(); // Play the sound
        }
        if (speed > 0) setImage(gif.getCurrentImage()); // Only show the animation if the speed is a positive integer
        Check_Boundaries();
    }    
}
